module.exports = {
  client: 'sqlite3',
  useNullAsDefault: true,
  connection: {
    filename: process.env.SQLITE_BROWSER_DATA || './data/development.db',
  },
  pool: {
    afterCreate: (conn, cb) => {
      conn.run('PRAGMA foreign_keys = ON', cb)
    },
  },
}
