import path from 'path';
import {promises as fs} from 'fs';
import {PartialModelGraph, GraphParameters} from 'objection';

import {
  AppSetting,
  initializeModels,
  RunScenario,
  RunSession,
} from '../main/model';

type InsertType = PartialModelGraph<RunScenario, RunScenario & GraphParameters>[];

export const main = async () => {
  if (process.env.NODE_ENV !== 'test') {
    process.env.NODE_ENV = 'development';
  }

  const dataDir = path.resolve(__dirname, '..', '..', 'data');

  const deinitialize = process.env.NODE_ENV === 'test' ? null : (await initializeModels({
    defaultDataDir: dataDir,
    userDataDir: dataDir,
    migrate: true,
    migrationDirectory: path.resolve(__dirname, '..', '..', 'migrations'),
  }));

  const seedPath = path.resolve(__dirname, '..', '..', 'scripts', 'seed_data.json')
  const str = await fs.readFile(seedPath, 'utf8');
  const data = JSON.parse(str) as InsertType;

  for (let i = 0; i < data.length; ++i) {
    const dataItem = data[i];
    const res = await RunScenario.query().insertGraph(
      [dataItem],
      {allowRefs: true}
    );

    const runSession = res[0].run_sessions[0];
    const runSessionSplit = runSession.run_session_splits[0];

    await RunSession.query()
      .findById(runSession.id)
      .patch({
        current_session_split_id: runSessionSplit.id,
      });
  }

  await AppSetting.query().insert({
    id: 1,
    current_run_scenario_id: 1,
  });

  if (deinitialize) {
    await deinitialize();
  }
}

main().then(() => {
  console.log('seeding complete');
}).catch((err) => {
  console.error(err);
});
