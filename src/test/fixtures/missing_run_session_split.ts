import {
  AppSetting,
  RunScenario,
  RunSession,
  SplitCollection,
} from '../../main/model';

export const missingRunSessionSplitFixture = async () => {
  await RunScenario.query().insert({
    id: 1,
    name: 'run scenario 1'
  });

  await AppSetting.query().insert({
    id: 1,
    current_run_scenario_id: 1,
  });

  await SplitCollection.query().insertGraph([
    {
      id: 1,
      name: 'split collection 1',
      run_scenario_id: 1,
      split_steps: [
        {id: 1, name: '1', split_collection_id: 1, order: 0},
        {id: 2, name: '2', split_collection_id: 1, order: 1},
        {id: 3, name: '3', split_collection_id: 1, order: 2},
        {id: 4, name: '4', split_collection_id: 1, order: 3},
      ],
    }
  ]);

  await RunSession.query().insertGraph({
    id: 1,
    run_scenario_id: 1,
    run_session_split_collections: [
      {
        id: 1,
        run_session_id: 1,
        current_split_step_id: 1,
        split_collection_id: 1,
      },
    ],
    run_session_splits: [
      {id: 1, run_session_id: 1, split_step_id: 1, hits_way: 0, hits_boss: 0, time: 0},
      {id: 2, run_session_id: 1, split_step_id: 2, hits_way: 0, hits_boss: 0, time: 0},
      // third split step should be removed
      // {id: 3, run_session_id: 1, split_step_id: 3, hits_way: 0, hits_boss: 0, time: 0},
      {id: 4, run_session_id: 1, split_step_id: 4, hits_way: 0, hits_boss: 0, time: 0},
    ],
  });

  await RunSession.query().update({current_session_split_id: 1});
}
