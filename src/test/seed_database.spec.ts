import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from './helpers';
import {getSplitPageDataHandler} from '../main/ipc_api/handlers';

import {main} from './seed_database'

import {
  RunScenario,
} from '../main/model';

describe('seed_database', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('creates default data', async () => {
    await main();
    const subject = await getSplitPageDataHandler({}, '1');
    expect(subject.runScenario.id).to.be.a('number');
    const runScenarios = await RunScenario.query();
    expect(runScenarios.length).to.eql(17);
  });
});
