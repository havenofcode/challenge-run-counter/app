import {
  initializeModels,
  defaultOpts,
} from '../main/model';

export type RestoreTask = () => Promise<void>

export const waitTick = () => new Promise<void>((resolve) => {
  setTimeout(() => resolve(), 2);
});

export const getRestoreDbTasks = () => {
  let destroyFunc = () => Promise.resolve();

  return {
    async restoreDb() {
      if (process.env.NODE_ENV !== 'test') {
        throw new Error('NODE_ENV !== test');
      }
      destroyFunc = await initializeModels({...defaultOpts, migrate: true});
    },
    async destroyDb() {
      await destroyFunc();
      destroyFunc = () => Promise.resolve();
    }
  }
};
