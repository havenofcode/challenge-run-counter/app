import {logger} from '../common/util';
import {ipcRenderer, contextBridge, IpcRendererEvent} from 'electron';
import {OpenDialogOptions} from 'electron';

import {
  IBridge,
  IEventShortcut,
  IEventShortcutError,
  IPageSettings,
  IPaginatedOneToManyParams,
  ISwapSplitOrderParams,
  ITrySetGlobalShortcutParams,
} from '../common';

import {
  IAppSetting,
  IRunScenario,
  IRunSession,
  IRunSessionSplit,
  ISplitCollection,
  ISplitPageData,
  ISplitStep,
  IViewerSetting,
  IViewerSettingsJson,
} from '../common/model';

const bridge: IBridge = {
  get_split_collection: async (id: string) => {
    logger.info('bridge:get_split_collection');
    return await ipcRenderer.invoke('get_split_collection', id);
  },
  get_run_scenario: async (id: string) => {
    logger.info('bridge:get_run_scenario');
    return await ipcRenderer.invoke('get_run_scenario', id);
  },
  get_run_session: async (id: string) => {
    logger.info('bridge:get_run_session');
    return await ipcRenderer.invoke('get_run_session', id);
  },
  create_run_session: async (data: Partial<IRunSession>) => {
    logger.info('bridge:create_run_session');
    const res = await ipcRenderer.invoke('create_run_session', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  update_run_session: async (data: Partial<IRunSession>) => {
    logger.info('bridge:update_run_session');
    const res = await ipcRenderer.invoke('update_run_session', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  get_all_run_sessions: async (params: IPaginatedOneToManyParams) => {
    logger.info('bridge:get_all_run_sessions');
    return await ipcRenderer.invoke('get_all_run_sessions', params);
  },
  // SplitStep
  create_split_step: async (data: Partial<ISplitStep>) => {
    logger.info('bridge:create_split_step');
    const res = await ipcRenderer.invoke('create_split_step', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  update_split_step: async (data: Partial<ISplitStep>) => {
    logger.info('bridge:update_split_step');
    const res = await ipcRenderer.invoke('update_split_step', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  update_all_split_steps: async (data: Partial<ISplitStep>[]) => {
    logger.info('bridge:update_all_split_steps');
    const res = await ipcRenderer.invoke('update_all_split_steps', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  get_split_step: async (id: string) => {
    logger.info('bridge:get_split_step');
    return await ipcRenderer.invoke('get_split_step', id);
  },
  remove_split_step: async (id: string) => {
    logger.info('bridge:remove_split_step');
    const res = await ipcRenderer.invoke('remove_split_step', id);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  update_run_session_split: async (data: Partial<IRunSessionSplit>) => {
    logger.info('bridge:update_run_session_split');
    const res = await ipcRenderer.invoke('update_run_session_split', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  get_run_session_split_collection: async (id: string) => {
    logger.info('bridge:get_run_session_split_collection');
    return await ipcRenderer.invoke('get_run_session_split_collection', id);
  },
  update_run_scenario: async (data: Partial<IRunScenario>) => {
    logger.info('bridge:update_run_scenario');
    const res = await ipcRenderer.invoke('update_run_scenario', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  remove_run_scenario: async (id: string): Promise<number> => {
    logger.info('bridge:remove_run_scenario');
    const res = await ipcRenderer.invoke('remove_run_scenario', id);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  get_app_setting: async (): Promise<IAppSetting> => {
    logger.info('bridge:get_app_setting');
    return await ipcRenderer.invoke('get_app_setting');
  },
  update_app_setting: async (data: Partial<IAppSetting>) => {
    logger.info('bridge:update_app_setting');
    const res = await ipcRenderer.invoke('update_app_setting', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  create_run_scenario: async (data: Partial<IRunScenario>) => {
    logger.info('bridge:create_run_scenario');
    const res = await ipcRenderer.invoke('create_run_scenario', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  get_all_run_scenarios: async () => {
    logger.info('bridge:get_all_run_scenarios');
    return await ipcRenderer.invoke('get_all_run_scenarios');
  },
  update_split_collection: async (data: Partial<ISplitCollection>) => {
    logger.info('bridge:update_split_collection');
    const res = await ipcRenderer.invoke('update_split_collection', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  create_split_collection: async (data: Partial<ISplitCollection>) => {
    logger.info('bridge:create_split_collection');
    const res = await ipcRenderer.invoke('create_split_collection', data);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  remove_split_collection: async (id: string) => {
    logger.info('bridge:remove_split_collection');
    const res = await ipcRenderer.invoke('remove_split_collection', id);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  restart_run: async () => {
    logger.info('bridge:restart_run');
    const res = await ipcRenderer.invoke('restart_run');
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  record_session_as_pb: async (): Promise<void> => {
    logger.info('bridge:record_session_as_pb');
    const res = await ipcRenderer.invoke('record_session_as_pb');
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  start_timer: async (time: number): Promise<void> => {
    logger.info('bridge:start_timer');
    const res = await ipcRenderer.invoke('start_timer', time);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  stop_timer: async (time: number): Promise<void> => {
    logger.info('bridge:stop_timer');
    const res = await ipcRenderer.invoke('stop_timer', time);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  // helpers
  set_active_split: async (id: string, dateTime: number): Promise<ISplitStep> => {
    logger.info('bridge:set_active_split');
    const res = await ipcRenderer.invoke('set_active_split', id, dateTime);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  get_split_page_data: async (id: string): Promise<ISplitPageData> => {
    logger.info('bridge:get_split_page_data');
    return await ipcRenderer.invoke('get_split_page_data', id);
  },
  swap_split_order: async (params: ISwapSplitOrderParams) => {
    logger.info('bridge:swap_split_order');
    const res = await ipcRenderer.invoke('swap_split_order', params);
    ipcRenderer.invoke('notify_splits_change').catch(console.error);
    return res;
  },
  get_all_split_collections: async () => {
    logger.info('bridge:get_all_split_collections');
    return await ipcRenderer.invoke('get_all_split_collections');
  },

  // non api
  on_event_shortcut_invoked: (fn: (eventType: string, e?: IEventShortcut) => void) => {
    logger.info('bridge:on_event_shortcut_invoked');
    ipcRenderer.on('event_shortcut_invoked', function (event: IpcRendererEvent, eventType: string, e?: IEventShortcut) {
      logger.info('bridge:ipcRenderer');
      fn(eventType, e);
    });
  },
  on_active_viewer_settings_change: (fn: () => void) => {
    ipcRenderer.on('active_viewer_settings_change', function () {
      fn();
    });
  },
  on_event_shortcut_error: (fn: (e: IEventShortcutError) => void) => {
    logger.info('bridge:on_event_shortcut_error');
    ipcRenderer.on('event_shortcut_error', function (event: IpcRendererEvent, e: IEventShortcutError) {
      logger.info('bridge:ipcRenderer');
      fn(e);
    });
  },

  update_viewer_settings: async (name: string, opts: IViewerSettingsJson) => {
    logger.info('bridge:update_viewer_settings');
    return await ipcRenderer.invoke('update_viewer_settings', name, opts);
  },

  get_viewer_settings: async (name: string): Promise<IViewerSetting | null> => {
    logger.info('bridge:get_viewer_settings');
    return await ipcRenderer.invoke('get_viewer_settings', name);
  },

  get_obs_page_styles: async (): Promise<string> => {
    logger.info('bridge:get_obs_page_styles');
    return await ipcRenderer.invoke('get_obs_page_styles');
  },

  update_obs_page_styles: async (css: string): Promise<void> => {
    logger.info('bridge:update_obs_page_styles');
    return await ipcRenderer.invoke('update_obs_page_styles', css);
  },

  restart_display_server: async (): Promise<void> => {
    logger.info('bridge:restart_display_server');
    return await ipcRenderer.invoke('restart_display_server');
  },

  notify_page_settings_change: async (settings: Partial<IPageSettings>): Promise<void> => {
    logger.info('bridge:notify_page_settings_change');
    return await ipcRenderer.invoke('notify_page_settings_change', settings);
  },

  reset_hotkeys: async () => {
    logger.info('bridge:reset_hotkeys');
    return await ipcRenderer.invoke('reset_hotkeys');
  },

  request_preferences_window: async () => {
    logger.info('bridge:request_preferences_window');
    return await ipcRenderer.invoke('request_preferences_window');
  },

  try_set_global_shortcut: async (params: ITrySetGlobalShortcutParams) => {
    logger.info('bridge:try_set_global_shortcut');
    return ipcRenderer.invoke('try_set_global_shortcut', params);
  },
  open_choose_directory: async (opts: OpenDialogOptions) => {
    logger.info('bridge:open_choose_directory');
    return ipcRenderer.invoke('open_choose_directory', opts);
  },
  notify_set_active_split: async (id: string, dateTime: number) => {
    logger.info('bridge:notify_set_active_split');
    return await ipcRenderer.invoke('notify_set_active_split', id, dateTime);
  },
  notify_set_count_by_name: async (id: string, name: string, val: number): Promise<void> => {
    logger.info('bridge:notify_set_count_by_name');
    return ipcRenderer.invoke('notify_set_count_by_name', name, val);
  },
  async get_active_viewer_settings(): Promise<string[]> {
    return ipcRenderer.invoke('get_active_viewer_settings');
  },
}

/* show right click menu */
window.addEventListener('contextmenu', () => {
  // e.preventDefault()
  ipcRenderer.send('show-context-menu');
});

contextBridge.exposeInMainWorld('bridge', bridge);
