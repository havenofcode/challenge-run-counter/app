import {OpenDialogReturnValue, OpenDialogOptions} from 'electron';

import {
  IAppSetting,
  IRunScenario,
  IRunSession,
  IRunSessionSplit,
  IRunSessionSplitCollection,
  ISplitCollection,
  ISplitPageData,
  ISplitStep,
  IViewerSetting,
  IViewerSettingsJson,
} from './model';

export * from './model';

export interface IPaginatedParams {
  offset: number,
  limit: number,
}

export interface IPaginatedOneToManyParams extends IPaginatedParams {
  id: number;
}

export interface ITrySetGlobalShortcutParams {
  name: string;
  value: string;
}

export interface ISwapSplitOrderParams {
  fromId: number,
  toId: number,
}

export interface IPageSettings {
  width: string;
  height: string;
  css: string;
  viewerSettings: {
    [appName: string]: IViewerSettingsJson;
  };
}

export interface IEventShortcut {
  time?: number;
  pageSettings?: IPageSettings,
}

export interface IEventShortcutError {
  name: string;
  message: string;
}

export interface IBridge {
  get_split_collection: (id: string) => Promise<ISplitCollection | undefined>;
  get_run_session: (id: string) => Promise<IRunSession | undefined>;
  create_run_session: (data: Partial<IRunSession>) => Promise<IRunSession>;
  update_run_session: (data: Partial<IRunSession>) => Promise<IRunSession>;
  get_all_run_sessions: (params: IPaginatedOneToManyParams) => Promise<IRunSession[]>;
  get_run_scenario: (id: string) => Promise<IRunScenario | undefined>;

  create_split_step: (data: Partial<ISplitStep>) => Promise<ISplitStep>;
  update_split_step: (data: Partial<ISplitStep>) => Promise<ISplitStep>;
  update_all_split_steps: (data: Partial<ISplitStep>[]) => Promise<ISplitStep[]>;
  get_split_step: (id: string) => Promise<ISplitStep>;
  remove_split_step: (id: string) => Promise<ISplitStep>;

  update_run_session_split: (data: Partial<IRunSessionSplit>) => Promise<IRunSessionSplit>;

  get_run_session_split_collection: (id: string) => Promise<IRunSessionSplitCollection>;
  update_run_scenario: (data: Partial<IRunScenario>) => Promise<IRunScenario>;
  remove_run_scenario: (id: string) => Promise<number>;
  create_run_scenario: (data: Partial<IRunScenario>) => Promise<IRunScenario>;
  get_all_run_scenarios: () => Promise<IRunScenario[]>;

  update_app_setting: (data: Partial<IAppSetting>) => Promise<IAppSetting>;
  get_app_setting: () => Promise<IAppSetting>;

  update_split_collection: (data: Partial<ISplitCollection>) => Promise<ISplitCollection>;
  create_split_collection: (data: Partial<ISplitCollection>) => Promise<ISplitCollection>;
  remove_split_collection: (id: string) => Promise<number>;
  restart_run: () => Promise<void>;
  record_session_as_pb: () => Promise<void>;
  start_timer: (time: number) => Promise<void>;
  stop_timer: (time: number) => Promise<void>;

  set_active_split: (id: string, timeNow: number) => Promise<ISplitStep>;
  get_split_page_data: (id: string) => Promise<ISplitPageData>;
  on_event_shortcut_invoked: (fn: (e: string) => void) => void;
  on_active_viewer_settings_change: (fn: () => void) => void;
  on_event_shortcut_error: (fn: (e: IEventShortcutError) => void) => void;
  request_preferences_window: () => Promise<void>;
  reset_hotkeys: () => Promise<void>;
  try_set_global_shortcut: (params: ITrySetGlobalShortcutParams) => Promise<void>;
  swap_split_order: (params: ISwapSplitOrderParams) => Promise<void>;
  get_all_split_collections: () => Promise<ISplitCollection[]>;
  open_choose_directory: (opts: OpenDialogOptions) => Promise<OpenDialogReturnValue>;
  update_viewer_settings: (name: string, opts: IViewerSettingsJson) => Promise<void>;
  get_viewer_settings: (name: string) => Promise<IViewerSetting | null>;

  get_obs_page_styles(): Promise<string>;
  update_obs_page_styles(css: string): Promise<void>;
  restart_display_server(): Promise<void>;
  notify_page_settings_change(settings: Partial<IPageSettings>): Promise<void>;
  notify_set_active_split(id: string, timeNow: number): Promise<void>;
  notify_set_count_by_name(id: string, name: string, val: number): Promise<void>;
  get_active_viewer_settings(): Promise<string[]>;
}

declare global {
  interface Window {
    bridge: IBridge;
  }
}

export const getBridge = () => window.bridge;
