import path from 'path';
import winston from 'winston';
import {app} from 'electron';

const getLogger = () => {
  const basePath = app ?
    path.join(app.getPath('userData'), 'challenge_run_data') :
    path.resolve(__dirname, '..', '..', '..', '..');

  const errPath = path.join(basePath, 'error.log');
  const combinedPath = path.join(basePath, 'combined.log');

  const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
      new winston.transports.Console({format: winston.format.simple()}),
      new winston.transports.File({filename: errPath, level: 'error'}),
      new winston.transports.File({filename: combinedPath}),
    ],
  });

  return logger;
}

export const logger = getLogger();
