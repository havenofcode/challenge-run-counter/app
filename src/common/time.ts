import {IRunAttemptTimePoint} from '../common';

type SplitTimes = {[key: number]: IRunAttemptTimePoint[]}
type DurationMap = {[key: number]: number}

interface AccumulatedSplitDuration {
  total: number;
  splits: {[key: number]: number}
}

/**
 * Each time point `run_session_split` relation property must be already available
 *
 * @param runAttemptTimePoints time points
 */
export const accumulateSplitDurations = (runAttemptTimePoints: IRunAttemptTimePoint[]): AccumulatedSplitDuration => {
  let total = 0;
  let attemptDateStarted = 0;

  const splitTimes = runAttemptTimePoints.reduce<SplitTimes>((accumSplitTimes, timePoint) => {
    if (!timePoint.run_session_split) {
      throw new Error('run_session_split relation not available');
    }

    if (!accumSplitTimes[timePoint.run_session_split.split_step_id]) {
      accumSplitTimes[timePoint.run_session_split.split_step_id] = [];
    }

    switch (timePoint.event_type) {
      case 'attempt_started': {
        attemptDateStarted = timePoint.time;
        break;
      }
      case 'attempt_stopped': {
        total += timePoint.time - attemptDateStarted;
        break;
      }
    }

    accumSplitTimes[timePoint.run_session_split.split_step_id].push(timePoint);
    return accumSplitTimes;
  }, {});

  const splitKeys = Object.keys(splitTimes).map((n) => parseInt(n, 10));

  const durationMap = splitKeys.reduce<DurationMap>((accumDurationMap, id) => {
    let duration = 0;
    let splitDateStarted = 0;
    const timePoints = splitTimes[id];

    for (let i = 0; i < timePoints.length; ++i) {
      const timePoint: IRunAttemptTimePoint = timePoints[i];

      if (!timePoint.run_session_split) {
        throw new Error('run_session_split relation not available');
      }

      switch (timePoint.event_type) {
        case 'split_started': {
          splitDateStarted = timePoint.time;
          break;
        }
        case 'split_stopped': {
          duration += timePoint.time - splitDateStarted;
          break;
        }
      }
    }

    accumDurationMap[id] = duration;
    return accumDurationMap;
  }, {});

  return {
    total,
    splits: durationMap,
  }
}
