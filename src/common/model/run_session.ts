import {
  IRunScenario,
  IRunSessionSplit,
  IRunSessionSplitCollection
} from './';

export interface IRunSession {
  id: number;
  created_at: number;
  updated_at: number;
  run_scenario_id: number;
  current_session_split_id?: number;
  start_time?: number;
  end_time?: number;
  current_session_split?: IRunSessionSplit;
  run_scenario?: IRunScenario;
  run_session_splits?: IRunSessionSplit[];
  run_session_split_collections?: IRunSessionSplitCollection[];
  current_run_session_split_collection?: IRunSessionSplitCollection;
}
