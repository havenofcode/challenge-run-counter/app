import {AttemptDuration, IPageSettings} from '..';

export interface ISplitItem {
  id: number; // id of split step in database
  name: string; // id
  pb_hits?: number; // personal best recorded
  pb_time?: number; // personal best time
  hits_way: number; // current number of way hits
  hits_boss: number; // current number of boss hits
  active: boolean;
}

export interface IRoute {
  id: number;
  name: string;
  pb_time?: number;
  pb_hits?: number;
}

export interface ISplitsData {
  api_version: string;
  routes: IRoute[];
  activeRoute: IRoute;
  activeSplits: ISplitItem[];
  attemptDuration: AttemptDuration;
  bestSegments: ISplitItem[];
  pageSettings: IPageSettings;
}

export interface BaseViewerSetting {
  label: string;
  helperText?: string;
}

export interface TextViewerSetting extends BaseViewerSetting {
  type: "text";
}

export interface NumberViewerSetting extends BaseViewerSetting {
  type: "number";
  max?: number;
  min?: number;
}

export interface TextareaViewerSetting extends BaseViewerSetting {
  type: "textarea";
}

export interface FileViewerSetting extends BaseViewerSetting {
  type: "file";
  extensions?: string[];
}

export type IViewerSettingsConfigurationItem =
  TextViewerSetting |
  NumberViewerSetting |
  TextareaViewerSetting |
  FileViewerSetting;

export interface IViewerSettingsConfiguration {
  [key: string]: IViewerSettingsConfigurationItem;
}

export interface INotifyConfigurationAvailable {
  name: string;
  configuration: IViewerSettingsConfiguration;
}
