import {
  IRunSession,
  ISplitStep,
  ISplitCollection,
  IRunAttemptTimePoint,
} from './';

export interface IRunSessionSplitCollection {
  id: number;
  created_at: number;
  updated_at: number;
  run_session_id: number;
  current_split_step_id: number;
  split_collection_id: number;
  hits_boss: number;
  hits_way: number;
  time: number;
  start_time?: number;
  end_time?: number;
  run_session?: IRunSession;
  current_split_step?: ISplitStep;
  split_collection?: ISplitCollection;
  run_attempt_time_points?: IRunAttemptTimePoint[];
}
