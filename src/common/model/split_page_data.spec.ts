import {expect} from 'chai';
import {ISplitPageData} from './'

describe('ISplitPageData', () => {
  it('compiles', () => {
    const data = {runScenario: {id: 3}} as unknown as ISplitPageData;
    expect(data.runScenario.id).to.eql(3);
  });
});
