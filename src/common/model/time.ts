export interface SplitDuration {
  milliseconds: number;
  activeAt: number;
}

export interface AttemptDuration {
  attempt: number;
  attemptActiveAt: number;
  splits: {[key: number]: SplitDuration};
}
