import {
  IRunSessionSplitCollection,
  IRunSessionSplit,
} from './';

export type IRunAttemptTimeType = 'attempt_started' |
  'attempt_stopped' |
  'split_started' |
  'split_stopped';

export interface IRunAttemptTimePoint {
  id: number;
  created_at: number;
  updated_at: number;
  run_session_split_collection_id: number;
  run_session_split_id: number;
  event_type: IRunAttemptTimeType;
  time: number;
  run_session_split_collection?: IRunSessionSplitCollection;
  run_session_split?: IRunSessionSplit;
}
