import {
  IRunSession,
  ISplitStep,
  ISplitCollection,
  IRunSessionSplit,
  IRunSessionSplitCollection,
  IRunScenario,
} from './';

export interface ISplitPageSplit {
  splitStep: ISplitStep;
  data: IRunSessionSplit;
}

export interface ISplitPageCollection {
  splitCollection: ISplitCollection;
  data: IRunSessionSplitCollection;
  currentSplit: ISplitPageSplit;
}

export interface ISplitPageData {
  runScenario: IRunScenario;
  runSession: IRunSession;
  splitCollections: ISplitPageCollection[];
  currentSplitCollection: ISplitPageCollection;
  currentSplit: ISplitPageSplit;
  splits: ISplitPageSplit[];
  bestSegments: ISplitPageSplit[];
}
