import {
  IRunSession,
  ISplitStep,
} from './';

export interface IRunSessionSplit {
  id: number;
  created_at: number;
  updated_at: number;
  run_session_id: number;
  split_step_id: number;
  hits_way: number;
  hits_boss: number;
  time: number;
  split_step?: ISplitStep;
  run_session?: IRunSession;
}
