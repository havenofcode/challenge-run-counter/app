import {ISplitCollection, IRunSessionSplit} from './';

export interface ISplitStep {
  id: number;
  created_at: number;
  updated_at: number;
  name: string;
  deleted: number;
  order: number;
  pb_hits?: number;
  pb_time?: number;
  split_collection_id: number;
  split_collection?: ISplitCollection;
  run_session_splits?: IRunSessionSplit[];
}
