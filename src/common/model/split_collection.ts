import {} from './run_scenario';
import {} from './split_step';

import {
  IRunScenario,
  ISplitStep,
} from './'

export interface ISplitCollection {
  id: number;
  created_at: number;
  updated_at: number;
  name: string;
  run_scenario_id: number;
  deleted: number;
  pb_time: number;
  pb_hits: number;
  run_scenario?: IRunScenario;
  split_steps?: ISplitStep[];
}
