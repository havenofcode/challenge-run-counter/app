import {ISplitCollection, IRunSession} from './';

export interface IRunScenario {
  id: number;
  created_at: number;
  updated_at: number;
  name: string;
  deleted: number;
  split_collections?: ISplitCollection[];
  run_sessions?: IRunSession[];
}
