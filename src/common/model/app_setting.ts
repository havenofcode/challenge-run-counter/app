import {IRunScenario} from './';

export interface IAppSetting {
  id: number;
  created_at: number;
  updated_at: number;
  obs_browser_host: string;
  obs_browser_port: string;
  hotkey_next_split: string;
  hotkey_prev_split: string;
  hotkey_increment_boss_hit: string;
  hotkey_increment_way_hit: string;
  hotkey_decrement_boss_hit: string;
  hotkey_decrement_way_hit: string;
  hotkey_restart_run: string;
  hotkey_start_timer: string;
  hotkey_stop_timer: string;
  hotkey_record_pb: string;
  current_run_scenario_id: number;
  global_css_overrides: string;
  obs_page_width: string;
  obs_page_height: string;
  extra_obs_pages_directory?: string | null;
  current_run_scenario?: IRunScenario[];
}
