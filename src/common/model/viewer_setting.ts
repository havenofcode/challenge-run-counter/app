export type IViewerSettingsJson = {[key: string]: string | number | null}

export interface IViewerSetting {
  id: number;
  name: string;
  settings: string;
  settings_schema: string;
  updated_at: number;
  created_at: number;
}
