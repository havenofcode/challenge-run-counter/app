import {expect} from 'chai';
import {accumulateSplitDurations} from './time';
import {IRunAttemptTimePoint} from './';

const getTimePoint = (event_type: string, id: number, time: number) => ({
  event_type,
  time,
  run_session_split: {
    split_step_id: id,
  }
} as unknown as IRunAttemptTimePoint);

describe('time', () => {
  it('exists', () => {
    expect(accumulateSplitDurations).to.be.a('function');
  });

  it('accumulates times', () => {
    const timePoints = [
      getTimePoint('attempt_started', 1, 5),
      getTimePoint('split_started', 1, 5),
      getTimePoint('split_stopped', 1, 10),
      getTimePoint('attempt_stopped', 1, 10),
    ];

    const duration = accumulateSplitDurations(timePoints);
    expect(duration.total).to.eql(5);
    expect(duration.splits[1]).to.eql(5);
  });

  it('accumulates times', () => {
    const timePoints = [
      getTimePoint('attempt_started', 1, 5),
      getTimePoint('split_started', 1, 5),
      getTimePoint('split_stopped', 1, 10),
      getTimePoint('split_started', 2, 10),
      getTimePoint('split_stopped', 2, 15),
      getTimePoint('attempt_stopped', 2, 15),
    ];

    const duration = accumulateSplitDurations(timePoints);
    expect(duration.total).to.eql(10);
    expect(duration.splits[1]).to.eql(5);
    expect(duration.splits[2]).to.eql(5);
  });

  it('accumulates times', () => {
    const timePoints = [
      getTimePoint('attempt_started', 1, 5),
      getTimePoint('split_started', 1, 5),
      getTimePoint('split_stopped', 1, 10),
      getTimePoint('split_started', 2, 10),
      getTimePoint('split_stopped', 2, 15),
      getTimePoint('attempt_stopped', 2, 15),

      getTimePoint('attempt_started', 1, 5 + 100),
      getTimePoint('split_started', 1, 5 + 100),
      getTimePoint('split_stopped', 1, 10 + 100),
      getTimePoint('split_started', 2, 10 + 100),
      getTimePoint('split_stopped', 2, 15 + 100),
      getTimePoint('attempt_stopped', 2, 15 + 100),
    ];

    const duration = accumulateSplitDurations(timePoints);
    expect(duration.total).to.eql(20);
    expect(duration.splits[1]).to.eql(10);
    expect(duration.splits[2]).to.eql(10);
  });
});
