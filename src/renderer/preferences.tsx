import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@mui/material/CssBaseline';
import {ThemeProvider} from '@mui/material/styles';
import {PreferencesApp} from './preferences_app';
import {theme} from './theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <PreferencesApp />
  </ThemeProvider>,
  document.querySelector('#root'),
);

