import * as React from 'react';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import {styled, darken} from '@mui/material/styles';

const StyledTypography = styled(Typography)(
  ({theme}) => `
    color: ${theme.palette.primary.main};

    :hover {
      color: ${darken(theme.palette.primary.main, 0.2)};
    }
  `
);

export function ProTip(): React.ReactElement {
  return (
    <StyledTypography
      sx={{mt: 6, mb: 3}}
    >
      Pro tip: See more
      <Link href="https://mui.com/getting-started/templates/">templates</Link>
      on the MUI documentation.
    </StyledTypography>
  );
}

console.log("ok')")