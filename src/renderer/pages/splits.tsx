import * as React from 'react';
import {SplitsPageComponent} from '../components/splits_page';
import {getBridge} from '../../common';

import {
  IAppSetting,
} from '../../common/model';

interface IState {
  appSetting: IAppSetting | null;
  copyTextActive: boolean;
}

interface IProps {
  onRequestRoute: (pageRoute: string) => void;
  pageRoute: string;
}

export class SplitsPage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      appSetting: null,
      copyTextActive: false,
    }
  }

  async reload() {
    const bridge = getBridge();
    const appSetting = await bridge.get_app_setting();
    this.setState({appSetting});
  }

  async componentDidMount() {
    await this.reload();
  }

  async onLoadProfileRequest() {
    await this.reload();
  }

  async onRemoveProfile() {
    await this.reload();
  }

  renderSplitsPage() {
    if (!this.state.appSetting) {
      return null;
    }

    return (
      <SplitsPageComponent
        appSetting={this.state.appSetting}
        onRequestRoute={this.props.onRequestRoute}
        id={`${this.state.appSetting.current_run_scenario_id}`}
        onLoadProfileRequest={this.onLoadProfileRequest.bind(this)}
        onRemoveProfile={this.onRemoveProfile.bind(this)}
      />
    );
  }

  render() {
    return this.renderSplitsPage();
  }
}
