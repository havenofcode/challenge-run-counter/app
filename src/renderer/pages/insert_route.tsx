import * as React from 'react';

import {
  Box,
  Button,
  Container,
  TextField,
  Autocomplete,
  Stack,
  Paper,
} from '@mui/material';

import UrlPattern from 'url-pattern';

import {
  ISplitCollection,
  ISplitStep,
  getBridge,
} from '../../common';

import {Toolbar} from '../components/toolbar';

interface IState {
  value: string;
  inputValue: string;
  runScenarioId: number;
  splitCollections?: ISplitCollection[];
  previewSplits: ISplitStep[];
  options: string[];
}

interface IProps {
  onRequestRoute: (pageRoute: string) => void;
  pageRoute: string;
}

interface IRouteParams {
  runScenarioId: string;
}

export class InsertRoutePage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    const pattern = new UrlPattern('/insert-route/:runScenarioId');
    const params = pattern.match(props.pageRoute) as IRouteParams;

    if (!params) {
      throw new Error(`invalid route: ${props.pageRoute}`);
    }

    this.state = {
      runScenarioId: parseInt(params.runScenarioId, 10),
      value: '',
      inputValue: '',
      options: [],
      previewSplits: [],
    }
  }

  async componentDidMount() {
    const bridge = getBridge();
    const splitCollections = await bridge.get_all_split_collections();

    this.setState({
      splitCollections,
      options: splitCollections.map(({id, name}) => {
        return `${name}: ${id}`;
      }),
    });
  }

  async selectBoxDidChange() {
    const bridge = getBridge();
    const {inputValue, value} = this.state;
    if (inputValue !== '' && value !== '' && inputValue === value) {
      const parts = value.split(': ');
      const id = parts.pop() || '';
      const splitCollection = await bridge.get_split_collection(id);

      if (splitCollection && this.state.value === this.state.inputValue) {
        this.setState({previewSplits: splitCollection.split_steps || []});
      }
    } else {
      this.setState({previewSplits: []});
    }
  }

  async onCreateClick() {
    const bridge = getBridge();
    const {previewSplits} = this.state;

    if (previewSplits.length) {
      // using existing split collection?
      const {id} = await bridge.create_split_collection({
        name: `${this.state.inputValue} Copy`,
        run_scenario_id: this.state.runScenarioId,
      });

      const newSplitCollection = await bridge.get_split_collection(`${id}`);

      for (let i = 0; i < previewSplits.length; ++i) {
        const split: Partial<ISplitStep> = {...previewSplits[i]};
        delete split.id;
        split.split_collection_id =  id;
        await bridge.create_split_step(split);
      }

      if (
        newSplitCollection &&
        newSplitCollection.split_steps &&
        newSplitCollection.split_steps[0]
      ) {
        const splitStep = newSplitCollection.split_steps[0];
        await bridge.remove_split_step(`${splitStep.id}`);
      }

      this.props.onRequestRoute('/splits');
    } else {
      // create a new split collection
      await bridge.create_split_collection({
        name: this.state.inputValue,
        run_scenario_id: this.state.runScenarioId,
      });

      this.props.onRequestRoute('/splits');
    }
  }

  renderButtons() {
    const sx = {ml: 2}
    return (
      <Box>
        <Button
          sx={sx}
          variant="text"
          onClick={() => this.props.onRequestRoute('/splits')}
        >
          Cancel
        </Button>
        <Button
          sx={sx}
          variant="contained"
          onClick={() => this.onCreateClick()}
        >
          {this.getLabel()}
        </Button>
      </Box>
    )
  }

  getLabel() {
    const {value, inputValue} = this.state;
    if (value === '' && inputValue === '') {
      return 'Insert or Create New Route';
    }
    if (value === inputValue) {
      return 'Insert Existing Route'
    }
    return 'Create New Route';
  }

  renderAutocomplete() {
    return (
      <Autocomplete
        sx={{width: '100%'}}
        id="free-solo-demo"
        freeSolo
        options={this.state.options}
        onChange={(e, value) => {
          this.setState({value: value || ''}, () => this.selectBoxDidChange())
        }}
        onInputChange={(e, inputValue) => {
          this.setState({inputValue}, () => this.selectBoxDidChange());
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            label={this.getLabel()}
          />
        )}
      />
    );
  }

  renderSplitsIfExists() {
    if (!this.state.previewSplits.length) {
      return null;
    }


    return this.state.previewSplits.map(({name}, idx) => (
      <Paper key={`paper-key-${idx}`} sx={{p: 1, px: 2}} elevation={24}>
        {name}
      </Paper>
    ))
  }

  render() {
    if (!this.state.splitCollections) {
      return null;
    }

    return (
      <Container maxWidth={false} disableGutters={true}>
        <Toolbar pageTitle="Insert New Route" rightSideComponent={this.renderButtons()} />
        <Box sx={{width: 1}}>
          <Box
            sx={{
              display: 'flex',
              m: 2,
              flexGrow: 1,
            }}
          >
            {this.renderAutocomplete()}
          </Box>
          <Box sx={{mx: 2}}>
            <Stack spacing={2}>
              {this.renderSplitsIfExists()}
            </Stack>
          </Box>
        </Box>
      </Container>
    );
  }
}
