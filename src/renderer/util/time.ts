import {Duration} from 'luxon';

import {
  IRunAttemptTimePoint,
  AttemptDuration,
} from '../../common';

const addPadding = (num: string, addColon = true) => {
  const res = num.length === 1 ? `0${num}` : num;

  if (addColon) {
    return `${res}:`;
  }

  return res;
}

export const renderDurationStringFromMillis = (millis: number, showSign = false) => {
  const absMillis = Math.abs(millis);
  const sign = millis < 0 ? '- ' : '+ ';
  const obj = Duration.fromMillis(absMillis)
    .shiftTo('hours', 'minutes', 'seconds', 'milliseconds')
    .toObject();

  const hours = (obj.hours || 0) > 0 ? `${obj.hours}` : '';
  const minutes = `${obj.minutes}`;
  const seconds = `${obj.seconds}`;
  const fractionalSecond = Math.floor(obj.milliseconds ? obj.milliseconds / 10 : 0);
  const fract = `${fractionalSecond}`;
  return `${(millis && showSign) ? sign : ''}${hours}${hours.length > 0 ? ':' : ''}${hours.length > 0 ? addPadding(minutes, false) : minutes}:${addPadding(seconds, false)}.${addPadding(fract, false)}`;
}

export const getDuration = (timePoints: IRunAttemptTimePoint[]) => {
  const result: AttemptDuration = {
    attempt: 0,
    attemptActiveAt: 0,
    splits: {},
  }

  for (let i = 0; i < timePoints.length; ++i) {
    const timePoint = timePoints[i];
    const splitId = timePoint.run_session_split_id;

    switch (timePoint.event_type) {
      case 'attempt_started': {
        result.attemptActiveAt = timePoint.time;
        break;
      }
      case 'attempt_stopped': {
        result.attempt += timePoint.time - result.attemptActiveAt;
        result.attemptActiveAt = 0;
        break;
      }
      case 'split_started': {
        if (!result.splits[splitId]) {
          result.splits[splitId] = {
            activeAt: 0,
            milliseconds: 0,
          }
        }

        result.splits[splitId].activeAt = timePoint.time;
        break;
      }
      case 'split_stopped': {
        if (!result.splits[splitId]) {
          result.splits[splitId] = {
            activeAt: 0,
            milliseconds: 0,
          }
        }

        result.splits[splitId].milliseconds += timePoint.time - result.splits[splitId].activeAt;
        result.splits[splitId].activeAt = 0;
        break;
      }
    }
  }

  return result;
}
