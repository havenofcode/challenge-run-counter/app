export * from './time';

export const isNumber = (val: string) => {
  if (val >= '0' && val <= '9') {
    return true;
  }
  return false;
}
