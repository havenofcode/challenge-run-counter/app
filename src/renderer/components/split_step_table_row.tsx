import * as React from 'react';
import {css} from '@emotion/css';
import {renderDurationStringFromMillis} from '../util/time';

import {
  TableCell,
  TableRow,
  Input,
  Box,
  IconButton,
  Divider,
  Menu,
  MenuItem,
  ListItemText,
  ListItemIcon,
} from '@mui/material';

import {
  MoreVert,
  ArrowRight,
  Delete,
  ArrowUpward,
  ArrowDownward,
  AddCircle,
} from '@mui/icons-material';

import {
  IRunSession,
  IRunSessionSplit,
  IRunSessionSplitCollection,
  ISplitPageCollection,
  ISplitPageSplit,
  AttemptDuration,
} from '../../common/model';

import {SplitStepNumberInput} from './split_step_number_input';
import {getBridge} from '../../common';
import {Duration} from './duration';

export type SplitStepTableRowDragEvent = {
  dragImageData: string;
  splitData: ISplitPageSplit;
  e: React.MouseEvent<HTMLButtonElement> | MouseEvent;
};

export interface IProps {
  onChange: () => Promise<void>;
  onCreate: () => Promise<void>;
  onDelete: () => Promise<void>;
  onMoveDown: () => Promise<void>;
  onMoveUp: () => Promise<void>;
  runSession: IRunSession;
  runSessionSplit: IRunSessionSplit;
  runSessionSplitCollection: IRunSessionSplitCollection;
  splitData: ISplitPageSplit;
  currentSplitCollection: ISplitPageCollection;
  currentSplit: ISplitPageSplit;
  attemptDuration: AttemptDuration | null;
}

export interface IState {
  splitStepName: string;
  menuOpen: boolean;
  menuAnchorEl: HTMLElement | null;
}

export class SplitStepTableRow extends React.Component<IProps, IState> {
  state: IState = {
    splitStepName: this.props.splitData.splitStep.name,
    menuOpen: false,
    menuAnchorEl: null,
  };

  private rowRef = React.createRef<HTMLTableRowElement>();

  async onChangeRunSessionSplit(
    params: Partial<IRunSessionSplit>,
  ) {
    try {
      const bridge = getBridge();
      const {runSessionSplit, splitData} = this.props;

      const req = {
        split_step_id: splitData.splitStep.id,
        ...(this.props.runSession ? {run_session_id: this.props.runSession.id} : {}),
        ...(runSessionSplit ? {id: runSessionSplit.id} : {}),
        ...params,
      }

      await bridge.update_run_session_split(req);
      await this.props.onChange();
    } catch (err) {
      console.error(err);
    }
  }

  componentDidMount() {
    if (this.rowRef.current) {
      this.rowRef.current.tabIndex = 0;
    }
  }

  renderHitsWayColumn() {
    const bridge = getBridge();

    const onChange = async (value: string) => {
      const num = parseInt(value, 10);
      this.onChangeRunSessionSplit({hits_way: num});
      bridge.notify_set_count_by_name(`${this.props.splitData.data.id}`, 'hits_way', num);
    }

    return (
      <Box sx={{display: 'flex', flexDirection: 'row-reverse'}}>
        <SplitStepNumberInput
          sx={{width: '38px'}}
          onChange={onChange}
          value={`${this.props.runSessionSplit.hits_way || 0}`}
        />
      </Box>
    );
  }

  renderHitsBossColumn() {
    const bridge = getBridge();

    const onChange = async (value: string) => {
      const num = parseInt(value, 10);
      this.onChangeRunSessionSplit({ hits_boss: num});
      bridge.notify_set_count_by_name(`${this.props.splitData.data.id}`, 'hits_boss', num);
    }

    return (
      <Box sx={{display: 'flex', flexDirection: 'row-reverse'}}>
        <SplitStepNumberInput
          sx={{width: '38px'}}
          onChange={onChange}
          value={`${this.props.runSessionSplit.hits_boss || 0}`}
        />
      </Box>
    );
  }

  renderPbTimeColumn() {
    const {pb_time} = this.props.splitData.splitStep;
    const timeString = pb_time ? renderDurationStringFromMillis(pb_time) : '';

    return (
      <Box sx={{display: 'flex', flexDirection: 'row-reverse'}}>
        <Input
          sx={{
            width: '64px',
          }}
          value={timeString}
          disabled
          disableUnderline={true}
        />
      </Box>
    );
  }

  renderPbHitsColumn() {
    const bridge = getBridge();
    const {id, pb_hits} = this.props.splitData.splitStep;

    const onChange = async (value: string) => {
      bridge.update_split_step({id, pb_hits: parseInt(value, 10)});
      this.props.onChange();
    }

    return (
      <Box sx={{display: 'flex', flexDirection: 'row-reverse'}}>
        <SplitStepNumberInput
          sx={{width: '38px'}}
          onChange={onChange}
          value={`${pb_hits || 0}`}
        />
      </Box>
    );
  }

  getDiffValue() {
    const {pb_hits} = this.props.splitData.splitStep;
    const {hits_way, hits_boss} = (this.props.runSessionSplit || {})

    if (typeof pb_hits === 'number') {
      const val = (hits_way || 0) + (hits_boss || 0) - pb_hits;
      return `${val > 0 ? '+' : ''}${val}`;
    }

    return '?';
  }

  private async updateTitle() {
    const bridge = await getBridge();
    bridge.update_split_step({
      id: this.props.splitData.splitStep.id,
      name: this.state.splitStepName,
    });
    this.props.onChange();
  }

  onTitleChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({splitStepName: e.currentTarget.value});
    this.updateTitle();
  }

  renderDuration() {
    if (this.props.attemptDuration) {
      const id = this.props.splitData.data.id;
      const splitDuration = this.props.attemptDuration.splits[id];

      if (!splitDuration) {
        return null;
      }

      const {milliseconds, activeAt} = splitDuration;
      return (
        <Duration
          activeAt={activeAt}
          milliseconds={milliseconds}
        />
      );
    }
  }

  renderTitleColumn() {
    return (
      <Input
        className={this.s.titleColumnInput}
        sx={{
          whiteSpace: 'nowrap',
          width: 1,
        }}
        inputProps={{
          className: this.s.inputTextOverflow,
        }}
        onChange={this.onTitleChange.bind(this)}
        value={this.state.splitStepName}
        disableUnderline={true}
        placeholder={`name of split`}
      />
    );
  }

  getIsHighlighted() {
    const {splitStep} = this.props.splitData;
    const {currentSplit} = this.props;
    return (splitStep.id === currentSplit.splitStep.id);
  }

  renderIconColumn() {
    return (
      <Box sx={{
        position: 'relative',
        flexDirection: 'row',
        paddingLeft: '24px',
      }}>
        <Box sx={{
          position: 'absolute',
          top: '50%',
          left: '0',
          transform: 'translateY(-50%)',
        }}>
          {this.renderIcon()}
        </Box>
      </Box>
    );
  }

  renderActiveIcon() {
    return (
      <IconButton
        aria-label="drag"
        sx={{
          padding: 0,
        }}
      >
        <ArrowRight />
      </IconButton>
    );
  }

  renderIcon() {
    if (this.getIsHighlighted()) {
      return this.renderActiveIcon();
    }
  }

  async onRowFocus() {
    if (
      this.props.currentSplit.splitStep.id ===
      this.props.splitData.splitStep.id
    ) {
      return;
    }

    const bridge = getBridge();
    const timeNow = new Date().getTime();

    await Promise.all([
      bridge.set_active_split(`${this.props.splitData.splitStep.id}`, timeNow),
      bridge.notify_set_active_split(`${this.props.splitData.data.id}`, timeNow),
    ]);

    this.props.onChange();
  }

  onMenuClose() {
    this.setState({menuAnchorEl: null});
  }

  async onMenuMoveUp() {
    await this.props.onMoveUp();
    this.onMenuClose();
  }

  async onMenuMoveDown() {
    await this.props.onMoveDown();
    this.onMenuClose();
  }

  async onMenuDelete() {
    await this.props.onDelete();
  }

  async onMenuCreate() {
    await this.props.onCreate();
    this.onMenuClose();
  }

  renderMenu() {
    if (!this.state.menuAnchorEl) {
      return null;
    }

    return (
      <Menu
        anchorEl={this.state.menuAnchorEl}
        open={Boolean(this.state.menuAnchorEl)}
        onClose={this.onMenuClose.bind(this)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <MenuItem onClick={this.onMenuMoveUp.bind(this)}>
          <ListItemIcon>
            <ArrowUpward fontSize="small" />
          </ListItemIcon>
          <ListItemText>Move Up</ListItemText>
        </MenuItem>
        <MenuItem onClick={this.onMenuMoveDown.bind(this)}>
          <ListItemIcon>
            <ArrowDownward fontSize="small" />
          </ListItemIcon>
          <ListItemText>Move Down</ListItemText>
        </MenuItem>
        <Divider />
        <MenuItem onClick={this.onMenuCreate.bind(this)}>
          <ListItemIcon>
            <AddCircle fontSize="small" />
          </ListItemIcon>
          <ListItemText>Create</ListItemText>
        </MenuItem>
        <MenuItem onClick={this.onMenuDelete.bind(this)}>
          <ListItemIcon>
            <Delete fontSize="small" />
          </ListItemIcon>
          <ListItemText>Delete</ListItemText>
        </MenuItem>
      </Menu>
    );
  }

  onMenuButtonClick(e: React.MouseEvent<HTMLElement>) {
    this.setState({
      menuAnchorEl: e.currentTarget,
    });
  }

  renderDiffTime() {
    if (!this.props.attemptDuration) {
      return null;
    }

    const id = this.props.splitData.data.id;
    const splitDuration = this.props.attemptDuration.splits[id];

    if (!splitDuration) {
      return null;
    }

    const pbTime = this.props.splitData.splitStep.pb_time;
    const {milliseconds} = splitDuration;

    if (!milliseconds || !pbTime) {
      return null;
    }

    return renderDurationStringFromMillis(milliseconds - pbTime, true);
  }

  private s = {
    rowColumn: css`
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    `,
    titleColumnInput: css`
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    `,
    inputTextOverflow: css`
      text-overflow: ellipsis;
    `,
  }

  render() {
    const isHighlighted = this.getIsHighlighted();
    const sxAfterTitle = {pr: 2}
    const {s} = this;

    return (
      <TableRow
        hover={true}
        selected={isHighlighted}
        ref={this.rowRef}
        onFocus={this.onRowFocus.bind(this)}
      >
        <TableCell className={s.rowColumn} padding="none">
          {this.renderIcon()}
        </TableCell>
        <TableCell className={s.rowColumn} padding="none">
          {this.renderTitleColumn()}
        </TableCell>
        <TableCell className={s.rowColumn} sx={sxAfterTitle} padding="none" align="right">
          {this.renderDuration()}
        </TableCell>
        <TableCell className={s.rowColumn} sx={sxAfterTitle} padding="none" align="right">
          {this.renderDiffTime()}
        </TableCell>
        <TableCell className={s.rowColumn} sx={sxAfterTitle} padding="none" align="right">{this.renderHitsBossColumn()}</TableCell>
        <TableCell className={s.rowColumn} sx={sxAfterTitle} padding="none" align="right">{this.renderHitsWayColumn()}</TableCell>
        <TableCell className={s.rowColumn} sx={sxAfterTitle} padding="none" align="right">{this.getDiffValue()}</TableCell>
        <TableCell className={s.rowColumn} sx={sxAfterTitle} padding="none" align="right">{this.renderPbTimeColumn()}</TableCell>
        <TableCell className={s.rowColumn} sx={sxAfterTitle} padding="none" align="right">{this.renderPbHitsColumn()}</TableCell>
        <TableCell className={s.rowColumn} sx={sxAfterTitle} padding="none" align="right">
          <IconButton onClick={this.onMenuButtonClick.bind(this)}>
            <MoreVert />
          </IconButton>
          {this.renderMenu()}
        </TableCell>
      </TableRow>
    );
  }
}
