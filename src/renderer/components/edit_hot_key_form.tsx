import * as React from 'react';
import {WithTheme, withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {css, cx} from '@emotion/css';
import {IAppSetting} from '../../common/model';
import {getBridge} from '../../common/bridge';

import {
  Box,
  TextField,
  IconButton,
} from '@mui/material';

import {
  Delete,
} from '@mui/icons-material';

interface IProps extends WithTheme<Theme> {
  appSetting: IAppSetting;
  hotKeyName: string;
  onAppSettingsChanged: () => Promise<void>;
  theme: Theme,
}

interface IState {
  hotKeyValue: string;
  hotkeyLabel: string;
  editing: boolean;
  errMessage: string;
  focused: boolean;
}

class EditHotKeyFormImpl extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    const state: IState = {
      hotKeyValue: '',
      errMessage: '',
      hotkeyLabel: '',
      editing: false,
      focused: false,
    }

    const {appSetting} = props;

    switch (this.props.hotKeyName) {
      case 'next_split': {
        state.hotkeyLabel = 'Next Split';
        state.hotKeyValue = appSetting.hotkey_next_split;
        break;
      }
      case 'prev_split': {
        state.hotkeyLabel = 'Previous Split';
        state.hotKeyValue = appSetting.hotkey_prev_split;
        break;
      }
      case 'increment_boss_hit': {
        state.hotkeyLabel = 'Increment Boss Hit';
        state.hotKeyValue = appSetting.hotkey_increment_boss_hit;
        break;
      }
      case 'decrement_boss_hit': {
        state.hotkeyLabel = 'Decrement Boss Hit';
        state.hotKeyValue = appSetting.hotkey_decrement_boss_hit;
        break;
      }
      case 'decrement_way_hit': {
        state.hotkeyLabel = 'Decrement Way Hit';
        state.hotKeyValue = appSetting.hotkey_decrement_way_hit;
        break;
      }
      case 'increment_way_hit': {
        state.hotkeyLabel = 'Increment Way Hit';
        state.hotKeyValue = appSetting.hotkey_increment_way_hit;
        break;
      }
      case 'restart_run': {
        state.hotkeyLabel = 'Restart Run';
        state.hotKeyValue = appSetting.hotkey_restart_run;
        break;
      }
      case 'start_timer': {
        state.hotkeyLabel = 'Start Timer';
        state.hotKeyValue = appSetting.hotkey_start_timer;
        break;
      }
      case 'stop_timer': {
        state.hotkeyLabel = 'Stop Timer';
        state.hotKeyValue = appSetting.hotkey_stop_timer;
        break;
      }
      case 'record_pb': {
        state.hotkeyLabel = 'Record PB';
        state.hotKeyValue = appSetting.hotkey_record_pb;
        break;
      }
      default: {
        throw new Error('hotkey does not exist');
      }
    }

    this.state = state;
  }

  onKeyDownInput(e: React.KeyboardEvent<HTMLInputElement>) {
    e.preventDefault();

    if (e.repeat) {
      return;
    }

    let key = '';

    console.log('e.code', e.code);










    /**
    * - DOM_KEY_LOCATION_STANDARD: 0x00
    *   - The key described by the event is not identified as being located in a
    *     particular area of the keyboard
    * - DOM_KEY_LOCATION_LEFT	0x01
    *   - The key is one which may exist in multiple locations on the keyboard and,
    *     in this instance, is on the left side of the keyboard. Examples
    *     include the left Control key, the left Command key on a Macintosh
    *     keyboard, or the left Shift key.
    * - DOM_KEY_LOCATION_RIGHT: 0x02
    *   - The key is one which may exist in multiple positions on the keyboard
    *     and, in this case, is located on the right side of the keyboard.
    * - DOM_KEY_LOCATION_NUMPAD: 0x03
    *   - The key is located on the numeric keypad
    *
    * num0 - num9
    * numdec - decimal key
    * numadd - numpad + key
    * numsub - numpad - key
    * nummult - numpad * key
    * numdiv - numpad ÷ key
    *
    * For all keys on on the numpad, we need to change to use electron's keycode
    * identifier for accelerators.
    */
    switch (e.code) {
      case 'NumLock': {
        key = 'NumLock';
        break;
      }
      case 'NumpadDivide': {
        key = 'numdiv'
        break;
      }
      case 'NumpadMultiply': {
        key = 'nummult';
        break;
      }
      case 'NumpadSubtract': {
        key = 'numsub';
        break;
      }
      case 'Numpad7': {
        key = 'num7';
        break;
      }
      case 'Numpad8': {
        key = 'num8';
        break;
      }
      case 'Numpad9': {
        key = 'num9';
        break;
      }
      case 'NumpadAdd': {
        key = 'numadd';
        break;
      }
      case 'Numpad4': {
        key = 'num4';
        break;
      }
      case 'Numpad5': {
        key = 'num5';
        break;
      }
      case 'Numpad6': {
        key = 'num6';
        break;
      }
      case 'Numpad1': {
        key = 'num1';
        break;
      }
      case 'Numpad2': {
        key = 'num2';
        break;
      }
      case 'Numpad3': {
        key = 'num3';
        break;
      }
      case 'NumpadEnter': {
        key = 'Enter';
        break;
      }
      case 'Numpad0': {
        key = 'num0';
        break;
      }
      case 'NumpadDecimal': {
        key = 'numdec';
        break;
      }
      default: {
        switch (e.key) {
          case 'ArrowUp': {
            key = 'Up';
            break;
          }
          case 'ArrowDown': {
            key = 'Down';
            break;
          }
          case 'ArrowLeft': {
            key = 'Left';
            break;
          }
          case 'ArrowRight': {
            key = 'Right';
            break;
          }
          case '+': {
            key = 'Plus';
            break;
          }
          default: {
            key = e.key;
            break;
          }
        }
      }
    }

    if (!this.state.editing) {
      this.setState({
        editing: true,
        hotKeyValue: key,
      });
    } else {
      this.setState({
        hotKeyValue: `${this.state.hotKeyValue}+${key}`,
      });
    }
  }

  async onHotKeySaved() {
    const bridge = getBridge();
    const {state} = this;

    const name = `hotkey_${this.props.hotKeyName}`;
    try {
      await bridge.try_set_global_shortcut({name, value: state.hotKeyValue});
      await this.props.onAppSettingsChanged();
      this.setState({
        errMessage: '',
      });
    } catch (err) {
      this.setState({
        errMessage: `unable to use ${state.hotKeyValue}`,
      });
    }
  }

  async onKeyUpInput() {
    if (this.state.editing === true) {
      await this.onHotKeySaved();
    }

    this.setState({editing: false});
  }

  getLabel() {
    if (this.state.errMessage !== '') {
      return 'Try again';
    }

    if (this.state.focused) {
      return `Recording ${this.state.hotkeyLabel}`;
    }

    return this.state.hotkeyLabel;
  }

  onInputFocus() {
    this.setState({
      focused: true,
    });
  }

  onInputBlur() {
    this.setState({
      focused: false,
    });
  }

  async onDelete() {
    const bridge = getBridge();
    const name = `hotkey_${this.props.hotKeyName}`;
    await bridge.try_set_global_shortcut({name, value: ''});
    await this.props.onAppSettingsChanged();
    this.setState({hotKeyValue: '', errMessage: ''});
  }

  private s = {
    inputWrapper: css`
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      position: relative;
    `,
    iconButton: css`
      position: absolute;
      right: 3px;
      top: 20px;
      transform: translateY(-50%);
    `,
  }

  renderIconButton() {
    const {s} = this;
    if (this.state.focused) {
      return null;
    }

    if (this.state.hotKeyValue === '') {
      return null;
    }

    return (
      <IconButton size="small" onClick={this.onDelete.bind(this)} className={cx(s.iconButton)}>
        <Delete />
      </IconButton>
    );
  }

  renderColor() {
    if (this.state.hotKeyValue !== '' && !this.state.focused) {
      return 'success';
    }
  }

  getIsFocused() {
    return this.state.focused || this.state.hotKeyValue !== '';
  }

  render() {
    const {s} = this;
    return (
      <Box sx={{
        display: 'flex',
        flexDirection: 'row',
        p: 1
      }}>
        <Box sx={{width: 1}} className={cx(s.inputWrapper)}>
          <TextField
            size="small"
            fullWidth
            label={this.getLabel()}
            helperText={this.state.errMessage}
            error={this.state.errMessage !== ''}
            color={this.renderColor()}
            value={this.state.hotKeyValue}
            variant="outlined"
            onKeyDown={this.onKeyDownInput.bind(this)}
            onKeyUp={this.onKeyUpInput.bind(this)}
            onFocus={this.onInputFocus.bind(this)}
            onBlur={this.onInputBlur.bind(this)}
            focused={this.getIsFocused()}
          />
          {this.renderIconButton()}
        </Box>
      </Box>
    );
  }
}

export const EditHotKeyForm = withTheme<Theme, typeof EditHotKeyFormImpl>(EditHotKeyFormImpl);
