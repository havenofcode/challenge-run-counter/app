import * as React from 'react';
import {withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';

import {
  ListItemIcon,
  ListItemText,
  Box,
  Input,
  List,
  ListItemButton,
  MenuItem,
  IconButton,
  Menu,
} from '@mui/material';

import {
  AddCircle,
  Delete,
  MoreVert,
  Close,
  FolderOpen,
  EmojiEvents,
  Timer,
  TimerOff,
  RestartAlt,
  Settings,
} from '@mui/icons-material';

import {css, cx} from '@emotion/css';

import {
  IRunScenario,
  IRunSession,
  ISplitPageData,
} from '../../common/model';

import {
  getBridge,
} from '../../common/bridge';

export interface IState {
  runScenarios: IRunScenario[];
  menuAnchorEl: HTMLElement | null;
  runScenarioName: string;
  menuOpen: boolean;
}

export interface IProps {
  onRequestRoute: (pageRoute: string) => void;
  onLoadProfileRequest: () => Promise<void>;
  onRemoveProfile: () => Promise<void>;
  onSessionChange: () => Promise<void>;
  selectedRunScenario: IRunScenario;
  runSession?: IRunSession;
  theme: Theme;
  splitPageData: ISplitPageData;
}

class RunScenarioDropDownImpl extends React.Component<IProps, IState> {
  state: IState = {
    runScenarios: [],
    menuAnchorEl: null,
    menuOpen: false,
    runScenarioName: (this.props.selectedRunScenario ? this.props.selectedRunScenario.name : ''),
  };

  private anchorEl = React.createRef<HTMLDivElement>();

  private s = {
    inputWrapper: css`
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
    `,
    inputMoreButton: css`
      flex-grow: 0;
    `,
    input: css`
      flex-grow: 1;
      padding-left: ${this.props.theme.spacing(1)};
      padding-right: ${this.props.theme.spacing(1)};
    `,
    wrapper: css`
      margin-top: ${this.props.theme.spacing(1)};
      width: 100%;
      position: relative;
    `,
    menu: css`
      width: 100%;
      position: absolute;
      top: 100%;
      left: 0;
      background-color: ${this.props.theme.palette.primary.contrastText};
      z-index: 1;
      border-bottom: 1px solid #000;
    `,
  }

  onMoreMenuClose() {
    this.setState({menuAnchorEl: null});
  }

  async onChangeRunScenarioClick(runScenario: IRunScenario) {
    const bridge = getBridge();
    await bridge.update_app_setting({
      id: 1,
      current_run_scenario_id: runScenario.id,
    });
    this.setState({
      menuOpen: false,
    })
    this.props.onLoadProfileRequest();
  }

  componentDidUpdate(prevProps: IProps) {
    const {selectedRunScenario} = this.props;

    if (
      prevProps.selectedRunScenario &&
      prevProps.selectedRunScenario.id !== selectedRunScenario.id
    ) {
      this.setState({runScenarioName: selectedRunScenario.name});
    }
  }

  renderRunScenarioMenuOptions() {
    return this.state.runScenarios.map((runScenario) => (
      <ListItemButton
        key={`load-run-scenario-${runScenario.id}`}
        onClick={() => this.onChangeRunScenarioClick(runScenario)}
      >
        <ListItemIcon>
          <FolderOpen fontSize="small" />
        </ListItemIcon>
        <ListItemText>{runScenario.name}</ListItemText>
      </ListItemButton>
    ));
  }

  renderMenu() {
    const {s} = this;

    if (!this.state.menuOpen) {
      return;
    }

    return (
      <Box
        className={cx(s.menu)}
      >
        <List component="nav">
          {this.renderRunScenarioMenuOptions()}
        </List>
      </Box>
    );
  }

  async updateRunScenarioName() {
    const bridge = getBridge();
    await bridge.update_run_scenario({
      id: this.props.selectedRunScenario.id,
      name: this.state.runScenarioName,
    });
  }

  async onChangeInput(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({runScenarioName: e.currentTarget.value});
  }

  async onKeyPressInput(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      await this.updateRunScenarioName();
    }
  }

  async onBlurInput() {
    await this.updateRunScenarioName();
  }

  // load the first dropdown
  onMoreClick(e: React.MouseEvent<HTMLButtonElement>) {
    this.setState({
      menuAnchorEl: e.currentTarget,
    });
  }

  // close load profile list
  onMoreClose() {
    this.setState({menuOpen: false});
  }

  // open load profile list
  async onMenuLoadProfile() {
    const bridge = getBridge();
    const runScenarios = await bridge.get_all_run_scenarios();

    this.setState({
      menuOpen: true,
      menuAnchorEl: null,
      runScenarios,
    });
  }

  async onMenuCreate() {
    const bridge = getBridge();

    const runScenario = await bridge.create_run_scenario({
      name: '',
    });

    await bridge.update_app_setting({
      id: 1,
      current_run_scenario_id: runScenario.id,
    });

    await this.props.onLoadProfileRequest();
  }

  async onMenuRecordAsPB() {
    const bridge = getBridge();
    await bridge.record_session_as_pb();
    await this.props.onLoadProfileRequest();
  }

  async onStartTimer() {
    const bridge = getBridge();
    const {runSession} = this.props;

    if (runSession) {
      try {
        await bridge.start_timer((new Date()).getTime());
        await this.props.onSessionChange();
      } catch (err) {
        // todo handle
        console.error(err);
      }
    }

    this.setState({menuAnchorEl: null});
  }

  async onStopTimer() {
    const bridge = getBridge();
    const {runSession} = this.props;

    if (runSession) {
      try {
        await bridge.stop_timer((new Date()).getTime());
        await this.props.onSessionChange();
      } catch (err) {
        // todo handle error
        console.error(err);
      }
    }

    this.setState({menuAnchorEl: null});
  }

  renderStartStopTime() {
    const {currentSplitCollection} = this.props.splitPageData;
    const timePoints = currentSplitCollection.data.run_attempt_time_points;

    if (!timePoints) {
      return null;
    }

    let started = false;

    for (let i = timePoints.length - 1; i >= 0; --i) {
      const timePoint = timePoints[i];

      if (timePoint.event_type === 'attempt_started') {
        started = true;
        break;
      }

      if (timePoint.event_type === 'attempt_stopped') {
        break;
      }
    }

    if (started) {
      return (
        <MenuItem onClick={this.onStopTimer.bind(this)}>
          <ListItemIcon>
            <TimerOff fontSize="small" />
          </ListItemIcon>
          <ListItemText>Stop Timer</ListItemText>
        </MenuItem>
      );
    }

    return (
      <MenuItem onClick={this.onStartTimer.bind(this)}>
        <ListItemIcon>
          <Timer fontSize="small" />
        </ListItemIcon>
        <ListItemText>Start Timer</ListItemText>
      </MenuItem>
    );
  }

  async onMenuRestartRun() {
    const bridge = getBridge();
    await bridge.restart_run();

    this.setState({
      menuAnchorEl: null,
    });

    await this.props.onSessionChange();
  }

  async onMenuDelete() {
    this.props.onRemoveProfile();
    this.setState({
      menuAnchorEl: null,
    });
  }

  async onMenuPreferences() {
    const bridge = getBridge();
    await bridge.request_preferences_window();
  }

  onInsertExistingRoute() {
    this.props.onRequestRoute(`/insert-route/${this.props.selectedRunScenario.id}`);
  }

  renderMoreMenu() {
    if (!this.state.menuAnchorEl) {
      return null;
    }

    return (
      <Menu
        anchorEl={this.state.menuAnchorEl}
        open={Boolean(this.state.menuAnchorEl)}
        onClose={this.onMoreMenuClose.bind(this)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <MenuItem onClick={this.onMenuLoadProfile.bind(this)}>
          <ListItemIcon>
            <FolderOpen fontSize="small" />
          </ListItemIcon>
          <ListItemText>Open</ListItemText>
        </MenuItem>
        <MenuItem onClick={this.onMenuRestartRun.bind(this)}>
          <ListItemIcon>
            <RestartAlt fontSize="small" />
          </ListItemIcon>
          <ListItemText>Restart Run</ListItemText>
        </MenuItem>
        {this.renderStartStopTime()}
        <MenuItem onClick={this.onInsertExistingRoute.bind(this)}>
          <ListItemIcon>
            <RestartAlt fontSize="small" />
          </ListItemIcon>
          <ListItemText>Insert Route</ListItemText>
        </MenuItem>
        <MenuItem onClick={this.onMenuRecordAsPB.bind(this)}>
          <ListItemIcon>
            <EmojiEvents fontSize="small" />
          </ListItemIcon>
          <ListItemText>Record as PB</ListItemText>
        </MenuItem>
        <MenuItem onClick={this.onMenuCreate.bind(this)}>
          <ListItemIcon>
            <AddCircle fontSize="small" />
          </ListItemIcon>
          <ListItemText>Create</ListItemText>
        </MenuItem>
        <MenuItem onClick={this.onMenuDelete.bind(this)}>
          <ListItemIcon>
            <Delete fontSize="small" />
          </ListItemIcon>
          <ListItemText>Delete</ListItemText>
        </MenuItem>
        <MenuItem onClick={this.onMenuPreferences.bind(this)}>
          <ListItemIcon>
            <Settings fontSize="small" />
          </ListItemIcon>
          <ListItemText>Preferences</ListItemText>
        </MenuItem>
      </Menu>
    );
  }

  renderMenuButton() {
    const {s} = this;

    if (this.state.menuOpen) {
      return (
        <IconButton
          className={cx(s.inputMoreButton)}
          onClick={this.onMoreClose.bind(this)}
        >
          <Close />
        </IconButton>
      );
    }

    return (
      <IconButton
        className={cx(s.inputMoreButton)}
        onClick={this.onMoreClick.bind(this)}
      >
        <MoreVert />
      </IconButton>
    );
  }

  render() {
    const {s} = this;

    return (
      <Box ref={this.anchorEl} className={cx(s.wrapper)}>
        <Box className={cx(s.inputWrapper)}>
          <Input
            placeholder="Profile Name"
            className={cx(s.input)}
            onKeyPress={this.onKeyPressInput.bind(this)}
            onBlur={this.onBlurInput.bind(this)}
            onChange={this.onChangeInput.bind(this)}
            value={this.state.runScenarioName}
            disableUnderline={true}
          />
          {this.renderMenuButton()}
          {this.renderMoreMenu()}
        </Box>
        {this.renderMenu()}
      </Box>
    );
  }
}

export const RunScenarioDropDown = withTheme<Theme, typeof RunScenarioDropDownImpl>(RunScenarioDropDownImpl);
