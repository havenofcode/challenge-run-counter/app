import * as React from 'react';

import {
  AppBar,
  Typography,
  Toolbar as MuiToolbar,
  IconButton,
  Box,
} from '@mui/material';

import {
  DirectionsRun
} from '@mui/icons-material';

import {css} from '@emotion/css';
import {ReactElement} from 'react';

interface IProps {
  pageTitle?: string;
  rightSideComponent?: ReactElement | null;
}

export class Toolbar extends React.Component<IProps> {
  render() {
    const {pageTitle} = this.props;
    return (
      <AppBar position="relative">
        <MuiToolbar>
          <Box sx={{display: 'flex', m: 0, flexDirection: 'row'}}>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="open drawer"
              sx={{mr: 0}}
              className={css`
                pointer-events: none;
              `}
            >
              <DirectionsRun />
            </IconButton>
          </Box>
          <Typography
            variant="h6"
            noWrap
            component="div"
          >
            {pageTitle || 'Challenge Run Counter'}
          </Typography>
          <Box sx={{display: 'flex', flexGrow: 1}}/>
          <Box sx={{display: 'flex'}}>
            {this.props.rightSideComponent}
          </Box>
        </MuiToolbar>
      </AppBar>
    );
  }
}
