import * as React from 'react';
import {WithTheme, withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {SxProps} from '@mui/system';
import {IAppSetting} from '../../common/model';
import {FileTextField} from './file_text_field';

import {
  getBridge,
  IViewerSetting,
  IViewerSettingsJson,
  NumberViewerSetting,
  IViewerSettingsConfiguration,
  IViewerSettingsConfigurationItem,
} from '../../common';

import {
  Box,
  Paper,
  TextField,
} from '@mui/material';

interface IProps extends WithTheme<Theme> {
  theme: Theme;
  appSetting: IAppSetting;
  onAppSettingsChanged: () => Promise<void>;
  viewerSetting: IViewerSetting;
  sx?: SxProps;
}

interface IState {
  viewerSetting: IViewerSetting;
}

class EditViewerOptionsFormImpl extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    const state: IState = {viewerSetting: {...props.viewerSetting}}
    this.state = state;
  }

  componentDidUpdate(prevProps: IProps) {
    if (prevProps.viewerSetting.id !== this.props.viewerSetting.id) {
      this.setState({
        viewerSetting: {...this.props.viewerSetting},
      });
    }
  }

  renderFileOption(opts: {
    optionConfig: IViewerSettingsConfigurationItem;
    label: string;
    optionName: string;
    value?: string | number | null,
    onChange?: (val: string) => void,
    textFieldType?: string;
    helperText?: string;
  }) {
    return (
      <FileTextField
        label={opts.label}
        value={typeof opts.value === 'string' ? opts.value : ''}
        helperText={opts.helperText}
        onChange={async (value: string) => {
          const {optionName} = opts;
          const settings: IViewerSettingsJson = {
            [optionName]: value,
          }

          const name = this.props.viewerSetting.name;
          const bridge = getBridge();
          await bridge.update_viewer_settings(name, settings);
          await bridge.notify_page_settings_change({
            viewerSettings: {
              [name]: settings,
            },
          });
        }}
      />
    );
  }

  renderOption(opts: {
    optionConfig: IViewerSettingsConfigurationItem;
    label: string;
    optionName: string;
    value?: string | number | null,
    onChange?: (val: string) => void,
    textFieldType?: string;
    helperText?: string;
  }) {
    if (opts.textFieldType === 'file') {
      return this.renderFileOption(opts);
    }

    return (
      <TextField
        error={false}
        fullWidth
        multiline={opts.optionConfig.type === 'textarea'}
        helperText={opts.helperText}
        label={opts.label}
        size="small"
        type={opts.textFieldType}
        variant="outlined"
        value={opts.optionConfig.type === 'file' ? '' : opts.value}
        onChange={async (e) => {
          const {value} = e.currentTarget;
          const {optionName} = opts;
          const settings: IViewerSettingsJson = {}

          switch (opts.textFieldType) {
            case 'number': {
              const config = opts.optionConfig as NumberViewerSetting;
              const newValue = parseInt(value, 10);

              if (typeof config.min === 'number' && config.min > newValue) {
                return;
              }

              if (typeof config.max === 'number' && config.max < newValue) {
                return;
              }

              settings[optionName] = newValue;
              break;
            }
            default: {
              settings[optionName] = value;
              break;
            }
          }

          const name = this.props.viewerSetting.name;
          const bridge = getBridge();
          await bridge.update_viewer_settings(name, settings);
          await bridge.notify_page_settings_change({
            viewerSettings: {
              [name]: settings,
            },
          });

          const updatedViewerSetting = await bridge.get_viewer_settings(name);

          if (updatedViewerSetting) {
            this.setState({
              viewerSetting: updatedViewerSetting,
            });
          }

          await this.props.onAppSettingsChanged();
        }}
      />
    );
  }

  render() {
    if (!this.state.viewerSetting) {
      return null;
    }

    const {settings, settings_schema} = this.state.viewerSetting;
    const viewerSettingsJson: IViewerSettingsJson = JSON.parse(settings || '{}');
    const viewerSettingsConfiguration: IViewerSettingsConfiguration = JSON.parse(settings_schema || '{}');

    return (
      <>
        <Paper
          sx={{
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
            flexGrow: 1,
            width: 1,
            padding: 0,
          }}
          elevation={12}
        >
          {Object.keys(viewerSettingsConfiguration).map((optionName) =>
            <Box
              key={optionName}
              sx={{
                display: 'flex',
                p: 1,
                width: 1,
                flexGrow: 1,
              }}
            >
              {this.renderOption({
                optionConfig: viewerSettingsConfiguration[optionName],
                label: viewerSettingsConfiguration[optionName].label,
                helperText: viewerSettingsConfiguration[optionName].helperText || undefined,
                optionName,
                value: viewerSettingsJson[optionName] || undefined,
                textFieldType: viewerSettingsConfiguration[optionName].type,
              })}
            </Box>
          )}
        </Paper>
      </>
    );
  }
}

export const EditViewerOptionsForm = withTheme<Theme, typeof EditViewerOptionsFormImpl>(EditViewerOptionsFormImpl);
