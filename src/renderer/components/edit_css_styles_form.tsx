import * as React from 'react';
import {WithTheme, withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {IAppSetting} from '../../common/model';
import {SxProps} from '@mui/system';
import {getBridge} from '../../common';
import styled from '@emotion/styled';

import {
  Box,
  Button,
  IconButton,
  Paper,
  TextField,
} from '@mui/material';

import {FileOpen} from '@mui/icons-material';

const HiddenInput = styled.input`
  display: none;
`;

interface IProps extends WithTheme<Theme> {
  theme: Theme;
  appSetting: IAppSetting;
  onAppSettingsChanged: () => Promise<void>;
  sx?: SxProps;
}

interface IState {
  globalCssOverridesValue: string;
  globalCssOverridesInitialContentValue: string;
  globalCssOverridesContentValue: string;
  error?: string;
}

class EditCssStylesFormImpl extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    const {appSetting} = props;

    const state: IState = {
      globalCssOverridesValue: appSetting.global_css_overrides,
      globalCssOverridesInitialContentValue: '',
      globalCssOverridesContentValue: '',
    }

    this.state = state;
  }

  async componentDidMount() {
    const bridge = getBridge();
    let css = '';

    try {
      css = await bridge.get_obs_page_styles();
    } catch (err) {
      const error = (err as Error) || new Error('unknown');
      console.warn('unable to read page styles', error.message);
    }

    this.setState({
      globalCssOverridesContentValue: css,
      globalCssOverridesInitialContentValue: css,
    });
  }

  async onSave() {
    const bridge = getBridge();
    try {
      const css = this.state.globalCssOverridesContentValue;

      await bridge.update_obs_page_styles(css);
      await bridge.notify_page_settings_change({css});
      await this.props.onAppSettingsChanged();
      this.setState({
        globalCssOverridesInitialContentValue: css,
      });
    } catch (err) {
      this.setState({
        error: (err && (err as Error).message) ? (err as Error).message : 'error saving'
      });
    }
  }

  renderSaveButton() {
    const {
      globalCssOverridesContentValue,
      globalCssOverridesInitialContentValue,
    } = this.state;

    if (
      globalCssOverridesContentValue === globalCssOverridesInitialContentValue
    ) {
      return null;
    }

    return (
      <Button
        size="small"
        onClick={(this.onSave.bind(this))}
      >
        Save
      </Button>
    );
  }

  readCssFile(file: File): Promise<string> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.addEventListener('load', (event) => {
        console.log('load reader event', event);

        if (event.target) {
          resolve(event.target.result as string);
        }

        reject(new Error('no target'));
      });

      reader.addEventListener('error', () => reject(new Error('error')));
      reader.readAsText(file);
    });
  }

  async handleFilesSelected(files: FileList) {
    const [file] = files;

    if (!file) {
      return null;
    }

    const css = await this.readCssFile(file);
    const bridge = getBridge();
    const path = (file as unknown as {path: string}).path;

    try {
      await bridge.update_app_setting({
        id: this.props.appSetting.id,
        global_css_overrides: path,
      });

      await bridge.notify_page_settings_change({css});
      this.props.onAppSettingsChanged();

      this.setState({
        globalCssOverridesContentValue: css,
        globalCssOverridesValue: path,
      });
    } catch (err) {
      this.setState({
        error: (err as Error).message,
      });
    }
  }

  renderOpenFileButton() {
    return (
      <>
        <HiddenInput
          accept="*"
          id="contained-button-file"
          type="file"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            if (e.target.files) {
              this.handleFilesSelected(e.target.files);
            }
          }}
        />
        <IconButton
          size="small"
        >
          <label htmlFor="contained-button-file">
            <FileOpen />
          </label>
        </IconButton>
      </>
    );
  }

  render() {
    return (
      <Paper
        sx={{
          width: 1,
          padding: 1.5,
          borderRadius: '15px',
          ...(this.props.sx || {})
        }}
        elevation={12}
      >
        <Box sx={{width: 1, position: 'relative'}}>
          <TextField
            size="small"
            fullWidth
            label="Path to css for browser"
            variant="outlined"
            value={this.state.globalCssOverridesValue}
          />
          <Box
            sx={{
              display: 'flex',
              position: 'absolute',
              right: '0',
              top: '50%',
              transform: 'translateY(-50%);',
            }}>
            {this.renderOpenFileButton()}
            {this.renderSaveButton()}
          </Box>
        </Box>

        <Box sx={{mt: 2, width: 1}}>
          <TextField
            minRows={4}
            size="small"
            fullWidth
            label="Add custom css for overriding browser page."
            error={Boolean(this.state.error)}
            variant="outlined"
            multiline={true}
            placeholder="placeholder"
            value={this.state.globalCssOverridesContentValue}
            onChange={(e) => this.setState({
              globalCssOverridesContentValue: e.currentTarget.value,
            })}
          />
        </Box>
      </Paper>
    );
  }
}

export const EditCssStylesForm = withTheme<Theme, typeof EditCssStylesFormImpl>(
  EditCssStylesFormImpl
);
