import * as React from 'react';

import {
  Input,
} from '@mui/material';

export interface IState {
  value: string;
}

export interface IProps {
  value: string;
  onChange: (value: string) => Promise<void>;
  onImmediateChange?: (value: string) => void;
  sx?: {[key: string]: string};
}

export class SplitStepNumberInput extends React.Component<IProps, IState> {
  state: IState = {
    value: this.props.value,
  };

  private onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    this.props.onChange(e.currentTarget.value);
    this.setState({value: e.currentTarget.value});
  }

  componentDidUpdate(prevProps: IProps) {
    if (prevProps.value != this.props.value) {
      this.setState({
        value: this.props.value,
      });
    }
  }

  render() {
    return (
      <Input
        {...this.props}
        type="number"
        onChange={this.onChangeHandler.bind(this)}
        value={this.state.value}
        disableUnderline={true}
        inputProps={{max: 99, min: 0}}
      />
    );
  }
}
