import * as React from 'react';
import {Box, TextField} from '@mui/material';
import {SxProps} from '@mui/system';
import {withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {isNumber} from '../util';

interface IProps {
  label: string;
  onChange?: (val: string) => void,
  optionName: string;
  sx?: SxProps;
  theme: Theme;
  value: string;
}

interface IState {
  value: string;
  errMessage: string;
}

export class EditCssNumericInputImpl extends React.Component<IProps, IState> {
  state: IState = {
    errMessage: '',
    value: this.props.value,
  }

  onChange(e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>) {
    const {value} = e.currentTarget;
    this.setState({value});

    if (this.props.onChange) {
      this.props.onChange(value);
    }
  }

  isNumberValue() {
    const {value} = this.state;
    for (let i = 0; i < value.length; ++i) {
      if (!isNumber(value.charAt(i))) {
        return false
      }
    }

    return true;
  }

  determineInputType() {
    if (this.state.value.trim().length > 0 && this.isNumberValue()) {
      return 'number';
    }
    return 'text';
  }

  render() {
    const opts = this.props;

    return (
      <Box
        key={`misc-option-${opts.optionName}`}
        sx={{
          display: 'flex',
          p: 1,
          width: 1 / 2,
          flexGrow: 1,
        }}
      >
        <Box
          sx={{
            width: 1,
            display: 'flex',
            flexDirection: 'row',
            flexGrow: 1,
            ...(this.props.sx || {}),
          }}
        >

          <TextField
            size="small"
            fullWidth
            type={this.determineInputType()}
            label={this.props.label}
            helperText={this.state.errMessage}
            error={this.state.errMessage !== ''}
            value={this.state.value}
            variant="outlined"
            onChange={this.onChange.bind(this)}
          />
        </Box>
      </Box>
    );
  }
}

export const EditCssNumericInput = withTheme<Theme, typeof EditCssNumericInputImpl>(EditCssNumericInputImpl);
