import * as React from 'react';
import {WithTheme, withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {SxProps} from '@mui/system';
import {IAppSetting} from '../../common/model';
import {EditObsUserPageDirectory} from './edit_obs_user_page_directory';
import {EditOptionText} from './edit_option_text';
import {getBridge} from '../../common';

import {
  Box,
  Paper,
} from '@mui/material';
import {EditCssNumericInput} from './edit_css_numeric_input';

interface IProps extends WithTheme<Theme> {
  theme: Theme;
  appSetting: IAppSetting;
  onAppSettingsChanged: () => Promise<void>;
  sx?: SxProps;
}

interface IState {
  value: string;
}

class EditMiscOptionsFormImpl extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    const state: IState = {
      value: '',
    }

    this.state = state;
  }

  renderOption(opts: {
    label: string;
    optionName: string;
    value: string,
    onChange?: (val: string) => void,
    textFieldType?: string;
  }) {
    return (
      <Box
        key={`misc-option-${opts.optionName}`}
        sx={{
          display: 'flex',
          p: 1,
          width: 1 / 2,
          flexGrow: 1,
        }}
      >
        <EditOptionText
          label={opts.label}
          optionName={opts.optionName}
          value={opts.value}
          onChange={opts.onChange}
          sx={{
            display: 'flex',
            flexGrow: 1,
            width: 1,
          }}
          textFieldType={opts.textFieldType}
        />
      </Box>
    )
  }

  render() {
    return (
      <>
        <Paper
          sx={{
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
            flexGrow: 1,
            width: 1,
            padding: 0,
          }}
          elevation={12}
        >
          {this.renderOption({
            label: 'Web Page Host',
            optionName: 'obs_browser_host',
            value: this.props.appSetting.obs_browser_host,
          })}

          {this.renderOption({
            label: 'Web Page Port',
            optionName: 'obs_browser_port',
            value: this.props.appSetting.obs_browser_port,
          })}

          <EditCssNumericInput
            label="Web Page Width"
            optionName="obs_page_width"
            value={this.props.appSetting.obs_page_width}
            onChange={async (val) => {
              const bridge = getBridge();
              await bridge.update_app_setting({
                id: 1,
                obs_page_width: val,
              });
              await this.props.onAppSettingsChanged();
              await bridge.notify_page_settings_change({
                width: val,
              });
            }}
          />

          <EditCssNumericInput
            label="Web Page Height"
            optionName="obs_page_height"
            value={this.props.appSetting.obs_page_height}
            onChange={async (val) => {
              const bridge = getBridge();
              await bridge.update_app_setting({
                id: 1,
                obs_page_height: val,
              });
              await this.props.onAppSettingsChanged();
              await bridge.notify_page_settings_change({
                height: val,
              });
            }}
          />

          <EditObsUserPageDirectory
            sx={{
              p: 1,
              width: 1,
              flexGrow: 1,
            }}
            appSetting={this.props.appSetting}
          />
        </Paper>
      </>
    );
  }
}

export const EditMiscOptionsForm = withTheme<Theme, typeof EditMiscOptionsFormImpl>(EditMiscOptionsFormImpl);
