import * as React from 'react';

import {
  ButtonBase,
  Input,
} from '@mui/material';

import {css, cx} from '@emotion/css';

import {getBridge} from '../../common';
import {ISplitCollection} from '../../common/model';

export interface IState {
  splitCollectionName: string;
}

export interface IProps {
  splitCollection: ISplitCollection;
  onChange: () => Promise<void>;
  className: string;
}

export class SelectedSplitCollectionTab extends React.Component<IProps, IState> {
  state: IState = {
    splitCollectionName: this.props.splitCollection.name,
  };

  async onSaveName() {
    if (this.state.splitCollectionName === this.props.splitCollection.name) {
      return;
    }

    const bridge = getBridge();
    await bridge.update_split_collection({
      id: this.props.splitCollection.id,
      name: this.state.splitCollectionName,
    });
    await this.props.onChange();
  }

  onChangeInput(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({splitCollectionName: e.currentTarget.value});
  }

  async onBlurInput() {
    await this.onSaveName();
  }

  async onKeyPressInput(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      await this.onSaveName();
    }
  }

  private s = {
    input: css`
      text-align: center;
      width: 100%;
    `,
  }

  render() {
    const {splitCollection} = this.props;
    const {s} = this;
    return (
      <ButtonBase
        className={this.props.className}
        key={`split-collection-tab-button-${splitCollection.id}`}
      >
        <Input
          className={cx(s.input)}
          inputProps={{style: {textAlign: 'center'}}}
          value={this.state.splitCollectionName}
          disableUnderline={true}
          placeholder="Game Name"
          onChange={this.onChangeInput.bind(this)}
          onBlur={this.onBlurInput.bind(this)}
          onKeyPress={this.onKeyPressInput.bind(this)}
        />
      </ButtonBase>
    )
  }
}