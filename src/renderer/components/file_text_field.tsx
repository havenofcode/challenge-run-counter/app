import * as React from 'react';
import {SxProps} from '@mui/system';
import {Theme} from '@mui/material/styles';
import {WithTheme, withTheme} from '@mui/styles';
import {getBridge} from '../../common';

import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
} from '@mui/material';

import {FileOpen, Delete} from '@mui/icons-material';

interface IProps extends WithTheme<Theme> {
  theme: Theme;
  helperText?: string;
  label: string;
  value?: string;
  sx?: SxProps;
  onChange: (val: string) => void;
}

interface IState {
  value: string;
}

class FileTextFieldImpl extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {value: this.props.value || ''};
  }

  async onOpenClick() {
    const bridge = getBridge();
    const res = await bridge.open_choose_directory({
      // defaultPath: undefined,
      // properties: ["openDirectory"],
    });

    if (!res.canceled && res.filePaths.length === 1) {
      this.setState({
        value: res.filePaths[0],
      });

      this.props.onChange(res.filePaths[0]);
      // try {
      //   await bridge.update_app_setting({
      //     id: this.props.appSetting.id,
      //     extra_obs_pages_directory: res.filePaths[0],
      //   });

      //   await bridge.restart_display_server();

      //   this.setState({
      //     extraObsPagesDirectory: res.filePaths[0],
      //     error: null,
      //   });
      // } catch (err) {
      //   console.error(err);
      //   const error: Error = err ? (err as Error) : new Error('unknown error');
      //   this.setState({error: error.message});
      // }
    }
  }

  async onDeleteClick() {
    this.setState({value: ''});
    this.props.onChange('');
  }

  renderDeleteButton() {
    return (
      <IconButton
        size="small"
        onClick={this.onDeleteClick.bind(this)}
      >
        <label>
          <Delete />
        </label>
      </IconButton>
    );
  }

  renderOpenFileButton() {
    return (
      <IconButton
        size="small"
        onClick={this.onOpenClick.bind(this)}
      >
        <label>
          <FileOpen />
        </label>
      </IconButton>
    );
  }

  render() {
    return (
      <Box sx={{
        width: 1,
        ...(this.props.sx ? this.props.sx : {}),
        position: 'relative',
      }}>
        <TextField
          fullWidth
          helperText={this.props.helperText}
          label={this.props.label}
          size="small"
          value={this.state.value}
          variant="outlined"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                {this.renderDeleteButton()}
                {this.renderOpenFileButton()}
              </InputAdornment>
            ),
          }}
        />
      </Box>
    );
  }
}

export const FileTextField = withTheme<Theme, typeof FileTextFieldImpl>(
  FileTextFieldImpl
);
