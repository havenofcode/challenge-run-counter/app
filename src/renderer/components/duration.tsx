import * as React from 'react';
import {renderDurationStringFromMillis} from '../util/time';

interface IProps {
  activeAt: number;
  milliseconds: number;
}

interface IState {
  durationString: string;
}

export class Duration extends React.Component<IProps, IState> {
  private timer: ReturnType<typeof setTimeout> = setTimeout(() => 69, 100);

  public state: IState = {
    durationString: '',
  }

  updateTime() {
    this.setState(
      {durationString: this.getTimeString()},
      () => {
        if (this.props.activeAt !== 0) {
          this.timer = setTimeout(() => this.updateTime(), 1000 / 60);
        }
      },
    );
  }

  componentDidMount() {
    this.updateTime();
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  componentDidUpdate(prevProps: IProps) {
    if (
      prevProps.activeAt !== this.props.activeAt ||
      prevProps.milliseconds !== this.props.milliseconds
    ) {
      clearTimeout(this.timer);
      this.updateTime();
    }
  }

  getTimeString() {
    const {activeAt} = this.props;
    const currentMilliseconds = this.props.milliseconds;
    const addedTime = activeAt > 0 ? new Date().getTime() - activeAt : 0;
    const num = currentMilliseconds + addedTime;
    return renderDurationStringFromMillis(num);
  }

  render() {
    return (
      <>
        {this.state.durationString}
      </>
    );
  }
}
