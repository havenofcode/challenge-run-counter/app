import * as React from 'react';
import {WithTheme, withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {css} from '@emotion/css';
import {getBridge} from '../../common/bridge';

import {
  Box,
  TextField,
} from '@mui/material';

import {SxProps} from '@mui/system';

interface IProps extends WithTheme<Theme> {
  theme: Theme;
  label: string;
  optionName: string;
  value: string;
  onChange?: (val: string) => void,
  sx?: SxProps;
  textFieldType?: string;
}

interface IState {
  value: string;
  errMessage: string;
}

class EditOptionTextImpl extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    const state: IState = {
      value: this.props.value,
      errMessage: '',
    }

    this.state = state;
  }

  private s = {
    inputWrapper: css`
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      position: relative;
    `,
    iconButton: css`
      position: absolute;
      right: 0;
      top: 28px;
      transform: translateY(-50%);
    `,
  }

  async onInputBlur() {
    const bridge = getBridge();

    switch (this.props.optionName) {
      case 'obs_browser_host': {
        await bridge.update_app_setting({
          id: 1,
          obs_browser_host: this.state.value,
        });
        break;
      }
      case 'obs_browser_port': {
        await bridge.update_app_setting({
          id: 1,
          obs_browser_port: this.state.value,
        });
        break;
      }
      case 'obs_page_width': {
        await bridge.update_app_setting({
          id: 1,
          obs_page_width: this.state.value,
        });
        break;
      }
      case 'obs_page_height': {
        await bridge.update_app_setting({
          id: 1,
          obs_page_height: this.state.value,
        });
        break;
      }
    }
  }

  render() {
    return (
      <Box
        sx={{
          width: 1,
          display: 'flex',
          flexDirection: 'row',
          flexGrow: 1,
          ...(this.props.sx || {}),
        }}
      >

        <TextField
          size="small"
          fullWidth
          type={this.props.textFieldType}
          label={this.props.label}
          helperText={this.state.errMessage}
          error={this.state.errMessage !== ''}
          value={this.state.value}
          variant="outlined"
          onChange={(e) => {
            this.setState({value: e.currentTarget.value})
            if (this.props.onChange) {
              this.props.onChange(e.currentTarget.value);
            }
          }}
          onBlur={this.onInputBlur.bind(this)}
        />
      </Box>
    );
  }
}

export const EditOptionText = withTheme<Theme, typeof EditOptionTextImpl>(EditOptionTextImpl);
