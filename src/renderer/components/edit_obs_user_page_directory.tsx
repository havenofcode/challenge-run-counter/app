import * as React from 'react';
import {SxProps} from '@mui/system';
import {Theme} from '@mui/material/styles';
import {WithTheme, withTheme} from '@mui/styles';
import {IAppSetting} from '../../common/model';
import {getBridge} from '../../common';

import {
  Box,
  IconButton,
  TextField,
} from '@mui/material';

import {FileOpen, Delete} from '@mui/icons-material';

interface IProps extends WithTheme<Theme> {
  theme: Theme;
  appSetting: IAppSetting;
  sx?: SxProps;
}

interface IState {
  extraObsPagesDirectory?: string | null;
  error?: string | null;
}

class EditObsUserPageDirectoryImpl extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {extraObsPagesDirectory: props.appSetting.extra_obs_pages_directory};
  }

  async onOpenClick() {
    const bridge = getBridge();
    const res = await bridge.open_choose_directory({
      defaultPath: this.state.extraObsPagesDirectory || undefined,
      properties: ["openDirectory"],
    });

    if (!res.canceled && res.filePaths.length === 1) {
      try {
        await bridge.update_app_setting({
          id: this.props.appSetting.id,
          extra_obs_pages_directory: res.filePaths[0],
        });

        await bridge.restart_display_server();

        this.setState({
          extraObsPagesDirectory: res.filePaths[0],
          error: null,
        });
      } catch (err) {
        console.error(err);
        const error: Error = err ? (err as Error) : new Error('unknown error');
        this.setState({error: error.message});
      }
    }
  }

  async onDeleteClick() {
    const bridge = getBridge();

    try {
      await bridge.update_app_setting({
        id: this.props.appSetting.id,
        extra_obs_pages_directory: null,
      });

      await bridge.restart_display_server();

      this.setState({
        extraObsPagesDirectory: null,
        error: null,
      });
    } catch (err) {
      const error: Error = err ? (err as Error) : new Error('unknown error');
      this.setState({
        error: error.message,
      });
    }
  }

  renderDeleteButton() {
    if (!this.state.extraObsPagesDirectory) {
      return null;
    }

    return (
      <IconButton
        size="small"
        onClick={this.onDeleteClick.bind(this)}
      >
        <label>
          <Delete />
        </label>
      </IconButton>
    );
  }

  renderOpenFileButton() {
    return (
      <IconButton
        size="small"
        onClick={this.onOpenClick.bind(this)}
      >
        <label>
          <FileOpen />
        </label>
      </IconButton>
    );
  }

  render() {
    return (
      <Box sx={{
        width: 1,
        ...(this.props.sx ? this.props.sx : {}),
        position: 'relative',
      }}>
        <TextField
          size="small"
          fullWidth
          label="User OBS Pages"
          variant="outlined"
          placeholder="Add custom obs pages..."
          value={this.state.extraObsPagesDirectory || ''}
          helperText={this.state.error || ''}
          error={Boolean(this.state.error)}
        />
        <Box
          sx={{
            display: 'flex',
            position: 'absolute',
            right: '8px',
            top: '50%',
            transform: 'translateY(-50%);',
          }}>
          {this.renderDeleteButton()}
          {this.renderOpenFileButton()}
        </Box>
      </Box>
    );
  }
}

export const EditObsUserPageDirectory = withTheme<Theme, typeof EditObsUserPageDirectoryImpl>(
  EditObsUserPageDirectoryImpl
);
