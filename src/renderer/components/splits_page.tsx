import * as React from 'react';
import {css, cx} from '@emotion/css';
import {WithTheme, withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {renderDurationStringFromMillis} from '../util/time';

import {
  Box,
  Button,
  ButtonBase,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@mui/material';

import {
  MoreVert,
  Add,
  DeleteForever,
} from '@mui/icons-material';

import {Toolbar} from './toolbar';
import {SplitStepTableRow} from './split_step_table_row';
import {SelectedSplitCollectionTab} from './selected_split_collection_tab'

import {
  IAppSetting,
  IRunScenario,
  ISplitPageCollection,
  ISplitPageData,
  ISplitStep,
  ISplitPageSplit,
  IEventShortcut,
} from '../../common';

import {Duration} from './duration';
import {getDuration} from '../util/time';

import {
  getBridge,
  SplitDuration,
  AttemptDuration,
} from '../../common';

import {RunScenarioDropDown} from './run_scenario_dropdown';

export interface SplitsPageComponentProps extends WithTheme<Theme> {
  id: string;
  onLoadProfileRequest: () => Promise<void>;
  onRemoveProfile: () => Promise<void>;
  theme: Theme;
  onRequestRoute: (pageRoute: string) => void;
  appSetting: IAppSetting;
}

type RequestAction = 'delete_collection' | 'delete_scenario';

export interface SplitsPageComponentState {
  data?: ISplitPageData;
  error: string | null;
  runScenario: IRunScenario | null;
  didRequestAction?: RequestAction | null;
  copyTextActive: boolean;
  attemptDuration: AttemptDuration | null;
}

export class SplitsPageComponentImpl extends React.Component<
  SplitsPageComponentProps,
  SplitsPageComponentState
> {
  state: SplitsPageComponentState = {
    error: null,
    runScenario: null,
    copyTextActive: false,
    attemptDuration: null,
  };

  async reload() {
    try {
      const bridge = getBridge();
      const data = await bridge.get_split_page_data(this.props.id);
      const timePoints = data.currentSplitCollection.data.run_attempt_time_points;
      let attemptDuration: AttemptDuration | null = null;

      if (timePoints) {
        attemptDuration = getDuration(timePoints);
      }

      this.setState({data, attemptDuration});
    } catch (err) {
      console.error(err);
      this.setState({error: 'and error occurred'});
    }
  }

  setActiveSplitDirty(
    nextSplit: ISplitPageSplit,
    prevSplit: ISplitPageSplit,
    time: number,
  ) {
    if (!this.state.data) {
      return;
    }

    if (
      this.state.attemptDuration &&
      this.state.attemptDuration.attemptActiveAt !== 0
    ) {
      const splitPrevDuration = this.state.attemptDuration.splits[prevSplit.data.id];
      const splitNextDuration = this.state.attemptDuration.splits[nextSplit.data.id];
      const obj: {[key: number]: SplitDuration} = {};

      obj[prevSplit.data.id] = {
        activeAt: 0,
        milliseconds: splitPrevDuration.milliseconds + (time - splitPrevDuration.activeAt),
      }

      obj[nextSplit.data.id] = {
        activeAt: time,
        milliseconds: splitNextDuration ? splitNextDuration.milliseconds : 0,
      }

      const newAttemptDuration = {
        ...this.state.attemptDuration,
        splits: {
          ...this.state.attemptDuration.splits,
          ...(obj),
        }
      }

      this.setState({
        data: {
          ...this.state.data,
          currentSplit: nextSplit,
        },
        attemptDuration: newAttemptDuration,
      });
    } else {
      this.setState({
        data: {
          ...this.state.data,
          currentSplit: nextSplit,
        }
      });
    }
  }

  async componentDidMount() {
    await this.reload();

    const bridge = getBridge();
    bridge.on_event_shortcut_invoked(async (name: string, e?: IEventShortcut) => {
      try {
        switch (name) {
          case 'hotkey_next_split': {
            if (!this.state.data) {
              console.log('breaking here?');
              break;
            }

            const {currentSplit, splits} = this.state.data;
            const nextIndex = currentSplit.splitStep.order + 1;
            const nextSplit = splits[nextIndex];

            if (nextSplit) {
              if (!e || !e.time) {
                throw new Error('hotkey_next_split does not have time');
              }

              this.setActiveSplitDirty(nextSplit, currentSplit, e.time);
            }

            break;
          }
          case 'hotkey_prev_split': {
            if (!this.state.data) {
              break;
            }

            const {currentSplit, splits} = this.state.data;
            const nextIndex = currentSplit.splitStep.order - 1;
            const nextSplit = splits[nextIndex];

            if (nextSplit) {
              if (!e || !e.time) {
                throw new Error('hotkey_next_split does not have time');
              }

              this.setActiveSplitDirty(nextSplit, currentSplit, e.time);
            }

            break;
          }
          case 'hotkey_increment_boss_hit':
          case 'hotkey_increment_way_hit':
          case 'hotkey_decrement_boss_hit':
          case 'hotkey_decrement_way_hit': {
            if (!this.state.data) {
              break;
            }

            const {currentSplit, splits} = this.state.data;
            const index = currentSplit.splitStep.order;
            const nextSplit = {...currentSplit}

            switch (name) {
              case 'hotkey_increment_boss_hit': {
                nextSplit.data.hits_boss += 1;
                break;
              }
              case 'hotkey_increment_way_hit': {
                nextSplit.data.hits_way += 1;
                break;
              }
              case 'hotkey_decrement_boss_hit': {
                // prevent hits lower than 0
                if (nextSplit.data.hits_boss) {
                  nextSplit.data.hits_boss -= 1;
                }
                break;
              }
              case 'hotkey_decrement_way_hit': {
                // prevent hits lower than 0
                if (nextSplit.data.hits_way) {
                  nextSplit.data.hits_way -= 1;
                }
                break;
              }
            }

            // duplicate splits array
            const nextSplitsArray = [...splits]

            // replace split data in array
            nextSplitsArray.splice(index, 1, nextSplit);

            this.setState({
              data: {
                ...this.state.data,
                currentSplit: nextSplit,
                splits: nextSplitsArray,
              }
            });

            break;
          }
          default: {
            await this.reload();
            break;
          }
        }
      } catch (err) {
        console.error(err);
      }
    });

    bridge.on_event_shortcut_error(async (e: {name: string, message: string}) => {
      try {
        await this.reload();
        alert(`error: ${e.message}, hotkey: ${e.name}`);
      } catch (err) {
        console.error(err);
      }
    });
  }

  async componentDidUpdate(prevProps: SplitsPageComponentProps) {
    if (prevProps.id !== this.props.id) {
      await this.reload();
    }
  }

  private s = {
    tabButtons: css`
      color: ${this.props.theme.palette.text.primary};
      display: flex;
      flex-direction: row;
    `,
    tab: css`
      min-width: 34px;
      border: 1px solid ${this.props.theme.palette.divider};
      flex-grow: 0;
      background-color: ${this.props.theme.palette.primary.contrastText};
      &:focus {
        background-color: ${this.props.theme.palette.action.focus};
      }
      &:hover {
        background-color: ${this.props.theme.palette.action.hover};
      }
    `,
    tabSelected: css`
      flex-grow: 1;
      background-color: ${this.props.theme.palette.action.selected};
      border-bottom: 2px solid ${this.props.theme.palette.primary.main};
    `,
  }

  async onTabClick(requestedSplitCollection: ISplitPageCollection) {
    const bridge = getBridge();
    const {data} = this.state;

    if (data) {
      const {currentSplit} = requestedSplitCollection;
      const newCurrentSessionSplitId = currentSplit.data.id;

      await bridge.update_run_session({
        id: data.runSession.id,
        current_session_split_id: newCurrentSessionSplitId,
      });

      await this.reload();
    }
  }

  renderTabButtons() {
    if (!this.state.data) {
      return;
    }

    const {s} = this;

    if (this.state.data) {
      const {splitCollection} = this.state.data.currentSplitCollection;
      return this.state.data.splitCollections.map((item) => {
        if (splitCollection.id === item.splitCollection.id) {
          return (
            <SelectedSplitCollectionTab
              className={cx(s.tab, s.tabSelected)}
              onChange={async () => await this.reload()}
              splitCollection={item.splitCollection}
              key={`split-collection-tab-button-${item.splitCollection.id}`}
            />
          );
        }

        return (
          <ButtonBase
            onClick={() => this.onTabClick(item)}
            key={`split-collection-tab-button-${item.splitCollection.id}`}
            className={cx(s.tab)}
          >
            <MoreVert />
          </ButtonBase>
        )
      });
    }
  }

  async onSplitCollectionCreate() {
    const bridge = getBridge();

    if (this.state.data) {
      await bridge.create_split_collection({
        run_scenario_id: this.state.data.runScenario.id,
        name: '',
      });
      await this.reload();
    }
  }

  async onSplitCollectionDelete() {
    const bridge = getBridge();
    if (this.state.data) {
      const {splitCollection} = this.state.data.currentSplitCollection;

      console.log('delete', splitCollection);
      await bridge.remove_split_collection(
        `${splitCollection.id}`
      );
      await this.reload();
    }
  }

  renderTabs() {
    const {s} = this;
    return (
      <Box className={cx(s.tabButtons)}>
        {this.renderTabButtons()}
        <ButtonBase
          onClick={() => this.onSplitCollectionCreate()}
          key={`split-collection-tab-button-minus`}
          className={cx(s.tab)}
        >
          <Add />
        </ButtonBase>
        <ButtonBase
          onClick={() => this.setState({
            didRequestAction: 'delete_collection'
          })}
          key={`split-collection-tab-button-plus`}
          className={cx(s.tab)}
        >
          <DeleteForever />
        </ButtonBase>
      </Box>
    );
  }

  async onSplitStepMoveUp(splitStep: ISplitStep) {
    if (this.state.data) {
      for (let i = this.state.data.splits.length - 1; i > 0 ; --i) {
        const split = this.state.data.splits[i].splitStep;
        if (split.order < splitStep.order) {
          // swap order
          const bridge = getBridge();

          await bridge.swap_split_order({
            fromId: split.id,
            toId: splitStep.id,
          });

          await this.reload();
          break;
        }
      }
    }
  }

  async onSplitStepMoveDown(splitStep: ISplitStep) {
    if (this.state.data) {
      for (let i = 0; i < this.state.data.splits.length - 1; ++i) {
        const split = this.state.data.splits[i].splitStep;
        if (split.order > splitStep.order) {
          // swap order
          const bridge = getBridge();

          await bridge.swap_split_order({
            fromId: split.id,
            toId: splitStep.id,
          });

          await this.reload();
          break;
        }
      }
    }
  }

  async onSplitStepDelete(deletedSplitStep: ISplitStep) {
    const bridge = getBridge();
    await bridge.remove_split_step(`${deletedSplitStep.id}`);
  }

  async onSplitRowCreate(splitStep: ISplitStep) {
    const bridge = getBridge();

    if (this.state.data) {
      const {splits} = this.state.data;

      await bridge.create_split_step({
        order: splits.length,
        split_collection_id: splitStep.split_collection_id,
        name: '',
      });

      await this.reload();
    }
  }

  renderSplits() {
    const {data} = this.state;

    if (data) {
      const {name} = data.currentSplitCollection.splitCollection;
      const sxCells = {pr: 2}

      if (data) {
        return (
          <Box>
            <TableContainer component={Paper}>
              <Table size="small" aria-label={`splits for ${name}`}>
                <TableHead>
                  <TableRow>
                    <TableCell padding="none"></TableCell>
                    <TableCell sx={sxCells} padding="none">title</TableCell>
                    <TableCell sx={sxCells} padding="none" align="right">Time</TableCell>
                    <TableCell sx={sxCells} padding="none" align="right">Time (Diff)</TableCell>
                    <TableCell sx={sxCells} padding="none" align="right">Hits (Boss)</TableCell>
                    <TableCell sx={sxCells} padding="none" align="right">Hits (Way)</TableCell>
                    <TableCell sx={sxCells} padding="none" align="right">Diff</TableCell>
                    <TableCell sx={sxCells} padding="none" align="right">PB (Time)</TableCell>
                    <TableCell sx={sxCells} padding="none" align="right">PB (Hits)</TableCell>
                    <TableCell sx={sxCells} padding="none" align="right"></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data.splits.map((splitData) => {
                    const {splitStep} = splitData;
                    const runSessionSplit = splitData.data;

                    return (
                      <SplitStepTableRow
                        key={`split-row-for-split-step-${splitStep.id}`}
                        currentSplitCollection={data.currentSplitCollection}
                        currentSplit={data.currentSplit}
                        onChange={() => this.reload()}
                        onCreate={() => this.onSplitRowCreate(splitStep)}
                        onDelete={() => this.onSplitStepDelete(splitStep)}
                        onMoveDown={() => this.onSplitStepMoveDown(splitStep)}
                        onMoveUp={() => this.onSplitStepMoveUp(splitStep)}
                        runSession={data.runSession}
                        runSessionSplit={runSessionSplit}
                        runSessionSplitCollection={data.currentSplitCollection.data}
                        splitData={splitData}
                        attemptDuration={this.state.attemptDuration}
                      />
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        );
      }
    }
  }

  async onSessionChange() {
    await this.reload();
  }

  async onLoadProfileRequest() {
    await this.props.onLoadProfileRequest();
    await this.reload();
  }

  async onDialogYes() {
    this.setState({didRequestAction: null});
    switch (this.state.didRequestAction) {
      case 'delete_collection': {
        this.onSplitCollectionDelete();
        break;
      }
      case 'delete_scenario': {
        const {data} = this.state;
        if (!data) {
          break;
        }

        const bridge = getBridge();
        await bridge.remove_run_scenario(`${data.runScenario.id}`);
        this.props.onRemoveProfile();
        break;
      }
    }
  }

  onDialogNo() {
    this.setState({didRequestAction: null});
  }

  getDialogMessage() {
    switch (this.state.didRequestAction) {
      case 'delete_collection': {
        return 'All splits on this screen will be removed!';
        break;
      }
      case 'delete_scenario': {
        return 'All games in this profile will be removed!';
      }
    }
    return 'sup';
  }

  renderDialog() {
    if (!this.state.didRequestAction) {
      return null;
    }

    return (
      <Dialog
        open={true}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Are you sure?
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {this.getDialogMessage()}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => this.onDialogNo()}>No</Button>
          <Button onClick={() => this.onDialogYes()}>Yes</Button>
        </DialogActions>
      </Dialog>
    );
  }

  renderBody() {
    if (this.state.error) {
      return (
        <div>
          {this.state.error}
        </div>
      );
    }

    if (this.state.data) {
      return (
        <Box>
          {this.renderDialog()}
          <RunScenarioDropDown
            selectedRunScenario={this.state.data.runScenario}
            onLoadProfileRequest={this.onLoadProfileRequest.bind(this)}
            onRemoveProfile={() => Promise.resolve(this.setState({
              didRequestAction: 'delete_scenario',
            }))}
            runSession={this.state.data.runSession}
            onSessionChange={this.onSessionChange.bind(this)}
            onRequestRoute={this.props.onRequestRoute}
            splitPageData={this.state.data}
          />
          {this.renderTabs()}
          <Box>
            {this.renderSplits()}
          </Box>
        </Box>
      );
    } else {
      return (
        <div>
          loading
        </div>
      );
    }
  }

  renderObsPageLink() {
    const {appSetting} = this.props;

    if (!appSetting) {
      return null;
    }

    const {obs_browser_host, obs_browser_port} = appSetting;
    const base = `http://${obs_browser_host}:${obs_browser_port}`;
    const htmlLink = `${base}/default.html`;

    if (this.state.copyTextActive) {
      return (
        <TextField
          size="small"
          sx={{width: `${htmlLink.length}ch`}}
          onBlur={() => this.setState({copyTextActive: false})}
          label="Copied to Clipboard!"
          onFocus={(e) => {
            console.log('focused', e.currentTarget);
            e.currentTarget.select();
            try {
              if (document.execCommand('copy')) {
                console.log('copied to clipboard');
              } else {
                console.log('couldnt copy to clipboard');
              }
            } catch (err) {
              console.error(err);
            }
          }}
          value={htmlLink}
          autoFocus
        />
      );
    }

    return (
      <Button
        onClick={() => this.setState({copyTextActive: true})}
        sx={{textTransform: 'none'}}
        variant="text"
      >
        {htmlLink}
      </Button>
    )
  }

  renderTime() {
    if (!this.state.data) {
      return null;
    }

    const attemptDuration = this.state.attemptDuration || {attempt: 0, attemptActiveAt: 0, splits: []}
    const splitCollection = this.state.data.currentSplitCollection.splitCollection;
    const pbTime = splitCollection.pb_time;

    return (
      <Box sx={{paddingRight: 2, textAlign: 'right'}}>
        <Typography
          variant="subtitle2"
          noWrap
          component="div"
          sx={{whiteSpace: 'nowrap'}}
        >
          Time:&nbsp;
          <Duration
            milliseconds={attemptDuration.attempt}
            activeAt={attemptDuration.attemptActiveAt}
          />
        </Typography>
        <Typography
          variant="body2"
          noWrap
          component="div"
          sx={{whiteSpace: 'nowrap'}}
        >
          PB: {pbTime ? renderDurationStringFromMillis(pbTime) : 'N/A'}
        </Typography>
      </Box>
    );
  }

  renderHits() {
    if (!this.state.data) {
      return null;
    }

    const splitCollection = this.state.data.currentSplitCollection.splitCollection;
    const pbHits = splitCollection.pb_hits || 0;
    let totalHits = 0;

    for (let i = 0; i < this.state.data.splits.length; ++i) {
      const split = this.state.data.splits[i];
      totalHits += split.data.hits_way || 0;
      totalHits += split.data.hits_boss || 0;
    }

    return (
      <Box sx={{paddingRight: 2, textAlign: 'right'}}>
        <Typography
          variant="subtitle2"
          noWrap
          component="div"
          sx={{whiteSpace: 'nowrap'}}
        >
          Hits: {totalHits}
        </Typography>
        <Typography
          variant="body2"
          noWrap
          component="div"
          sx={{whiteSpace: 'nowrap'}}
        >
          PB: {splitCollection.pb_hits === null ? 'N/A' : pbHits}
        </Typography>
      </Box>
    );
  }

  renderToolbarStuff() {
    return (
      <Box sx={{display: 'flex'}}>
        {this.renderTime()}
        {this.renderHits()}
        {this.renderObsPageLink()}
      </Box>
    );
  }

  render() {
    return (
      <Container maxWidth={false} disableGutters={true}>
        <Toolbar rightSideComponent={this.renderToolbarStuff()}  />
        {this.renderBody()}
      </Container>
    );
  }
}

export const SplitsPageComponent = withTheme<Theme, typeof SplitsPageComponentImpl>(SplitsPageComponentImpl);
