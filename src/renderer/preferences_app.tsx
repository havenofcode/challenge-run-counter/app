import * as React from 'react';

import {
  Box,
  Typography,
  Toolbar,
  Container,
  Tab,
  Tabs,
  AppBar,
  Paper,
} from '@mui/material';

import {getBridge} from '../common/bridge';

import {
  IAppSetting,
  IViewerSetting,
} from '../common/model';

import {EditHotKeyForm} from './components/edit_hot_key_form';
import {EditMiscOptionsForm} from './components/edit_misc_options_form';
import {EditCssStylesForm} from './components/edit_css_styles_form';
import {EditViewerOptionsForm} from './components/edit_viewer_options_form';

interface IState {
  appSetting?: IAppSetting;
  activeViewerSettings?: IViewerSetting[];
  tabValue: number;
}

export class PreferencesApp extends React.Component<Record<string, unknown>, IState> {
  state: IState = {
    tabValue: 0,
  }

  async reload() {
    const bridge = getBridge();
    const appSetting = await bridge.get_app_setting();
    const viewerNames = await bridge.get_active_viewer_settings();

    const maybeActiveViewerSettings = await Promise.all(viewerNames.map((name) => {
      return bridge.get_viewer_settings(name);
    }));

    const filteredViewerSettings = maybeActiveViewerSettings.filter((s) => {
      if (s) {
        return true;
      }
    }) as IViewerSetting[];

    const activeViewerSettings = filteredViewerSettings.sort((a: IViewerSetting, b: IViewerSetting) => {
      const A = a.created_at || a.updated_at;
      const B = b.created_at || b.updated_at;
      if (A > B) {
        return -1;
      } else if (A < B) {
        return 1;
      } else {
        return 0;
      }
    }).slice(0, 2);

    this.setState({appSetting, activeViewerSettings});
  }

  async componentDidMount() {
    await this.reload();
    const bridge = getBridge();
    bridge.on_active_viewer_settings_change(() => this.reload());
  }

  onTabChange(e: React.SyntheticEvent, value: number) {
    this.setState({tabValue: value});
  }

  async onAppSettingsChanged() {
    await this.reload();
  }

  renderHotkeyItems() {
    if (this.state.appSetting) {
      const hotKeyNames = [
        'next_split',
        'prev_split',
        'decrement_boss_hit',
        'increment_boss_hit',
        'decrement_way_hit',
        'increment_way_hit',
        'restart_run',
        'start_timer',
        'stop_timer',
        'record_pb',
      ];

      return hotKeyNames.map((name) => this.state.appSetting && (
        <Box key={`hotkey_edit_form_id_${name}`} sx={{width: 1 / 2}}>
          <EditHotKeyForm
            appSetting={this.state.appSetting}
            hotKeyName={name}
            onAppSettingsChanged={this.onAppSettingsChanged.bind(this)}
          />
        </Box>
      ));
    }
  }

  renderHotKeyForm() {
    return (
      <>
        <Typography variant="h6">
          Hotkeys
        </Typography>
        <Paper sx={{
          mt: 1,
          p: 1,
          display: 'flex',
          width: 1,
          flexWrap: 'wrap',
        }} elevation={12}>
          {this.renderHotkeyItems()}
        </Paper>
      </>
    );
  }

  renderViewerOptions(viewerSetting: IViewerSetting) {
    if (!this.state.appSetting) {
      return null;
    }

    return (
      <>
        <Typography variant="h6">
          {viewerSetting.name} Settings
        </Typography>
        <Box
          sx={{
            mt: 1,
            p: 0,
            display: 'flex',
            width: 1,
            flexWrap: 'wrap',
          }}
        >
          <EditViewerOptionsForm
            viewerSetting={viewerSetting}
            appSetting={this.state.appSetting}
            onAppSettingsChanged={this.onAppSettingsChanged.bind(this)}
          />
        </Box>
      </>
    );
  }

  renderMiscOptions() {
    if (!this.state.appSetting) {
      return;
    }

    return (
      <>
        <Typography variant="h6">
          OBS Page Settings
        </Typography>
        <Box
          key={`hotkey_edit_form_id_${name}`}
          sx={{
            mt: 1,
            p: 0,
            display: 'flex',
            width: 1,
            flexWrap: 'wrap',
          }}
        >
          <EditMiscOptionsForm
            appSetting={this.state.appSetting}
            onAppSettingsChanged={this.onAppSettingsChanged.bind(this)}
          />
        </Box>
        <Typography sx={{mt: 2}} variant="h6">
          CSS Styles
        </Typography>
        <Box
          sx={{
            mt: 1,
            p: 0,
            display: 'flex',
            width: 1,
            flexWrap: 'wrap',
          }}
        >
          <EditCssStylesForm
            appSetting={this.state.appSetting}
            onAppSettingsChanged={this.onAppSettingsChanged.bind(this)}
          />
        </Box>
      </>
    );
  }

  renderTabContent() {
    if (!this.state.activeViewerSettings) {
      return null;
    }

    switch (this.state.tabValue) {
      case 0: {
        return this.renderHotKeyForm();
      }
      case 1: {
        return this.renderMiscOptions();
      }
      default: {
        if (this.state.tabValue >= 2) {
          const activeSetting = this.state.activeViewerSettings[this.state.tabValue - 2];

          if (activeSetting) {
            return this.renderViewerOptions(activeSetting);
          } else {
            console.error(new Error('no active setting found'));
          }
        } else {
          console.warn('tab no content');
        }
        break;
      }
    }
    return null;
  }

  render() {
    if (!this.state.activeViewerSettings) {
      return null;
    }

    return (
      <Container maxWidth={false} disableGutters={true}>
        <AppBar position="relative">
          <Toolbar>
            <Typography
              variant="h6"
              noWrap
              component="div"
            >
              Preferences
            </Typography>
          </Toolbar>
        </AppBar>

        <Box sx={{width: '100%', typography: 'body1'}}>
          <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
            <Tabs
              value={this.state.tabValue}
              onChange={this.onTabChange.bind(this)}
              aria-label="basic tabs example"
            >
              <Tab label="Hotkeys" />
              <Tab label="Advanced" />
              {this.state.activeViewerSettings.map(({name}) => (
                <Tab key={name} label={`${name} Settings`} />
              ))}
            </Tabs>
          </Box>
          <Box
            sx={{
              padding: 2,
              display: 'flex',
              flexDirection: 'row',
              flexWrap: 'wrap',
            }}
          >
            {this.renderTabContent()}
          </Box>
        </Box>
      </Container>
    );
  }
}
