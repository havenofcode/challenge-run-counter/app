import * as React from 'react';
import Container from '@mui/material/Container';
import UrlPattern from 'url-pattern';
import {SplitsPage} from './pages/splits';
import {InsertRoutePage} from './pages/insert_route';

interface IState {
  pageRoute: string;
}

export class App extends React.Component<Record<string, unknown>, IState> {
  public state: IState = {
    pageRoute: '/splits',
  }

  onRequestRoute(pageRoute: string) {
    this.setState({pageRoute});
  }

  renderRoute() {
    const {pageRoute} = this.state;
    const pattern = new UrlPattern('/:page(/*)');
    const parsedRoute = pattern.match(pageRoute);

    const pageArgs = {
      pageRoute,
      onRequestRoute: this.onRequestRoute.bind(this),
    }

    switch (parsedRoute.page) {
      case 'splits': {
        return <SplitsPage {...pageArgs} />
      }
      case 'insert-route': {
        return <InsertRoutePage {...pageArgs} />
      }
      default: {
        return <SplitsPage {...pageArgs} />
      }
    }
  }

  render() {
    return (
      <Container maxWidth={false} disableGutters={true}>
        {this.renderRoute()}
      </Container>
    );
  }
}
