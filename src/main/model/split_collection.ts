import {Model} from './model';
import {RunScenario} from './run_scenario';
import {SplitStep} from './split_step';
import {ISplitCollection} from '../../common/model';
export {ISplitCollection};

export class SplitCollection extends Model implements ISplitCollection {
  id!: number;
  created_at!: number;
  updated_at!: number;
  name!: string;
  run_scenario_id!: number;
  deleted!: number;
  pb_hits!: number;
  pb_time!: number;
  run_scenario?: RunScenario;
  split_steps?: SplitStep[];

  static tableName = 'split_collections';

  static relationMappings = () => ({
    run_scenario: {
      relation: Model.BelongsToOneRelation,
      // The related model.
      modelClass: RunScenario,

      join: {
        from: 'split_collections.run_scenario_id',
        to: 'run_scenarios.id',
      },
    },

    split_steps: {
      relation: Model.HasManyRelation,
      modelClass: SplitStep,
      join: {
        from: 'split_collections.id',
        to: 'split_steps.split_collection_id',
      },
    },
  });
}
