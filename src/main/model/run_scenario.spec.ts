import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';
import {RunScenario} from './run_scenario';

describe('run_scenario', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('creates a row', async () => {
    await RunScenario.query().insert({
      name: 'run scenario 1'
    });

    const [res] = await RunScenario.query();
    expect(res.name).to.eql('run scenario 1');
  });
});
