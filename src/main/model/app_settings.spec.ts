import sinon from 'sinon';
import {expect} from 'chai';

import {GlobalShortcut} from 'electron';
import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';
import {AppSetting, noop} from './app_setting';
import {RunScenario} from './run_scenario';

describe('app_settings', () => {
  let destroyDbTask: RestoreTask = async () => null;

  const globalShortcutFixture = {
    register: sinon.spy(),
    unregister: sinon.spy(),
  }

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    globalShortcutFixture.register = sinon.stub();
    globalShortcutFixture.unregister = sinon.stub();
    sinon.stub(AppSetting, 'getGlobalShortcut')
      .returns(globalShortcutFixture as unknown as GlobalShortcut);
  });

  afterEach(async () => {
    await destroyDbTask();
    sinon.restore();
  });

  const createDefaultSettings = async () => {
    await RunScenario.query().insert({
      id: 1,
      name: 'run scenario 1'
    });

    await AppSetting.query().insert({
      id: 1,
      current_run_scenario_id: 1,
    });
  }

  it('creates a row', async () => {
    await createDefaultSettings();
    const [res] = await AppSetting.query();
    expect(res.id).to.eql(1);
  });

  it('sets valid global shortcut when no shortcut is enabled', async () => {
    await createDefaultSettings();
    const settings = await AppSetting.query().findById(1);
    await settings.setHotkey('hotkey_next_split', 'Control+Shift+Z');
    expect(globalShortcutFixture.register.callCount).to.eql(1);
    expect(globalShortcutFixture.unregister.callCount).to.eql(1);
    const registerArgs = globalShortcutFixture.register.getCall(0).args;
    const unregisterArgs = globalShortcutFixture.unregister.getCall(0).args;
    expect(registerArgs).to.eql(['Control+Shift+Z', noop]);
    expect(unregisterArgs).to.eql(['Control+Shift+Z']);
    const newSettings = await AppSetting.query().findById(1);
    expect(newSettings.hotkey_next_split).to.eql('Control+Shift+Z');
  });

  it('unsets old hotkey', async () => {
    await createDefaultSettings();
    await AppSetting.query().patch({
      hotkey_next_split: 'Control+Shift+N',
    }).findById(1);
    const settings = await AppSetting.query().findById(1);
    await settings.setHotkey('hotkey_next_split', 'Control+Shift+Z');
    expect(globalShortcutFixture.register.callCount).to.eql(1);
    expect(globalShortcutFixture.unregister.callCount).to.eql(2);

    const registerArgs = globalShortcutFixture.register.getCall(0).args;
    const unregisterArgs = globalShortcutFixture.unregister.getCall(0).args;
    const secondUnregisterArgs = globalShortcutFixture.unregister.getCall(1).args;
    expect(registerArgs).to.eql(['Control+Shift+Z', noop]);
    expect(unregisterArgs).to.eql(['Control+Shift+Z']);
    expect(secondUnregisterArgs).to.eql(['Control+Shift+N']);
    const newSettings = await AppSetting.query().findById(1);
    expect(newSettings.hotkey_next_split).to.eql('Control+Shift+Z');
  });

  it('does not set if shortcut returns error ', async () => {
    globalShortcutFixture.register = sinon.stub().throws(new Error());
    await createDefaultSettings();
    await AppSetting.query().patch({
      hotkey_next_split: 'Control+Shift+N',
    }).findById(1);

    let errorThrown = true;
    try {
      const settings = await AppSetting.query().findById(1);
      await settings.setHotkey('hotkey_next_split', 'dumb');
      errorThrown = false;
    } catch (err) {
      expect(globalShortcutFixture.register.callCount).to.eql(1);
      expect(globalShortcutFixture.unregister.callCount).to.eql(0);

      const appSetting = await AppSetting.query().findById(1);
      expect(appSetting.hotkey_next_split).to.eql('Control+Shift+N');
    }

    expect(errorThrown).to.be.true;
  });
});
