import {Model} from './model';
import {RunSession} from './run_session';
import {SplitStep} from './split_step';
import {IRunSessionSplit} from '../../common/model';
export {IRunSessionSplit};

export class RunSessionSplit extends Model implements IRunSessionSplit {
  id!: number;
  created_at!: number;
  updated_at!: number;
  run_session_id!: number;
  split_step_id!: number;
  hits_way!: number;
  hits_boss!: number;
  time!: number;
  split_step?: SplitStep;
  run_session?: RunSession;

  static tableName = 'run_session_splits';

  static relationMappings = () => ({
    split_step: {
      relation: Model.BelongsToOneRelation,
      modelClass: SplitStep,
      join: {
        from: 'run_session_splits.split_step_id',
        to: 'split_steps.id',
      },
    },
    run_session: {
      relation: Model.BelongsToOneRelation,
      modelClass: RunSession,
      join: {
        from: 'run_session_splits.run_session_id',
        to: 'run_sessions.id'
      }
    },
  })
}
