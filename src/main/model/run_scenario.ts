import {Model} from './model';
import {SplitCollection} from './split_collection';
import {IRunScenario} from '../../common/model';
import {RunSession} from './';
export {IRunScenario};

export class RunScenario extends Model implements IRunScenario {
  id!: number;
  created_at!: number;
  updated_at!: number;
  name!: string;
  deleted!: number;
  split_collections?: SplitCollection[];
  run_sessions?: RunSession[];

  static tableName = 'run_scenarios';

  static relationMappings = () => ({
    split_collections: {
      relation: Model.HasManyRelation,
      modelClass: SplitCollection,
      join: {
        from: 'run_scenarios.id',
        to: 'split_collections.run_scenario_id',
      },
    },
    run_sessions: {
      relation: Model.HasManyRelation,
      modelClass: RunSession,
      join: {
        from: 'run_scenarios.id',
        to: 'run_sessions.run_scenario_id',
      },
    },
  });
}
