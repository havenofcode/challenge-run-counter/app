import {Model} from './model';

import {
  SplitCollection,
  RunSessionSplit,
} from './';

import {ISplitStep} from '../../common/model';
export {ISplitStep};

export class SplitStep extends Model implements ISplitStep {
  id!: number;
  created_at!: number;
  updated_at!: number;
  name!: string;
  pb_hits!: number;
  pb_time!: number;
  deleted!: number;
  order!: number;
  split_collection_id!: number;
  split_collection?: SplitCollection;
  run_session_splits?: RunSessionSplit[];

  static tableName = 'split_steps';

  static relationMappings = () => ({
    run_session_splits: {
      relation: Model.HasManyRelation,
      modelClass: RunSessionSplit,
      join: {
        from: 'split_steps.id',
        to: 'run_session_splits.split_step_id',
      },
    },
    split_collection: {
      relation: Model.BelongsToOneRelation,
      // The related model.
      modelClass: SplitCollection,

      join: {
        from: 'split_steps.split_collection_id',
        to: 'split_collections.id',
      },
    },
  })
}
