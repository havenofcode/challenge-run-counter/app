import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';
import {RunScenario} from './run_scenario';
import {SplitCollection} from './split_collection';
import {SplitStep} from './split_step';

describe('split_step', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('creates a row', async () => {
    const [splitCollection] = await SplitCollection.query();
    const [splitStepFromTable] = await SplitStep.query();
    const [splitStep] = await splitCollection.$relatedQuery('split_steps');
    expect(splitStepFromTable).to.eql(splitStep);
    expect(splitStep.id).to.eql(1337);
    expect(splitStep.deleted).to.eql(0);
  });
});
