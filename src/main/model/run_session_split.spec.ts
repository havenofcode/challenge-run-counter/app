import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';
import {RunSessionSplit} from './run_session_split';
import {RunSession} from './run_session';
import {RunScenario} from './run_scenario';
import {SplitCollection} from './split_collection';
import {SplitStep} from './split_step';

describe('run_session_split', () => {

  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 1'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });

    await RunSession.query().insert({
      id: 420,
      run_scenario_id: 69,
    });

    await RunSessionSplit.query().insert({
      id: 1337,
      run_session_id: 420,
      split_step_id: 1337,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('creates a row', async () => {
    const [res] = await RunSessionSplit.query();
    expect(res.id).to.eql(1337);
  });

  it('selects relations', async () => {
    const runSessionSplit = await RunSessionSplit.query()
      .withGraphJoined('[split_step, run_session]')
      .findById(1337);

    expect(runSessionSplit.split_step.id).to.eql(1337);
    expect(runSessionSplit.run_session.id).to.eql(420);
  });
});
