import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';

import {
  RunSessionSplit,
  RunSession,
  RunScenario,
  SplitCollection,
  SplitStep,
  RunSessionSplitCollection,
  RunAttemptTimePoint,
} from './';

describe('run_attempt_time_points', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 1'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });

    await RunSession.query().insert({
      id: 420,
      run_scenario_id: 69,
    });

    await RunSessionSplit.query().insert({
      id: 1337,
      run_session_id: 420,
      split_step_id: 1337,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });

    await RunSessionSplitCollection.query().insert({
      id: 1,
      run_session_id: 420,
      current_split_step_id: 1337,
      split_collection_id: 420,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('creates a row', async () => {
    const {id} = await RunAttemptTimePoint.query().insert({
      run_session_split_collection_id: 1,
      run_session_split_id: 1337,
      time: new Date().getTime(),
      event_type: 'attempt_started',
    });

    const joinedRes = await RunAttemptTimePoint.query()
      .withGraphJoined('[run_session_split_collection, run_session_split]')
      .findById(id);

    expect(joinedRes.event_type).to.eql('attempt_started');
    expect(joinedRes.id).to.eql(id);
    expect(joinedRes.run_session_split_collection_id).to.eql(1);
    expect(joinedRes.run_session_split_id).to.eql(1337);
  });
});
