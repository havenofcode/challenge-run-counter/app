import path from 'path';
import {promises as fs} from 'fs';
import {Model} from './model';
import {app, App} from 'electron';
import {RunScenario} from './run_scenario';

import {
  globalShortcut,
  GlobalShortcut,
} from 'electron';

import {IAppSetting} from '../../common/model';
export {IAppSetting};

export const noop = () => console.log('noop');

export class AppSetting extends Model implements IAppSetting {
  id!: number;
  created_at!: number;
  updated_at!: number;
  obs_browser_host!: string;
  obs_browser_port!: string;
  hotkey_next_split!: string;
  hotkey_prev_split!: string;
  hotkey_increment_boss_hit!: string;
  hotkey_increment_way_hit!: string;
  hotkey_decrement_boss_hit!: string;
  hotkey_decrement_way_hit!: string;
  hotkey_restart_run!: string;
  hotkey_start_timer!: string;
  hotkey_stop_timer!: string;
  hotkey_record_pb!: string;
  current_run_scenario_id!: number;
  current_run_scenario!: RunScenario[];
  global_css_overrides!: string;
  obs_page_width!: string;
  obs_page_height!: string;
  extra_obs_pages_directory!: string | null;

  static tableName = 'app_settings';

  static getGlobalShortcut(): GlobalShortcut {
    return globalShortcut;
  }

  static getElectronApp(): App {
    return app;
  }

  static get globalShortcut(): GlobalShortcut {
    return this.getGlobalShortcut();
  }

  static relationMappings = () => ({
    current_run_scenario: {
      relation: Model.HasOneRelation,
      modelClass: RunScenario,
      join: {
        from: 'app_settings.current_run_scenario_id',
        to: 'run_scenarios.id',
      },
    },
  });

  getHotkey(name: string) {
    switch (name) {
      case 'hotkey_next_split': {
        return this.hotkey_next_split;
      }
      case 'hotkey_prev_split': {
        return this.hotkey_prev_split;
      }
      case 'hotkey_increment_boss_hit': {
        return this.hotkey_increment_boss_hit;
      }
      case 'hotkey_increment_way_hit': {
        return this.hotkey_increment_way_hit;
      }
      case 'hotkey_decrement_boss_hit': {
        return this.hotkey_decrement_boss_hit;
      }
      case 'hotkey_decrement_way_hit': {
        return this.hotkey_decrement_way_hit;
      }
      case 'hotkey_restart_run': {
        return this.hotkey_restart_run;
      }
      case 'hotkey_start_timer': {
        return this.hotkey_start_timer;
      }
      case 'hotkey_stop_timer': {
        return this.hotkey_stop_timer;
      }
      case 'hotkey_record_pb': {
        return this.hotkey_record_pb;
      }
      default: {
        throw new Error(`"${name}" is not a valid hotkey`);
      }
    }
  }

  async setHotkey(name: string, value: string) {
    const electronGlobalShortcut = AppSetting.getGlobalShortcut();

    if (value !== '') {
      electronGlobalShortcut.register(value, noop);
      electronGlobalShortcut.unregister(value);
    }

    let oldValue = '';

    switch (name) {
      case 'hotkey_next_split': {
        oldValue = this.hotkey_next_split;
        this.hotkey_next_split = value;
        break;
      }
      case 'hotkey_prev_split': {
        oldValue = this.hotkey_prev_split;
        this.hotkey_prev_split = value;
        break;
      }
      case 'hotkey_increment_boss_hit': {
        oldValue = this.hotkey_increment_boss_hit;
        this.hotkey_increment_boss_hit = value;
        break;
      }
      case 'hotkey_increment_way_hit': {
        oldValue = this.hotkey_increment_way_hit;
        this.hotkey_increment_way_hit = value;
        break;
      }
      case 'hotkey_decrement_boss_hit': {
        oldValue = this.hotkey_decrement_boss_hit;
        this.hotkey_decrement_boss_hit = value;
        break;
      }
      case 'hotkey_decrement_way_hit': {
        oldValue = this.hotkey_decrement_way_hit;
        this.hotkey_decrement_way_hit = value;
        break;
      }
      case 'hotkey_restart_run': {
        oldValue = this.hotkey_restart_run;
        this.hotkey_restart_run = value;
        break;
      }
      case 'hotkey_start_timer': {
        oldValue = this.hotkey_start_timer;
        this.hotkey_start_timer = value;
        break;
      }
      case 'hotkey_stop_timer': {
        oldValue = this.hotkey_stop_timer;
        this.hotkey_stop_timer = value;
        break;
      }
      case 'hotkey_record_pb': {
        oldValue = this.hotkey_record_pb;
        this.hotkey_record_pb = value;
        break;
      }
      default: {
        throw new Error(`"${name}" is not a valid hotkey`);
      }
    }

    if (oldValue !== '') {
      try {
        electronGlobalShortcut.unregister(oldValue);
      } catch (err) {
        console.error(err);
        console.error(new Error('unable to unregister a hotkey'));
      }
    }

    await AppSetting.query().patch(this).findById(this.id);
  }

  getResolvedObsPageStylesPath() {
    const electronApp = AppSetting.getElectronApp();
    const relativeFrom = path.join(electronApp.getPath('userData'), 'challenge_run_data');
    return path.resolve(relativeFrom, this.global_css_overrides);
  }

  async readObsPageStyles() {
    const filePath = this.getResolvedObsPageStylesPath();
    const stats = await fs.stat(filePath);

    if (stats.isFile()) {
      return await fs.readFile(filePath, 'utf8');
    }

    throw new Error(`error writing to path: ${filePath}`);
  }

  async writeObsPageStyles(content: string) {
    const filePath = this.getResolvedObsPageStylesPath();
    await fs.writeFile(filePath, content, 'utf8');
  }
}
