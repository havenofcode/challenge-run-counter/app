import {Model} from './model';
export * from '../../common/model/viewer_setting';
import {IViewerSetting} from '../../common/model';

export class ViewerSetting extends Model implements IViewerSetting {
  id!: number;
  name!: string;
  settings!: string;
  settings_schema!: string;

  static tableName = 'viewer_settings';
}
