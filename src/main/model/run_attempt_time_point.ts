import {Model} from './model';

import {IRunAttemptTimePoint, IRunAttemptTimeType} from '../../common/model';
import {RunSessionSplitCollection, RunSessionSplit} from './';

export class RunAttemptTimePoint extends Model implements IRunAttemptTimePoint {
  id!: number;
  created_at!: number;
  updated_at!: number;
  run_session_split_collection_id!: number;
  run_session_split_id!: number;
  event_type!: IRunAttemptTimeType;
  time!: number;
  run_session_split_collection?: RunSessionSplitCollection;
  run_session_split?: RunSessionSplit;

  static tableName = 'run_attempt_time_points';

  static relationMappings = () => ({
    run_session_split_collection: {
      relation: Model.BelongsToOneRelation,
      modelClass: RunSessionSplitCollection,
      join: {
        from: 'run_attempt_time_points.run_session_split_collection_id',
        to: 'run_session_split_collections.id',
      },
    },
    run_session_split: {
      relation: Model.BelongsToOneRelation,
      modelClass: RunSessionSplit,
      join: {
        from: 'run_attempt_time_points.run_session_split_id',
        to: 'run_session_splits.id'
      }
    },
  });
}
