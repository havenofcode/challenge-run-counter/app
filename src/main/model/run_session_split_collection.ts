import {Model} from './model';
import {RunSession} from './run_session';

import {
  RunAttemptTimePoint,
  SplitStep,
  SplitCollection,
} from './';

import {IRunSessionSplitCollection} from '../../common/model';
export {IRunSessionSplitCollection};

export class RunSessionSplitCollection extends Model implements IRunSessionSplitCollection {
  id!: number;
  created_at!: number;
  updated_at!: number;
  run_session_id!: number;
  current_split_step_id!: number;
  split_collection_id!: number;
  hits_boss!: number;
  hits_way!: number;
  time!: number;
  start_time?: number;
  end_time?: number;
  run_session?: RunSession;
  current_split_step?: SplitStep;
  split_collection?: SplitCollection;
  run_attempt_time_points?: RunAttemptTimePoint[];

  static tableName = 'run_session_split_collections';

  static relationMappings = () => ({
    run_session: {
      relation: Model.BelongsToOneRelation,
      modelClass: RunSession,

      join: {
        from: 'run_session_split_collections.run_session_id',
        to: 'run_sessions.id',
      },
    },
    current_split_step: {
      relation: Model.BelongsToOneRelation,
      modelClass: SplitStep,

      join: {
        from: 'run_session_split_collections.current_split_step_id',
        to: 'split_steps.id',
      },
    },
    split_collection: {
      relation: Model.BelongsToOneRelation,
      modelClass: SplitCollection,
      join: {
        from: 'run_session_split_collections.split_collection_id',
        to: 'split_collections.id',
      }
    },
    run_attempt_time_points: {
      relation: Model.HasManyRelation,
      modelClass: RunAttemptTimePoint,
      join: {
        from: 'run_session_split_collections.id',
        to: 'run_attempt_time_points.run_session_split_collection_id',
      }
    },
  })

  get timer_started(): boolean {
    return this.start_time !== null;
  }

  get timer_stopped(): boolean {
    return this.end_time !== null;
  }
}
