import {expect} from 'chai';

import {
  RunScenario,
  SplitCollection,
} from './';

import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';

describe('split_collection', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('creates a row', async () => {
    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    const [runScenario] = await RunScenario.query();
    const [splitCollectionFromTable] = await SplitCollection.query();
    const [splitCollection] = await runScenario.$relatedQuery('split_collections');
    expect(splitCollectionFromTable).to.eql(splitCollection);
  });

  it('contains run_scenario', async () => {
    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    const [runScenario] = await RunScenario.query();
    await runScenario.$relatedQuery('split_collections');
    expect(runScenario.id).to.eql(69);
  });
});
