import {Model as ObjectionModel, ModelOptions, QueryContext} from 'objection';
import {logger} from '../../common/util';

export class Model extends ObjectionModel {
  created_at!: number;
  updated_at!: number;

  // async $afterUpdate(opt: ModelOptions, queryContext: QueryContext) {
  //   await super.$beforeUpdate(opt, queryContext);
  // }

  async $beforeInsert(queryContext: QueryContext) {
    logger.info(`db:${this.$modelClass.tableName}:$beforeInsert`);
    await super.$beforeInsert(queryContext);
    this.created_at = new Date().getTime();

  }

  async $beforeUpdate(opts: ModelOptions, queryContext: QueryContext) {
    logger.info(`db:${this.$modelClass.tableName}:$beforeUpdate`);
    await super.$beforeUpdate(opts, queryContext);
    this.updated_at = new Date().getTime();
  }

  async $beforeDelete(queryContext: QueryContext) {
    logger.info(`db:${this.$modelClass.tableName}:$beforeDelete`);
    await super.$beforeDelete(queryContext);
  }
}
