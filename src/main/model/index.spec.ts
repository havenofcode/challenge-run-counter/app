import {expect} from 'chai';

import {
  getKnexConfig,
  defaultOpts,
} from './index';

import {getRestoreDbTasks} from '../../test/helpers';
import {RunScenario} from './run_scenario';

describe('objection', async () => {
  const {restoreDb, destroyDb} = await getRestoreDbTasks();

  beforeEach(async () => {
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDb();
  });

  it('exists', async () => {
    expect(process.env.NODE_ENV).to.eql('test');
    const config = getKnexConfig(defaultOpts.userDataDir);
    expect(config.test.connection).to.eql({filename: ':memory:'});
  });

  it('creates a row', async () => {
    await RunScenario.query().insert({
      name: 'run scenario 1'
    });
  });
});
