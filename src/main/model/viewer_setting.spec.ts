import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';
import {ViewerSetting} from './';

describe('viewer_setting', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('creates a row', async () => {
    await ViewerSetting.query().insert({
      name: 'name',
      settings: '{"cool": true}',
    });

    const [res] = await ViewerSetting.query();
    expect(res.name).to.eql('name');
    expect(res.settings).to.eql('{"cool": true}');
  });
});
