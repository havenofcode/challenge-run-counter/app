import {Model} from './model';
import {RunScenario} from './run_scenario';
import {RunSessionSplit} from './run_session_split';

import {
  RunSessionSplitCollection,
} from './run_session_split_collection';

import {IRunSession} from '../../common/model';
export {IRunSession};

export class RunSession extends Model implements IRunSession {
  id!: number;
  created_at!: number;
  updated_at!: number;
  run_scenario_id!: number;
  current_session_split_id?: number;
  start_time?: number;
  end_time?: number;
  current_session_split?: RunSessionSplit;
  run_scenario?: RunScenario;
  run_session_splits?: RunSessionSplit[];
  run_session_split_collections?: RunSessionSplitCollection[];
  current_run_session_split_collection?: RunSessionSplitCollection;

  static tableName = 'run_sessions';

  static relationMappings = () => ({
    current_session_split: {
      relation: Model.HasOneRelation,
      modelClass: RunSessionSplit,
      join: {
        from: 'run_sessions.current_session_split_id',
        to: 'run_session_splits.id'
      }
    },
    run_session_splits: {
      relation: Model.HasManyRelation,
      modelClass: RunSessionSplit,
      join: {
        from: 'run_sessions.id',
        to: 'run_session_splits.run_session_id',
      },
    },
    run_session_split_collections: {
      relation: Model.HasManyRelation,
      modelClass: RunSessionSplitCollection,
      join: {
        from: 'run_sessions.id',
        to: 'run_session_split_collections.run_session_id',
      },
    },
  });

  $afterFind() {
    this.current_run_session_split_collection = this.get_current_run_session_split_collection();
  }

  get_current_run_session_split_collection(): RunSessionSplitCollection | undefined {
    if (this.current_session_split && this.current_session_split.split_step) {
      const {split_step} = this.current_session_split;
      const splitCollections = this.run_session_split_collections;

      if (!splitCollections) {
        return;
      }

      const [filteredSplitCollection] = splitCollections.filter((item) => {
        return item.current_split_step_id === split_step.id;
      });

      if (!filteredSplitCollection) {
        return;
      }

      return filteredSplitCollection;
    }

    return;
  }
}
