import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';

import {
  RunSessionSplit,
  RunSession,
  RunScenario,
  SplitCollection,
  SplitStep,
  RunSessionSplitCollection,
  RunAttemptTimePoint,
} from './';

describe('run_session_split_collection', () => {

  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 1,
      name: 'run scenario 1'
    });

    await SplitCollection.query().insert({
      id: 1,
      name: 'split collection 1',
      run_scenario_id: 1,
    });

    await SplitStep.query().insert({
      id: 1,
      name: 'split collection 1',
      split_collection_id: 1,
    });

    await RunSession.query().insert({
      id: 1,
      run_scenario_id: 1,
    });

    await RunSessionSplit.query().insert({
      id: 1,
      run_session_id: 1,
      split_step_id: 1,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('creates a row', async () => {
    await RunSessionSplitCollection.query().insert({
      id: 1,
      run_session_id: 1,
      current_split_step_id: 1,
      split_collection_id: 1,
    });

    await RunAttemptTimePoint.query().insert({
      event_type: 'attempt_started',
      run_session_split_collection_id: 1,
      run_session_split_id: 1,
      time: new Date().getTime() - 500,
    });

    const [res] = await RunSessionSplitCollection.query().withGraphJoined(
      `[current_split_step, split_collection, run_session, run_attempt_time_points]`
    );

    expect(res.id).to.eql(1);
    expect(res.current_split_step).to.be.a('object');
    expect(res.run_session).to.be.a('object');
    expect(res.split_collection).to.be.a('object');
    expect(res.run_attempt_time_points.length).to.eql(1);
  });
});
