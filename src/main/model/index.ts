import path from 'path';
import {promises as fs} from 'fs';
import Knex from 'knex';

export * from '../../common/model';
export * from './app_setting';
export * from './run_scenario';
export * from './run_session_split_collection';
export * from './run_session_split';
export * from './run_session';
export * from './split_collection';
export * from './split_step';
export * from './run_attempt_time_point';
export * from './viewer_setting';

import {
  Model,
} from 'objection'

import {
  Database,
  RunResult
} from 'sqlite3';

type SqliteRunCallback = (this: RunResult, err: Error | null) => void;

export const getKnexConfig = (userDataDir: string) => ({
  test: {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: {
      filename: ':memory:',
    },
    pool: {
      afterCreate: (conn: Database, cb: SqliteRunCallback) => {
        conn.run('PRAGMA foreign_keys = ON', cb)
      },
    },
  },

  development: {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: {
      filename: path.join(userDataDir, 'development.db'),
    },
    pool: {
      afterCreate: (conn: Database, cb: SqliteRunCallback) => {
        conn.run('PRAGMA foreign_keys = ON', cb)
      },
    },
  },

  production: {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: {
      filename: path.join(userDataDir, 'production.db'),
    },
    pool: {
      afterCreate: (conn: Database, cb: SqliteRunCallback) => {
        conn.run('PRAGMA foreign_keys = ON', cb)
      },
    },
  },
})

export interface IKnexGetInstanceOptions {
  userDataDir: string;
  defaultDataDir: string;
  migrate: boolean;
  migrationDirectory: string;
}

export const defaultOpts = {
  userDataDir: path.resolve('data'),
  defaultDataDir: path.resolve('data'),
  migrate: false,
  migrationDirectory: path.resolve(__dirname, '..', '..', '..', 'migrations'),
}

const installDefaultData = async (opts: IKnexGetInstanceOptions) => {
  const dbProdPath = 'production.db';
  const dbDevPath = 'development.db';

  // make data directory if not exists
  try {
    await fs.stat(opts.userDataDir);
    console.warn('using user data directory:', opts.userDataDir);
  } catch (err) {
    console.warn('making user data directory', opts.userDataDir);
    await fs.mkdir(opts.userDataDir);
  }

  const absPathDev = path.join(opts.userDataDir, dbDevPath);
  const absPathProd = path.join(opts.userDataDir, dbProdPath);
  const absPathDefault = path.join(opts.defaultDataDir, dbDevPath);

  // copy development.db
  try {
    await fs.stat(absPathDev);
    console.warn('development user data:', absPathDev);
  } catch (err) {
    console.warn('initialize development user data: ', {from: absPathDefault, to: absPathDev});
    await fs.copyFile(
      absPathDefault,
      absPathDev,
    );
  }

  // copy production.db
  try {
    await fs.stat(absPathProd);
    console.warn('production user data:', absPathProd);
  } catch (err) {
    console.warn('initialize production user data: ', {from: absPathDefault, to: absPathProd});
    await fs.copyFile(
      absPathDefault,
      absPathProd,
    );
  }
}

export async function getKnexInstance(opts: IKnexGetInstanceOptions) {
  // Initialize knex.
  if (
    (!process.env.NODE_ENV || process.env.NODE_ENV === 'production') ||
    process.env.NODE_ENV === 'development'
  ) {
    console.warn('installing default user data with NODE_ENV', process.env.NODE_ENV);
    await installDefaultData(opts);
  }

  const config = getKnexConfig(opts.userDataDir);

  switch (process.env.NODE_ENV || 'production') {
    case 'test': {
      return Knex(config.test)
    }
    case 'development': {
      return Knex(config.development);
    }
    case 'production':
    default: {
      return Knex(config.production)
    }
  }
}

export async function initializeModels(opts: IKnexGetInstanceOptions = defaultOpts) {
  const {migrate, migrationDirectory} = opts;
  const knexInst = await getKnexInstance(opts);

  if (migrate) {
    await knexInst.migrate.latest({
      directory: migrationDirectory,
    });
  }

  Model.knex(knexInst);
  const res = async () => {
    await new Promise<void>((resolve) => {
      knexInst.destroy(() => resolve());
    });
  }

  return Promise.resolve(res);
}
