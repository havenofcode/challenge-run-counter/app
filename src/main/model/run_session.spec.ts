import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../test/helpers';
import {RunSession} from './run_session';
import {RunSessionSplit} from './run_session_split';
import {SplitCollection} from './split_collection';
import {SplitStep} from './split_step';
import {RunScenario} from './run_scenario';

describe('run_session', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 1'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });

    await RunSession.query().insert({
      id: 420,
      run_scenario_id: 69,
    });

    await RunSession.query().insert({
      id: 421,
      run_scenario_id: 69
    });

    await RunSessionSplit.query().insert({
      id: 1337,
      run_session_id: 421,
      split_step_id: 1337,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });

    await RunSession.query()
      .patch({
        current_session_split_id: 1337,
      })
      .findById(421);
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('return without current_session', async () => {
    const res = await RunSession.query()
      .findById(420)
      .withGraphJoined('current_session_split');

    expect(res.id).to.eql(420);
    expect(res.current_session_split).to.eql(null);
  });

  it('returns current_session_split', async () => {
    const res = await RunSession.query()
      .withGraphJoined('current_session_split')
      .findById(421);

    expect(res.current_session_split.id).to.eql(1337);
  });

  it('returns current_session_split', async () => {
    const res = await RunSession.query()
      .withGraphJoined('[run_session_splits.[split_step], current_session_split]')
      .orderBy('run_session_splits.id')
      .findById(421);

    expect(res.current_session_split.id).to.eql(1337);
  });
});
