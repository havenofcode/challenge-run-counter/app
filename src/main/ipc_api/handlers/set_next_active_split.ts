import {Transaction} from 'objection';
import {IpcMainInvokeEvent} from 'electron';
import {SplitStep} from '../../model/split_step';

import {
  stopTimerWithTransaction,
  setActiveSplitWithTransaction,
} from '.';

export const setNextActiveSplitWithTransaction = async (
  trx: Transaction,
  id: string,
  time: number,
): Promise<SplitStep | null> => {
  const splitStep = await SplitStep.query()
    .transacting(trx)
    .findById(id);

  const nextSplitStep = await SplitStep.query()
    .transacting(trx)
    .where('split_collection_id', splitStep.split_collection_id)
    .where('order', '>', splitStep.order)
    .where('deleted', 0)
    .orderBy('order', 'asc')
    .first();

  if (nextSplitStep) {
    await setActiveSplitWithTransaction(trx, `${nextSplitStep.id}`, time);
    return nextSplitStep;
  } else {
    try {
      await stopTimerWithTransaction(trx, time);
    } catch (err) {
      console.warn('set_next_active_split tried stopping unstarted timer');
      console.error(err);
    }

    return null;
  }
}

export const setNextActiveSplitHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string,
  time: number,
): Promise<SplitStep | null> => {
  return SplitStep.transaction((trx) => {
    return setNextActiveSplitWithTransaction(trx, id, time);
  });
}
