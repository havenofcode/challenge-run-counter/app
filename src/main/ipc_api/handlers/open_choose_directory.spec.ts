import {dialog, Dialog} from 'electron';
import {expect} from 'chai';
import {createSandbox} from 'sinon';
import {getRestoreDbTasks, RestoreTask} from '../../../test';

import {
  openChooseDirectoryHandler,
  openChooseDirectoryHandlerElectronApi,
} from './';

describe('IpcApi::openChooseDirectoryHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;
  const sandbox = createSandbox();
  const fakeDialog = {
    showOpenDialog: sandbox.stub().returns(Promise.resolve()),
  }

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
    openChooseDirectoryHandlerElectronApi.dialog = fakeDialog as unknown as Dialog;
  });

  afterEach(async () => {
    await destroyDbTask();
    sandbox.restore();
    openChooseDirectoryHandlerElectronApi.dialog = dialog;
  });

  it('exists', async () => {
    expect(openChooseDirectoryHandler).to.be.a('function');
  });

  it('loads the preferences page', async () => {
    await openChooseDirectoryHandler({}, {});
    expect(fakeDialog.showOpenDialog.getCall(0).args[0]).to.eql({});
  });
});
