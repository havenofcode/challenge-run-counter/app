import {AppSetting} from '../../model/app_setting';

export const getAppSettingHandler = async (): Promise<AppSetting> => {
  const appSetting = await AppSetting.query().findById(1);
  return appSetting;
}
