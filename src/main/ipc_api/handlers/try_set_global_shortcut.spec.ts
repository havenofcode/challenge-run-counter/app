import {expect} from 'chai';
import {createSandbox} from 'sinon';
import {GlobalShortcut} from 'electron';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {AppSetting} from '../../model';
import {trySetGlobalShortcutHandler} from './';

describe('IpcApi::trySetGlobalShortcutHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;
  const sandbox = createSandbox();

  const globalShortcutSpy = {
    register: sandbox.spy(),
    isRegistered: sandbox.spy(),
    unregister: sandbox.spy(),
  }

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    sandbox
      .stub(AppSetting, 'getGlobalShortcut')
      .returns(globalShortcutSpy as unknown as GlobalShortcut);
  });

  afterEach(async () => {
    await destroyDbTask();
    sandbox.restore();
  });

  it('exists', () => {
    expect(trySetGlobalShortcutHandler).to.be.a('function');
  });

  it('sets hotkey_next_split', async () => {
    await fixtures.freshInstallFixture();

    await trySetGlobalShortcutHandler({}, {
      name: 'hotkey_next_split',
      value: 'Ctrl+Alt+Delete'
    });

    const appSetting = await AppSetting.query().findById(1);
    expect(appSetting.hotkey_next_split).to.eql('Ctrl+Alt+Delete');
  });
});
