import {expect} from 'chai';
import {getRestoreDbTasks, fixtures} from '../../../test';
import {getSplitPageDataHandler} from './';

describe('IpcApi::getSplitPageDataHandler', () => {
  const {restoreDb, destroyDb} = getRestoreDbTasks();

  beforeEach(async () => {
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDb();
  });

  it('exists', () => {
    expect(getSplitPageDataHandler).to.be.a('function');
  });

  it('returns data on fresh install', async () => {
    await fixtures.freshInstallFixture();
    const data = await getSplitPageDataHandler({}, '1');
    expect(data.runScenario.id).to.eql(1);
    expect(data.runSession.id).to.eql(1);
    expect(data.splitCollections).to.have.lengthOf(1);
    expect(data.splits).to.have.lengthOf(4);
    expect(data.splits.map(({splitStep}) => splitStep.order)).to.eql([0, 1, 2, 3]);
    expect(data.currentSplitCollection.splitCollection.id).to.eql(1);
    expect(data.currentSplitCollection.data.id).to.eql(1);
  });

  it('returns data containing deleted splits', async () => {
    await fixtures.freshWithDeletedSplitsFixture();
    await getSplitPageDataHandler({}, '1');
  });

  it('returns data containing runAttemptTimePoints', async () => {
    await fixtures.withTimePointsAndCollectionsFixture();
    await getSplitPageDataHandler({}, '1');
  });

  it('returns data containing meaningful best segments', async () => {
    await fixtures.containerPBFixture();
    await getSplitPageDataHandler({}, '1');
  });

  it('throws if run scenario does not exist', async () => {
    await fixtures.freshInstallFixture();
    let didThrow = false;
    try {
      await getSplitPageDataHandler({}, '123');
    } catch (err) {
      didThrow = true;
      expect(err.message).to.eql('run scenario does not exist');
    }
    expect(didThrow).to.be.true;
  });
});
