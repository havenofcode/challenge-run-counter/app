import {IpcMainInvokeEvent} from 'electron';
import {AppSetting} from '../../model';

export const updateObsPageStylesHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  content: string,
): Promise<void> => {
  const appSetting = await AppSetting.query().findById(1);
  await appSetting.writeObsPageStyles(content);
}
