import {IpcMainInvokeEvent} from 'electron';
import {AppSetting} from '../../model/app_setting';

export const updateAppSettingHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<AppSetting> & {id: number},
): Promise<AppSetting> => {
  await AppSetting.query()
    .findById(data.id)
    .patch(data);

  return await AppSetting.query().findById(data.id);
}
