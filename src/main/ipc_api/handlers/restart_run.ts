import {Transaction} from 'objection';
import {RunSession} from '../../model/run_session';
import {SplitStep} from '../../model/split_step'
import {RunSessionSplit} from '../../model/run_session_split'
import {getCurrentSessionWithTransaction} from './get_current_session';
import {createRunSessionWithTransaction} from './create_run_session';

export const restartRunWithTransaction = async (
  trx: Transaction,
) => {
  const oldRunSession = await getCurrentSessionWithTransaction(trx);

  if (oldRunSession.current_session_split_id === null) {
    throw new Error(`runSession id ${oldRunSession.id} does not have current_session_split_id`);
  }

  const oldRunSessionSplit = await RunSessionSplit.query()
    .transacting(trx)
    .findById(oldRunSession.current_session_split_id);

  // get split collection user is currently viewing
  const oldSplitStep = await SplitStep.query()
    .transacting(trx)
    .findById(oldRunSessionSplit.split_step_id);

  const newRunSession = await createRunSessionWithTransaction(trx, {
    run_scenario_id: oldRunSession.run_scenario_id
  });

  const firstSplitStep = await SplitStep.query()
    .transacting(trx)
    .where({
      split_collection_id: oldSplitStep.split_collection_id,
      deleted: 0,
    })
    .orderBy('order', 'asc')
    .first();

  const firstRunSessionSplit = await RunSessionSplit.query()
    .transacting(trx)
    .where({
      split_step_id: firstSplitStep.id,
      run_session_id: newRunSession.id,
    })
    .first();

  await RunSession.query()
    .transacting(trx)
    .patch({
      current_session_split_id: firstRunSessionSplit.id,
    })
    .findById(newRunSession.id);
}

export const restartRunHandler = (): Promise<void> => {
  return RunSession.transaction((trx) => {
    return restartRunWithTransaction(trx);
  });
}
