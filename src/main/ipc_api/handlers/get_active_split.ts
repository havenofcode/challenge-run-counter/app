import {Transaction} from 'objection';
import {AppSetting} from '../../model/app_setting';
import {SplitStep} from '../../model/split_step';
import {RunSessionSplit} from '../../model/run_session_split';
import {RunSession} from '../../model/run_session';

const getCurrentSessionSplitId = async (trx: Transaction, runSession: RunSession) => {
  if (runSession.current_session_split_id === null) {
    console.warn('RunSession::current_session_split_id is null', runSession.id);

    const firstRunSessionSplit = await runSession.$relatedQuery('run_session_splits')
      .transacting(trx)
      .orderBy('id', 'asc')
      .first();

    await runSession.$query()
      .transacting(trx)
      .patch({current_session_split_id: firstRunSessionSplit.id});

    return firstRunSessionSplit.id;
  }

  return runSession.current_session_split_id;
}

export const getActiveSplitWithTransaction = async (trx: Transaction) => {
  const settings = await AppSetting.query()
    .transacting(trx)
    .findById(1);

  const runSession = await RunSession.query()
    .transacting(trx)
    .where('run_scenario_id', settings.current_run_scenario_id)
    .orderBy('created_at', 'desc')
    .first();

  const runSessionSplit = await RunSessionSplit.query()
    .transacting(trx)
    .findById(await getCurrentSessionSplitId(trx, runSession));

  const splitStep = await SplitStep.query()
    .transacting(trx)
    .findById(runSessionSplit.split_step_id);

  return splitStep;
}

export const getActiveSplitHandler = async (): Promise<SplitStep> => {
  return await AppSetting.transaction(async (trx) => {
    return getActiveSplitWithTransaction(trx);
  });
}
