import {IpcMainInvokeEvent} from 'electron';
import {SplitStep} from '../../model/split_step';

export const updateAllSplitStepsHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<SplitStep>[]
) => {
  await SplitStep.transaction(async (trx) => {
    await SplitStep.query()
      .transacting(trx)
      .upsertGraph(data);
  });
}
