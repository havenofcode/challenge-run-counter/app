import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {RunSession} from '../../model';
import {updateRunSessionHandler} from './';

describe('IpcApi::updateRunSessionHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(updateRunSessionHandler).to.be.a('function');
  });

  it('updates current_session_split_id', async () => {
    await fixtures.freshInstallFixture();
    await updateRunSessionHandler({}, {id: 1, current_session_split_id: 3});
    const runSession = await RunSession.query().findById(1);
    expect(runSession.current_session_split_id).to.eql(3);
  });
});
