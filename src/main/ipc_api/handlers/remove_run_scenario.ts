import {IpcMainInvokeEvent} from 'electron';
import {Transaction} from 'objection';
import {AppSetting} from '../../model/app_setting';
import {RunScenario} from '../../model/run_scenario';

export const removeRunScenarioHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string
): Promise<number> => {
  return await RunScenario.transaction(async (trx: Transaction) => {
    const [newSelectedRunScenario] = await RunScenario.query()
      .transacting(trx)
      .where('id', '!=', id)
      .where('deleted', '0')
      .orderBy('created_at', 'desc')
      .limit(1);

    if (newSelectedRunScenario) {
      await AppSetting.query()
        .transacting(trx)
        .findById(1).patch({
          current_run_scenario_id: newSelectedRunScenario.id,
        });
    }

    const deletedRunScenario = await RunScenario.query()
      .transacting(trx)
      .findById(id);

    await RunScenario.query()
      .transacting(trx)
      .findById(id)
      .patch({
        deleted: 1,
      });

    return deletedRunScenario.id;
  });
}
