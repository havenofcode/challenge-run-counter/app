import {expect} from 'chai';
import {recordSessionAsPBHandler} from '.';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';

import {
  RunSessionSplit,
  RunSessionSplitCollection,
  SplitCollection,
  SplitStep,
} from '../../model';

describe('run_attempt_time_points', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('records personal best time', async () => {
    await fixtures.withTimePointsFixture();
    await recordSessionAsPBHandler();
    const splitSteps = await SplitStep.query().orderBy('id', 'asc');
    splitSteps.forEach((step) => expect(step.pb_hits).to.eql(1));
    const splitCollection = await SplitCollection.query().findById(1);
    expect(splitCollection.pb_hits).to.eql(4);
    expect(splitCollection.pb_time).to.eql(200);
    const runSessionSplitCollection = await RunSessionSplitCollection.query().findById(1);
    const {hits_way, hits_boss, time} = runSessionSplitCollection;
    expect(hits_way).to.eql(2);
    expect(hits_boss).to.eql(2);
    expect(time).to.eql(200);
    const runSessionSplit1 = await RunSessionSplit.query().findById(1);
    const runSessionSplit2 = await RunSessionSplit.query().findById(2);
    expect(runSessionSplit1.time).to.eql(100);
    expect(runSessionSplit2.time).to.eql(100);
  });

  it('records pb on multiple runs', async () => {
    await fixtures.withTimePointsAndCollectionsFixture();
    await recordSessionAsPBHandler();

    // pb on split steps
    const splitSteps = await SplitStep.query()
      .where({split_collection_id: 1})
      .orderBy('id', 'asc');

    // hits
    expect(splitSteps[0].pb_hits).to.eql(1);
    expect(splitSteps[1].pb_hits).to.eql(0);
    expect(splitSteps[2].pb_hits).to.eql(0);
    expect(splitSteps[3].pb_hits).to.eql(1);

    // time
    expect(splitSteps[0].pb_time).to.eql(100);
    expect(splitSteps[1].pb_time).to.eql(100);
    expect(splitSteps[2].pb_time).to.eql(null);
    expect(splitSteps[3].pb_time).to.eql(null);

    const splitCollection = await SplitCollection.query().findById(1);
    expect(splitCollection.pb_hits).to.eql(2);
    expect(splitCollection.pb_time).to.eql(200);
    const runSessionSplitCollection = await RunSessionSplitCollection.query().findById(3);
    expect(runSessionSplitCollection.hits_way).to.eql(1);
    expect(runSessionSplitCollection.hits_boss).to.eql(1);
    expect(runSessionSplitCollection.time).to.eql(200);

    const runSessionSplits = await RunSessionSplit.query()
      .where({run_session_id: 2})
      .orderBy('id', 'asc');

    expect(runSessionSplits[0].hits_way).to.eql(1);
    expect(runSessionSplits[1].hits_way).to.eql(0);
    expect(runSessionSplits[2].hits_way).to.eql(0);
    expect(runSessionSplits[3].hits_way).to.eql(0);
    expect(runSessionSplits[0].hits_boss).to.eql(0);
    expect(runSessionSplits[1].hits_boss).to.eql(0);
    expect(runSessionSplits[2].hits_boss).to.eql(0);
    expect(runSessionSplits[3].hits_boss).to.eql(1);
    expect(runSessionSplits[0].time).to.eql(100);
    expect(runSessionSplits[1].time).to.eql(100);
    expect(runSessionSplits[2].time).to.eql(0);
    expect(runSessionSplits[3].time).to.eql(0);
  });

  it('records pb containing deleted splits', async () => {
    await fixtures.freshWithDeletedSplitsFixture();
    await recordSessionAsPBHandler();

    const splitSteps = await SplitStep.query()
      .where({
        split_collection_id: 2,
        deleted: false,
      }) 
      .orderBy('id', 'asc');

    expect(splitSteps[0].pb_hits).to.eql(1);
    expect(splitSteps[1].pb_hits).to.eql(0);
    expect(splitSteps[2].pb_hits).to.eql(0);
  });

  it('records pb on fresh install', async () => {
    await fixtures.freshInstallFixture();
    await recordSessionAsPBHandler();

    const splitSteps = await SplitStep.query()
      .where({split_collection_id: 1})
      .orderBy('id', 'asc');

    expect(splitSteps[0].pb_hits).to.eql(0);
    expect(splitSteps[1].pb_hits).to.eql(0);
    expect(splitSteps[2].pb_hits).to.eql(0);
    expect(splitSteps[3].pb_hits).to.eql(0);

    expect(splitSteps[0].pb_time).to.be.null;
    expect(splitSteps[1].pb_time).to.be.null;
    expect(splitSteps[2].pb_time).to.be.null;
    expect(splitSteps[3].pb_time).to.be.null;
  });

  it('records pb on fresh install', async () => {
    await fixtures.multipleCollectionsFixture();
    await recordSessionAsPBHandler();

    const splitSteps = await SplitStep.query()
      .where({split_collection_id: 2})
      .orderBy('id', 'asc');

    // splitSteps id
    expect(splitSteps[0].pb_hits).to.eql(2);
    expect(splitSteps[1].pb_hits).to.eql(2);
    expect(splitSteps[2].pb_hits).to.eql(2);
    expect(splitSteps[3].pb_hits).to.eql(2);
    // splitStep pb_time
    expect(splitSteps[0].pb_time).to.be.null;
    expect(splitSteps[1].pb_time).to.be.null;
    expect(splitSteps[2].pb_time).to.be.null;
    expect(splitSteps[3].pb_time).to.be.null;

    // splitCollection pb data
    const splitCollection = await SplitCollection.query().findById(2);
    expect(splitCollection.pb_hits).to.eql(8);
    expect(splitCollection.pb_time).to.eql(0);
  });

  it('fails recording pb when runSessionSplit is missing', async () => {
    await fixtures.missingRunSessionSplitFixture();
    let didThrow = false;
    let error: Error | null = null;

    try {
      await recordSessionAsPBHandler();
    } catch (err) {
      didThrow = true;
      error = err;
    }

    expect(didThrow).to.eql(true);
    expect(error.message).to.eql('internal error, no runSessionSplit for splitStep 3');
  });

  it('fails if current_session_split_id is null on runSession', async () => {
    await fixtures.missingCurrentSessionSplitFixture();
    let didThrow = false;
    let error: Error | null = null;

    try {
      await recordSessionAsPBHandler();
    } catch (err) {
      didThrow = true;
      error = err;
    }

    expect(didThrow).to.eql(true);
    expect(error.message).to.eql(`runSession id 1 does not have current_session_split_id`);
  });
});
