import {IpcMainInvokeEvent} from 'electron';
import {DisplayServer} from '../../../ws';
import {IPageSettings} from '../../../common';

export const notifyPageSettingsChangeHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  content: Partial<IPageSettings>,
): Promise<void> => {
  await DisplayServer.emitPageSettingsUpdate(content);
}
