import path from 'path';
import {promises as fs} from 'fs';
import os from 'os';
import {App} from 'electron';
import {createSandbox} from 'sinon';
import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {AppSetting} from '../../model';
import {updateObsPageStylesHandler} from './';

describe('IpcApi::updateObsPageStylesHandler', () => {
  const sandbox = createSandbox();
  let destroyDbTask: RestoreTask = async () => null;
  let tmpDir = '';

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
    tmpDir = await fs.mkdtemp(path.join(os.tmpdir(), 'challenge-run-counter-test-'));
    await fs.mkdir(path.join(tmpDir, 'challenge_run_data'));
    sandbox.stub(AppSetting, 'getElectronApp').returns({
      getPath() {
        return tmpDir;
      }
    } as unknown as App);
  });

  afterEach(async () => {
    await destroyDbTask();
    await fs.rm(tmpDir, {recursive: true, force: true});
    sandbox.restore();
  });

  it('exists', () => {
    expect(updateObsPageStylesHandler).to.be.a('function');
  });

  it('sets default obs page styles path', async () => {
    await fixtures.freshInstallFixture();
    await updateObsPageStylesHandler({}, 'banana');
    const defaultPath = path.join(tmpDir, 'challenge_run_data', 'default.css');
    const subject = await fs.readFile(defaultPath, 'utf8');
    expect(subject).to.eql('banana');
  });

  it('sets user defined obs page styles path', async () => {
    await fixtures.freshInstallFixture();

    await AppSetting.query().patch({
      global_css_overrides: path.join(tmpDir, 'banana.css'),
    });

    await updateObsPageStylesHandler({}, 'danny banany');
    const content = await fs.readFile(path.join(tmpDir, 'banana.css'), 'utf8');
    expect(content).to.eql('danny banany');
  });
});
