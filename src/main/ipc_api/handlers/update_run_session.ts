import {IpcMainInvokeEvent} from 'electron';
import {RunSession} from '../../model/run_session';

export const updateRunSessionHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<RunSession> & {id: number}
): Promise<RunSession> => {
  await RunSession.query().findById(data.id).patch(data);
  return await RunSession.query().findById(data.id);
}
