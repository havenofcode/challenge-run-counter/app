import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';

import {
  RunSession,
  RunSessionSplit,
  RunSessionSplitCollection,
} from '../../model';

import {restartRunHandler} from './';

describe('IpcApi::restartRunHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(restartRunHandler).to.be.a('function');
  });

  it('restarts run on fresh install', async () => {
    await fixtures.freshInstallFixture();
    await restartRunHandler();
    const runSession = await RunSession
      .query()
      .withGraphJoined([
        '[run_session_splits.[split_step]',
        'current_session_split.[split_step]',
        'run_session_split_collections.[split_collection]]'
      ].join(', '))
      .findById(2);

    const runSessionSplits = await RunSessionSplit.query()
      .select('id', 'split_step_id')
      .where({
        run_session_id: 2,
      })
      .orderBy('id', 'asc');

    const runSessionSplitCollections = await RunSessionSplitCollection.query()
      .where({run_session_id: 2})
      .orderBy('id', 'asc');

    expect(runSessionSplitCollections).to.have.lengthOf(1);
    expect(runSessionSplitCollections[0].current_split_step_id).to.eql(1);
    expect(runSession.current_session_split_id).to.eql(5);

    expect(runSessionSplits).to.eql([
      {id: 5, split_step_id: 1},
      {id: 6, split_step_id: 2},
      {id: 7, split_step_id: 3},
      {id: 8, split_step_id: 4},
    ]);
  });

  it('restarts run containing deleted splits', async () => {
    await fixtures.freshWithDeletedSplitsFixture();
    await restartRunHandler();
    const runSession = await RunSession
      .query()
      .withGraphJoined([
        '[run_session_splits.[split_step]',
        'current_session_split.[split_step]',
        'run_session_split_collections.[split_collection]]'
      ].join(', '))
      .findById(2);

    const runSessionSplits = await RunSessionSplit.query()
      .select('id', 'split_step_id')
      .where({
        run_session_id: 2,
      })
      .orderBy('id', 'asc');

    const runSessionSplitCollections = await RunSessionSplitCollection.query()
      .where({run_session_id: 2})
      .orderBy('id', 'desc');

    expect(runSessionSplitCollections).to.have.lengthOf(2);
    expect(runSessionSplitCollections[0].current_split_step_id).to.eql(5);
    expect(runSession.current_session_split_id).to.eql(13);

    expect(runSessionSplits).to.eql([
      {id: 9, split_step_id: 1},
      {id: 10, split_step_id: 2},
      {id: 11, split_step_id: 3},
      {id: 12, split_step_id: 4},
      {id: 13, split_step_id: 5},
      {id: 14, split_step_id: 7},
      {id: 15, split_step_id: 8},
    ]);
  });

  it('fails if current_session_split_id is null', async () => {
    await fixtures.missingCurrentSessionSplitFixture();
    let didThrow = false;
    let error: Error | null = null;

    try {
      await restartRunHandler();
    } catch (err) {
      didThrow = true;
      error = err;
    }

    expect(didThrow).to.eql(true);
    expect(error.message).to.eql(`runSession id 1 does not have current_session_split_id`);
  });
});
