import {IpcMainInvokeEvent} from 'electron';

import {
  SplitCollection,
} from '../../model/split_collection';

export const getSplitCollectionHandler = async (event: Partial<IpcMainInvokeEvent>, id: string): Promise<SplitCollection> => {
  const splitCollection = await SplitCollection.query()
    .withGraphJoined('split_steps')
    .where('split_steps.deleted', '=', '0')
    .orderBy('split_steps.order', 'asc')
    .findById(id);

  return splitCollection;
}
