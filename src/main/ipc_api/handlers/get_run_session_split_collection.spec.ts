import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {getRunSessionSplitCollectionHandler} from './';

describe('IpcApi::getRunSessionSplitCollectionHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getRunSessionSplitCollectionHandler).to.be.a('function');
  });

  it('returns on fresh install', async () => {
    await fixtures.freshInstallFixture();
    const collection = await getRunSessionSplitCollectionHandler({}, '1');
    expect(collection.split_collection_id).to.eql(1);
    expect(collection.current_split_step_id).to.eql(1);
  });
});
