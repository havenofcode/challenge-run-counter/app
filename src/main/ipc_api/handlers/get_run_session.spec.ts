import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, waitTick} from '../../../test/helpers';
import {getRunSessionHandler} from './get_run_session';
import {SplitCollection} from '../../model/split_collection';
import {RunSession} from '../../model/run_session';
import {RunScenario} from '../../model/run_scenario';
import {RunSessionSplit} from '../../model/run_session_split';
import {SplitStep} from '../../model/split_step';
import {RunSessionSplitCollection} from '../../model/run_session_split_collection';

describe('IpcApi::getRunSessionHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 1'
    });

    await waitTick();

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await waitTick();

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });

    await waitTick();

    await RunSession.query().insert({
      id: 420,
      run_scenario_id: 69,
    });

    await waitTick();

    await RunSessionSplitCollection.query().insert({
      id: 420,
      run_session_id: 420,
      current_split_step_id: 1337,
      split_collection_id: 420,
    });

    await waitTick();

    await RunSessionSplit.query().insert({
      id: 1338,
      run_session_id: 420,
      split_step_id: 1337,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });

    await waitTick();

    await RunSessionSplit.query().insert({
      id: 1337,
      run_session_id: 420,
      split_step_id: 1337,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });

    await waitTick();

    await SplitStep.query().insert({
      id: 666,
      name: 'deleted',
      deleted: 1,
      split_collection_id: 420,
    });

    await waitTick();

    await RunSessionSplit.query().insert({
      id: 666,
      run_session_id: 420,
      split_step_id: 666,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });

    await waitTick();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getRunSessionHandler).to.be.a('function');
  });

  it('returns split collection', async () => {
    const ipcMainInvokeEvent = {}
    const runSession = await getRunSessionHandler(ipcMainInvokeEvent, '420');
    expect(runSession.id).to.eql(420);
    expect(runSession.run_session_splits).to.have.lengthOf(2);
    const [runSessionSplit] = runSession.run_session_splits;
    expect(runSessionSplit.id).to.eql(1338);
  });
});
