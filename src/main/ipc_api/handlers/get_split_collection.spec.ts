import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {getSplitCollectionHandler} from './get_split_collection';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';

describe('IpcApi::getSplitCollectionHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1,
      order: 2,
      name: 'split step 1',
      split_collection_id: 420,
    });

    await SplitStep.query().insert({
      id: 666,
      order: 1,
      name: 'deleted',
      split_collection_id: 420,
      deleted: 1,
    });

    await SplitStep.query().insert({
      id: 2,
      order: 3,
      name: 'split step 2',
      split_collection_id: 420,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getSplitCollectionHandler).to.be.a('function');
  });

  it('returns split collection', async () => {
    const ipcMainInvokeEvent = {}
    const id = '420';
    const splitCollection = await getSplitCollectionHandler(ipcMainInvokeEvent, id);
    expect(splitCollection.id).to.eql(420);
    expect(splitCollection.name).to.eql('split collection 420');
    expect(splitCollection.split_steps).to.have.lengthOf(2);
    const [firstSplitStep] = splitCollection.split_steps;
    expect(firstSplitStep.id).to.eql(1);
  });

  it('orders by order column', async () => {
    await SplitStep.query().insert({
      name: 'split step 3',
      order: 1,
      split_collection_id: 420,
    });

    await SplitStep.query().insert({
      name: 'split step 3',
      order: 4,
      split_collection_id: 420,
    });

    const res = await getSplitCollectionHandler({}, '420');
    expect(res.split_steps.map(({order}) => order)).to.eql([1, 2, 3, 4]);
  });
});
