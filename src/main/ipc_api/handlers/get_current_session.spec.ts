import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {getCurrentSessionHandler} from './';

describe('IpcApi::getCurrentSessionHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getCurrentSessionHandler).to.be.a('function');
  });

  it('gets current run session', async () => {
    await fixtures.multipleRunScenariosFixture();
    const res = await getCurrentSessionHandler();
    expect(res.id).to.eql(2);
  });
});
