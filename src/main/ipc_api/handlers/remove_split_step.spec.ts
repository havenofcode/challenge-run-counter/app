import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {removeSplitStepHandler} from './remove_split_step';
import {RunSessionSplit} from '../../model/run_session_split';
import {RunSessionSplitCollection} from '../../model/run_session_split_collection';
import {RunSession} from '../../model/run_session';
import {RunScenario} from '../../model/run_scenario';
import {SplitCollection} from '../../model/split_collection';
import {SplitStep} from '../../model/split_step';
import {AppSetting} from '../../model/app_setting';

describe('IpcApi::removeSplitStepHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await RunSession.query().insert({
      id: 69,
      run_scenario_id: 69,
    })

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1,
      name: 'first ever split step',
      order: 0,
      split_collection_id: 420,
    });

    await RunSessionSplit.query().insert({
      id: 1,
      run_session_id: 69,
      split_step_id: 1,
    });

    await RunSessionSplitCollection.query().insert({
      id: 1,
      run_session_id: 69,
      current_split_step_id: 1,
      split_collection_id: 420,
    });

    await RunSession.query().findById(69).patch({
      id: 69,
      current_session_split_id: 1,
    });

    await AppSetting.query().insert({
      id: 1,
      current_run_scenario_id: 69,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(removeSplitStepHandler).to.be.a('function');
  });

  it('does not allow deleteing last split step', async () => {
    let errThrown = false;
    try {
      const splitStep = await SplitStep.query().findById(1);
      expect(splitStep.deleted).to.eql(0);
      await removeSplitStepHandler({}, `${splitStep.id}`);
      const updated = await SplitStep.query().findById(1);
      expect(updated.deleted).to.eql(1);
    } catch (err) {
      expect(err.message).to.eql('unable to delete last split step in collection');
      errThrown = true;
    }
    expect(errThrown).to.be.true;
  });

  it('allows delete when current_session_split_id', async () => {
    await SplitStep.query().insert({
      id: 2,
      name: 'first ever split step',
      order: 1,
      split_collection_id: 420,
    });

    await RunSessionSplit.query().insert({
      id: 2,
      run_session_id: 69,
      split_step_id: 2,
    });

    await removeSplitStepHandler({}, '1');
    const updated = await SplitStep.query().findById(1);
    const [runSession] = await RunSession.query();
    const [runSessionSplitCollection] = await RunSessionSplitCollection.query();
    expect(runSession.current_session_split_id).to.eql(2);
    expect(runSessionSplitCollection.current_split_step_id).to.eql(2);
    expect(updated.deleted).to.eql(1);
  });
});
