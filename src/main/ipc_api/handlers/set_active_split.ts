import {Transaction} from 'objection';
import {IpcMainInvokeEvent} from 'electron';

import {
  SplitStep,
  RunSessionSplit,
  RunSessionSplitCollection,
  RunSession,
  RunAttemptTimePoint,
} from '../../model';

import {getCurrentSessionWithTransaction} from './get_current_session';

export const setActiveSplitWithTransaction = async (trx: Transaction, id: string, time: number) => {
  const splitStep = await SplitStep.query()
    .transacting(trx)
    .where('deleted', 0)
    .findById(id);

  if (!splitStep) {
    console.warn('split step does note exist');
    return;
  }

  const runSession = await getCurrentSessionWithTransaction(trx);

  const runSessionSplit = await RunSessionSplit.query()
    .transacting(trx)
    .where({
      run_session_id: runSession.id,
      split_step_id: splitStep.id,
    })
    .first();

  const runSessionSplitCollection = await RunSessionSplitCollection.query()
    .transacting(trx)
    .where({
      run_session_id: runSession.id,
      split_collection_id: splitStep.split_collection_id,
    })
    .first();

  const timePoints = await RunAttemptTimePoint.query()
    .transacting(trx)
    .where({
      run_session_split_collection_id: runSessionSplitCollection.id
    }).orderBy('time', 'desc');

  // insert time points for tracking time spent at each split
  for (let i = 0; i < timePoints.length; ++i) {
    const timePoint = timePoints[i];

    if (timePoint.event_type === 'attempt_stopped') {
      break;
    }

    if (timePoint.event_type === 'attempt_started') {
      await RunAttemptTimePoint.query()
        .transacting(trx)
        .insert({
          run_session_split_id: runSession.current_session_split_id,
          run_session_split_collection_id: runSessionSplitCollection.id,
          event_type: 'split_stopped',
          time,
        });

      await RunAttemptTimePoint.query()
        .transacting(trx)
        .insert({
          run_session_split_id: runSessionSplit.id,
          run_session_split_collection_id: runSessionSplitCollection.id,
          event_type: 'split_started',
          time,
        });
      break;
    }
  }

  await RunSession.query()
    .transacting(trx)
    .findById(runSession.id)
    .patch({
      current_session_split_id: runSessionSplit.id
    });

  await RunSessionSplitCollection.query()
    .transacting(trx)
    .findById(runSessionSplitCollection.id)
    .patch({
      current_split_step_id: splitStep.id,
    });

  return splitStep;
}

export const setActiveSplitHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string,
  time: number,
): Promise<SplitStep> => {
  return await SplitStep.transaction(async (trx) => {
    return setActiveSplitWithTransaction(trx, id, time);
  });
}
