import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';

import {
  RunSession,
  RunSessionSplitCollection,
  RunAttemptTimePoint,
} from '../../model';

import {setActiveSplitHandler} from './';

describe('IpcApi::setActiveSplitHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(setActiveSplitHandler).to.be.a('function');
  });

  it('restarts run on fresh install', async () => {
    await fixtures.freshInstallFixture();
    await setActiveSplitHandler({}, '2', 100);
    const runSession = await RunSession.query().findById(1);
    const runSessionSplitCollection = await RunSessionSplitCollection.query().findById(1);
    const timePoints = await RunAttemptTimePoint.query();

    expect(timePoints).to.have.lengthOf(0);
    expect(runSession.current_session_split_id).to.eql(2);
    expect(runSessionSplitCollection.current_split_step_id).to.eql(2);
  });

  it('activates split when containing deleted splits', async () => {
    await fixtures.freshWithDeletedSplitsFixture();
    await setActiveSplitHandler({}, '7', 5000);
  });

  it('activates when run_sessions.current_session_split_id is null', async () => {
    await fixtures.missingCurrentSessionSplitFixture();
    await setActiveSplitHandler({}, '2', 5000);
  });

  it('activates when timer is started', async () => {
    await fixtures.timerStartedFixture();
    await setActiveSplitHandler({}, '3', 5000);

    const [t1, t2] = await RunAttemptTimePoint.query()
      .select('run_session_split_id', 'event_type')
      .orderBy('id', 'desc');

    expect(t1).to.eql({run_session_split_id: 11, event_type: 'split_started'});
    expect(t2).to.eql({run_session_split_id: 9, event_type: 'split_stopped'});
  });

  it('activates when timer is stopped', async () => {
    await fixtures.timerStoppedFixture();
    await setActiveSplitHandler({}, '3', 5000);

    const [t1, t2] = await RunAttemptTimePoint.query()
      .select('run_session_split_id', 'event_type')
      .orderBy('id', 'desc');

    expect(t1).to.eql({run_session_split_id: 10, event_type: 'attempt_stopped'});
    expect(t2).to.eql({run_session_split_id: 10, event_type: 'split_stopped'});
  });

  it('does not set active split if does not exist', async () => {
    const res = await setActiveSplitHandler({}, '3', 5000);
    expect(res).to.be.undefined;
  });
});
