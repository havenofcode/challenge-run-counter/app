import {Transaction} from 'objection';
import {IpcMainInvokeEvent} from 'electron';
import {SplitStep} from '../../model/split_step';
import {setActiveSplitWithTransaction} from './set_active_split';

export const setPrevActiveSplitWithTransaction = async (
  trx: Transaction,
  id: string,
  time: number,
) => {
  const splitStep = await SplitStep.query()
    .transacting(trx)
    .findById(id);

  const nextSplitStep = await SplitStep.query()
    .transacting(trx)
    .where('split_collection_id', splitStep.split_collection_id)
    .where('order', '<', splitStep.order)
    .where('deleted', 0)
    .orderBy('order', 'desc')
    .first();

  if (nextSplitStep) {
    await setActiveSplitWithTransaction(trx, `${nextSplitStep.id}`, time);
  }

}

export const setPrevActiveSplitHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string,
  time: number,
): Promise<void> => {
  return SplitStep.transaction((trx) => {
    return setPrevActiveSplitWithTransaction(trx, id, time);
  });
}
