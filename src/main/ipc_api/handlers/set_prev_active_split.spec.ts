import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {setPrevActiveSplitHandler} from './';
import {RunSession} from '../../model';

describe('set_next_active_split', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(setPrevActiveSplitHandler).to.be.a('function');
  });

  it('selects from 1 to 1', async () => {
    await fixtures.freshInstallFixture();
    await setPrevActiveSplitHandler({}, '1', 100);
    const runSession = await RunSession.query().findById(1);
    expect(runSession.current_session_split_id).to.eql(1);
  });

  it('selects from 3 to 2', async () => {
    await fixtures.freshInstallFixture();
    await setPrevActiveSplitHandler({}, '3', 100);
    const runSession = await RunSession.query().findById(1);
    expect(runSession.current_session_split_id).to.eql(2);
  });
});
