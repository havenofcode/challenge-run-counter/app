import {IpcMainInvokeEvent} from 'electron';
import {RunSessionSplit} from '../../model/run_session_split';
import {SplitStep} from '../../model/split_step';
import {RunSession} from '../../model/run_session';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';

export const createSplitStepHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<SplitStep> & Pick<SplitStep, "split_collection_id">
): Promise<SplitStep> => {
  const res = await SplitStep.transaction(async (trx): Promise<SplitStep> => {
    const getOrderParam = async () => {
      if (typeof data.order === 'number') {
        return data.order;
      }

      const [lastStep] = await SplitStep.query()
        .transacting(trx)
        .where({
          split_collection_id: data.split_collection_id,
        })
        .orderBy('order', 'desc')
        .limit(1);

      if (lastStep) {
        return lastStep.order + 1;
      }

      return 0;
    }

    const {id} = await SplitStep.query()
      .transacting(trx)
      .insert({...data, order: await getOrderParam()});

    const splitStep = await SplitStep.query()
      .transacting(trx)
      .findById(id);

    const splitCollection = await SplitCollection.query()
      .transacting(trx)
      .findById(data.split_collection_id);

    const runScenario = await RunScenario.query()
      .transacting(trx)
      .findById(splitCollection.run_scenario_id);

    const [runSession] = await RunSession.query()
      .transacting(trx)
      .where('run_scenario_id', runScenario.id)
      .orderBy('created_at', 'desc');

    await RunSessionSplit.query()
      .transacting(trx)
      .insert({
        run_session_id: runSession.id,
        split_step_id: splitStep.id,
      });

    return splitStep;
  });

  return res;
}
