import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {getAppSettingHandler} from './';

describe('IpcApi::getAppSettingHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getAppSettingHandler).to.be.a('function');
  });

  it('returns on fresh install', async () => {
    await fixtures.freshInstallFixture();
    const appSetting = await getAppSettingHandler();
    expect(appSetting.id).to.eql(1);
  });
});
