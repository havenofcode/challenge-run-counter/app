import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {getAllRunSessionsHandler} from './get_all_run_sessions';
import {SplitCollection} from '../../model/split_collection';
import {RunSession} from '../../model/run_session';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';

describe('IpcApi::getAllRunSessionsHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 1'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });

    for (let i = 0; i < 20; ++i) {
      // in memory database is too fast ordering by date returns
      // different results
      await new Promise<void>((resolve) => {
        setTimeout(async () => {
          await RunSession.query().insert({id: i, run_scenario_id: 69});
          resolve();
        }, 2);
      })
    }
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getAllRunSessionsHandler).to.be.a('function');
  });

  it('returns all run sessions with offset == 0', async () => {
    const all = await getAllRunSessionsHandler({}, {id: 69, offset: 0, limit: 10});
    let dec = 20;

    all.forEach((row) => {
      expect(row.id).to.eql(--dec);
    });
  });

  it('returns all run session with offset > 0', async () => {
    const all = await getAllRunSessionsHandler({}, {id: 69, offset: 10, limit: 10});
    let dec = 10;

    all.forEach((row) => {
      expect(row.id).to.eql(--dec);
    });
  });
});
