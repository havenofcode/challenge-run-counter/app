import {IpcMainInvokeEvent} from 'electron';
import {Transaction} from 'objection';

import {
  RunSession,
  RunScenario,
  RunSessionSplit,
} from '../../model';

import {
  ISplitPageData,
  ISplitPageSplit,
  ISplitPageCollection,
} from '../../../common/model';

import {
  getRunScenarioWithTransaction,
} from './';

export const getSplitPageDataWithTransaction = async (
  trx: Transaction,
  runScenarioId: string,
): Promise<ISplitPageData> => {
  const runScenario = await getRunScenarioWithTransaction(trx, runScenarioId);

  if (!runScenario) {
    throw new Error('run scenario does not exist');
  }

  const lastRunSession = await RunSession.query()
    .transacting(trx)
    .where('run_scenario_id', runScenarioId)
    .orderBy('created_at', 'desc')
    .first();

  const runSession = await RunSession.query()
    .transacting(trx)
    .withGraphFetched(
      [
        '[run_session_splits.[split_step]',
        'current_session_split.[split_step]',
        'run_session_split_collections.[split_collection, run_attempt_time_points]]'
      ].join(', '),
      {minimize: true}
    )
    .modifyGraph('run_session_split_collections.run_attempt_time_points', builder => {
      builder.orderBy('time', 'asc');
    })
    .modifyGraph('run_session_split_collections.split_collection', builder => {
      builder.where('deleted', 0);
    })
    .modifyGraph('run_session_splits.[split_step]', builder => {
      builder.where('deleted', 0);
    })
    .findById(lastRunSession.id);

  let currentSplitCollection: ISplitPageCollection;
  const allSplitCollections: ISplitPageCollection[] = [];
  const runSessionSplitsBySplitStep: {[id: string]: ISplitPageSplit} = {}

  const unsortedSplits: ISplitPageSplit[] = [];
  const {split_collection_id} = runSession.current_session_split.split_step;

  for (let i = 0; i < runSession.run_session_splits.length; ++i) {
    const runSessionSplit = runSession.run_session_splits[i];
    const {split_step} = runSessionSplit

    const item = {
      splitStep: split_step,
      data: runSessionSplit,
    }

    if (item.splitStep) {
      runSessionSplitsBySplitStep[item.splitStep.id] = item;

      if (split_step.split_collection_id === split_collection_id) {
        unsortedSplits.push(item);
      }
    }
  }

  for (let i = 0; i < runSession.run_session_split_collections.length; ++i) {
    const sessionCollection = runSession.run_session_split_collections[i];
    const currentSplitId = sessionCollection.current_split_step_id;

    if (!sessionCollection.split_collection) {
      continue;
    }

    const item = {
      data: sessionCollection,
      splitCollection: sessionCollection.split_collection,
      currentSplit: runSessionSplitsBySplitStep[currentSplitId],
    }

    allSplitCollections.push(item);

    if (
      runSession.run_session_split_collections[i].split_collection_id ===
      runSession.current_session_split.split_step.split_collection_id
    ) {
      currentSplitCollection = item
    }
  }

  const splits = unsortedSplits.sort((A, B) => {
    const a = A.splitStep.order;
    const b = B.splitStep.order;
    if (a > b) {
      return 1;
    } else if (a < b) {
      return -1;
    } else {
      return 0;
    }
  }).filter(({splitStep}) => splitStep.deleted === 0);

  const bestSegments = await Promise.all(
    splits.map(async (split) => {
      const data = await RunSessionSplit.query()
        .transacting(trx)
        .where('split_step_id', split.splitStep.id)
        .andWhere('time', '>', 0)
        .orderBy('time', 'asc')
        .first();

      if (!data) {
        return {
          data: split.data,
          splitStep: split.splitStep,
        }
      }

      return {
        data,
        splitStep: split.splitStep,
      }
    })
  );

  return {
    runScenario,
    runSession: lastRunSession,
    splitCollections: allSplitCollections,
    bestSegments,
    splits,
    currentSplitCollection,
    currentSplit: {
      splitStep: runSession.current_session_split.split_step,
      data: runSession.current_session_split,
    }
  }
}

export const getSplitPageDataHandler = (
  event: Partial<IpcMainInvokeEvent>,
  runScenarioId: string,
): Promise<ISplitPageData> => {
  return RunScenario.transaction((trx: Transaction) => {
    return getSplitPageDataWithTransaction(trx, runScenarioId);
  });
}
