import {PreferencesWindow} from '../../windows/preferences';

export const requestPreferencesWindowHandler = async (): Promise<void> => {
  PreferencesWindow.open();
  return Promise.resolve();
}
