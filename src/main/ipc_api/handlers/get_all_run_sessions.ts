import {IpcMainInvokeEvent} from 'electron';

import {IPaginatedOneToManyParams} from '../../../common';

import {
  RunSession,
} from '../../model/run_session';

export async function getAllRunSessionsHandler(
  event: Partial<IpcMainInvokeEvent>,
  params: IPaginatedOneToManyParams,
): Promise<RunSession[]> {
  return await RunSession.query()
    .where('run_scenario_id', params.id)
    .offset(params.offset)
    .limit(params.limit)
    .orderBy('created_at', 'desc');
}
