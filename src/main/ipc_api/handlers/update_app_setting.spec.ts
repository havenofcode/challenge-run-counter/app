import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {AppSetting} from '../../model';
import {updateAppSettingHandler} from './';

describe('IpcApi::updateAppSettingHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(updateAppSettingHandler).to.be.a('function');
  });

  it('updates nothing', async () => {
    await fixtures.freshInstallFixture();
    const subjectBefore = await AppSetting.query().findById(1);
    await updateAppSettingHandler({}, {id: 1});
    const subjectAfter = await AppSetting.query().findById(1);
    expect(subjectBefore).to.eql({...subjectAfter, updated_at: null});
  });

  it('updates name', async () => {
    await fixtures.freshInstallFixture();
    await updateAppSettingHandler({}, {id: 1, obs_browser_host: 'banana'});
    const {obs_browser_host} = await AppSetting.query().findById(1);
    expect(obs_browser_host).to.eql('banana');
  });
});
