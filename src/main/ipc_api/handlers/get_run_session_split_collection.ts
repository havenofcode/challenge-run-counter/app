import {IpcMainInvokeEvent} from 'electron';

import {
  RunSessionSplitCollection,
} from '../../model/run_session_split_collection';

export const getRunSessionSplitCollectionHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string
): Promise<RunSessionSplitCollection> => {
  const res = await RunSessionSplitCollection
    .query()
    .withGraphJoined('[split_collection, current_split_step, run_session]')
    .findById(id);

  return res;
}
