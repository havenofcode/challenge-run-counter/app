import {IpcMainInvokeEvent} from 'electron';

import {
  RunSession,
} from '../../model/run_session';

export const getRunSessionHandler = async (event: Partial<IpcMainInvokeEvent>, id: string): Promise<RunSession> => {
  const runSession = await RunSession
    .query()
    .withGraphJoined([
      '[run_session_splits.[split_step]',
      'current_session_split.[split_step]',
      'run_session_split_collections.[split_collection]]'
    ].join(', '))
    .where('run_session_splits:split_step.deleted', '0')
    .where('run_session_split_collections:split_collection:deleted', '0')
    .orderBy('run_session_splits.created_at', 'asc')
    .findById(id);

  return runSession;
}
