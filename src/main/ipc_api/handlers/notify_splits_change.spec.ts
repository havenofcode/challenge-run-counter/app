import {expect} from 'chai';
import {createSandbox, SinonStub} from 'sinon';
import {DisplayServer} from '../../../ws';
import {notifySplitsChangeHandler} from './';

describe('IpcApi::notifySplitsChangeHandler', () => {
  const sandbox = createSandbox();

  beforeEach(async () => {
    sandbox.stub(DisplayServer, 'sendSplits');
  });

  afterEach(async () => {
    sandbox.restore();
  });

  it('exists', () => {
    expect(notifySplitsChangeHandler).to.be.a('function');
  });

  it('returns on fresh install', async () => {
    await notifySplitsChangeHandler();
    expect((DisplayServer.sendSplits as unknown as SinonStub).getCalls()).to.have.lengthOf(1);
  });
});
