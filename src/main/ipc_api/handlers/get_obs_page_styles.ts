import {IpcMainInvokeEvent} from 'electron';
import {AppSetting} from '../../model/app_setting';

export const getObsPageStylesHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id = '1',
): Promise<string> => {
  const appSetting = await AppSetting.query().findById(id);
  return await appSetting.readObsPageStyles();
}
