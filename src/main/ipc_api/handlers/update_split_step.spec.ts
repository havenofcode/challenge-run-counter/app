import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {updateSplitStepHandler} from './update_split_step';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';

describe('IpcApi::updateSplitStepHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(updateSplitStepHandler).to.be.a('function');
  });

  it('creates split step', async () => {
    const res = await updateSplitStepHandler({}, {
      id: 1337,
      name: 'changed split step 1',
    });

    const updated = await SplitStep.query().findById(1337);

    expect(res.name).to.eql('changed split step 1');
    expect(updated).to.eql(res);
    expect(await SplitStep.query().findById(res.id)).to.eql(res);
  });
});
