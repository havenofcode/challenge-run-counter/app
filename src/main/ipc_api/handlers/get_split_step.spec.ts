import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';
import {getSplitStepHandler} from './';

describe('IpcApi::getSplitStepHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1,
      name: 'split step 1',
      split_collection_id: 420,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getSplitStepHandler).to.be.a('function');
  });

  it('returns split step', async () => {
    const splitStep = await getSplitStepHandler({}, '1');

    expect(splitStep).to.contain({
      id: 1,
      name: 'split step 1',
      split_collection_id: 420,
    });
  });
});
