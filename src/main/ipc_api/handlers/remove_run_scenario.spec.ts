import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {AppSetting, RunScenario} from '../../model';
import {removeRunScenarioHandler} from './';

describe('IpcApi::removeRunScenarioHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(removeRunScenarioHandler).to.be.a('function');
  });

  it('remove last run scenario', async () => {
    await fixtures.freshInstallFixture();
    await removeRunScenarioHandler({}, '1');
    const runScenarios = await RunScenario.query().orderBy('id', 'asc');
    expect(runScenarios).to.have.lengthOf(1);
    expect(runScenarios[0].deleted).to.eql(1);
  });

  it('sets last created run scenario as current selected run scenario', async () => {
    await fixtures.multipleRunScenariosFixture();
    await removeRunScenarioHandler({}, '2');
    const runScenarios = await RunScenario.query().orderBy('id', 'asc');
    const appSetting = await AppSetting.query().findById(1);
    expect(runScenarios).to.have.lengthOf(2);
    expect(runScenarios[0].deleted).to.eql(0);
    expect(runScenarios[1].deleted).to.eql(1);
    expect(appSetting.current_run_scenario_id).to.eql(1);
  });
});
