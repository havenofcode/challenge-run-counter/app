import {IpcMainInvokeEvent} from 'electron';
import {AppSetting} from '../../model/app_setting';
import {Hotkeys} from '../hotkeys';
import {ITrySetGlobalShortcutParams} from '../../../common';

export const trySetGlobalShortcutHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  params: ITrySetGlobalShortcutParams,
): Promise<AppSetting> => {
  const {name, value} = params;
  const appSetting = await AppSetting.query().findById(1);
  await appSetting.setHotkey(name, value);
  const hotkeys = Hotkeys.get();
  await hotkeys.init();
  return appSetting;
}
