import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {createRunScenarioHandler} from './create_run_scenario';
import {RunScenario} from '../../model/run_scenario';

describe('IpcApi::createRunScenarioHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(createRunScenarioHandler).to.be.a('function');
  });

  it('creates a run scenario', async () => {
    await createRunScenarioHandler({}, {
      id: 69,
      name: 'run scenario 69'
    });

    const [runScenario] = await RunScenario.query();

    expect(runScenario).to.contain({
      id: 69,
      name: 'run scenario 69'
    });
  });
});
