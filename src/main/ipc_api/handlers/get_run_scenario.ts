import {IpcMainInvokeEvent} from 'electron';
import {Transaction} from 'objection';

import {
  RunScenario,
} from '../../model/run_scenario';

export const getRunScenarioWithTransaction = async (
  trx: Transaction,
  id: string,
) => {
  return await RunScenario
    .query()
    .transacting(trx)
    .withGraphJoined('split_collections.[split_steps]')
    .where('split_collections.deleted', '0')
    .orderBy('split_collections.created_at', 'asc')
    .findById(id);
}

export const getRunScenarioHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string
): Promise<RunScenario> => {
  return RunScenario.transaction((trx) => getRunScenarioWithTransaction(trx, id));
}
