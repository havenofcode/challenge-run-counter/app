import {dialog, OpenDialogOptions, IpcMainInvokeEvent} from 'electron';

export const openChooseDirectoryHandlerElectronApi = {
  dialog,
}

export const openChooseDirectoryHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  opts: OpenDialogOptions,
) => {
  const res = await openChooseDirectoryHandlerElectronApi.dialog.showOpenDialog(opts);
  return res;
}
