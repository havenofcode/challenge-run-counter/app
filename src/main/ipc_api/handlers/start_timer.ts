import {IpcMainInvokeEvent} from 'electron';
import {Transaction} from 'objection';

import {
  RunSession,
  SplitStep,
  RunSessionSplitCollection,
  RunSessionSplit,
  RunAttemptTimePoint,
} from '../../model';

import {getCurrentSessionWithTransaction} from './';

export const startTimerWithTransaction = async (
  trx: Transaction,
  startTime: number,
) => {
  // get current runSessionSplitCollection
  const runSession = await getCurrentSessionWithTransaction(trx);

  const runSessionSplit = await RunSessionSplit.query()
    .transacting(trx)
    .findById(runSession.current_session_split_id || 0);

  // get split collection user is currently viewing
  const splitStep = await SplitStep.query()
    .transacting(trx)
    .findById(runSessionSplit.split_step_id);

  const runSessionSplitCollection = await RunSessionSplitCollection.query()
    .transacting(trx)
    .where({
      run_session_id: runSession.id,
      split_collection_id: splitStep.split_collection_id,
    })
    .first();

  if (!runSessionSplitCollection) {
    throw new Error('internal error: unable to detect current session split collection');
  }

  const runAttemptTimePoints = await RunAttemptTimePoint.query()
    .transacting(trx)
    .where({
      run_session_split_collection_id: runSessionSplitCollection.id,
    })
    .orderBy('time', 'desc');

  for (let i = 0; i < runAttemptTimePoints.length; ++i) {
    const point = runAttemptTimePoints[i];

    if (point.event_type === 'attempt_started') {
      throw new Error('timer already started');
      break;
    }

    if (point.event_type === 'attempt_stopped') {
      break;
    }
  }

  for (let i = 0; i < runAttemptTimePoints.length; ++i) {
    const point = runAttemptTimePoints[i];

    if (
      (point.event_type === 'split_started' || point.event_type === 'split_stopped') &&
      point.run_session_split_id === runSession.current_session_split_id
    ) {
      if (point.event_type === 'split_started') {
        throw new Error('internal error, trying to start timepoint twice');
      }
      break;
    }
  }

  await RunAttemptTimePoint.query()
    .transacting(trx)
    .insert({
      run_session_split_collection_id: runSessionSplitCollection.id,
      run_session_split_id: runSessionSplit.id,
      time: startTime,
      event_type: 'attempt_started',
    });

  await RunAttemptTimePoint.query()
    .transacting(trx)
    .insert({
      run_session_split_collection_id: runSessionSplitCollection.id,
      run_session_split_id: runSessionSplit.id,
      time: startTime,
      event_type: 'split_started',
    });
}

export const startTimerHandler = (
  event: Partial<IpcMainInvokeEvent>,
  startTime: number,
): Promise<void> => {
  return RunSession.transaction((trx) => {
    return startTimerWithTransaction(trx, startTime);
  });
}
