import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';

import {
  setNextActiveSplitHandler,
  getActiveSplitHandler,
} from './';

import {
  AppSetting,
  RunAttemptTimePoint,
  RunScenario,
  RunSession,
  SplitCollection,
  SplitStep,
} from '../../model';

describe('set_next_active_split', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 1,
      name: 'run scenario 1'
    });

    await AppSetting.query().insert({
      id: 1,
      current_run_scenario_id: 1,
    });

    await SplitCollection.query().insertGraph([
      {
        id: 1,
        name: 'split collection 1',
        run_scenario_id: 1,
        split_steps: [
          {id: 1, name: '1', split_collection_id: 1, order: 0},
          {id: 2, name: '2', split_collection_id: 1, order: 1},
          {id: 3, name: '3', split_collection_id: 1, order: 2},
          {id: 4, name: '4', split_collection_id: 1, order: 3},
        ],
      },
      {
        id: 2,
        name: 'split collection 2',
        run_scenario_id: 1,
        split_steps: [
          {id: 5, name: '1', split_collection_id: 2, order: 0},
          {id: 6, name: '2', split_collection_id: 2, order: 1},
          {id: 7, name: '3', split_collection_id: 2, order: 2},
          {id: 8, name: '4', split_collection_id: 2, order: 3},
        ],
      },
    ]);

    await RunSession.query().insertGraph({
      id: 1,
      run_scenario_id: 1,
      run_session_split_collections: [
        {
          id: 1,
          run_session_id: 1,
          current_split_step_id: 1,
          split_collection_id: 1,
        },
        {
          id: 2,
          run_session_id: 1,
          current_split_step_id: 5,
          split_collection_id: 2,
        },
      ],
      run_session_splits: [
        {id: 1, run_session_id: 1, split_step_id: 1, hits_way: 1, hits_boss: 0, time: 0},
        {id: 2, run_session_id: 1, split_step_id: 2, hits_way: 0, hits_boss: 1, time: 0},
        {id: 3, run_session_id: 1, split_step_id: 3, hits_way: 1, hits_boss: 0, time: 0},
        {id: 4, run_session_id: 1, split_step_id: 4, hits_way: 0, hits_boss: 1, time: 0},
        {id: 5, run_session_id: 1, split_step_id: 5, hits_way: 1, hits_boss: 0, time: 0},
        {id: 6, run_session_id: 1, split_step_id: 6, hits_way: 0, hits_boss: 1, time: 0},
        {id: 7, run_session_id: 1, split_step_id: 7, hits_way: 1, hits_boss: 0, time: 0},
        {id: 8, run_session_id: 1, split_step_id: 8, hits_way: 0, hits_boss: 1, time: 0},
      ],
    });

    await RunSession.query().update({current_session_split_id: 1});

    await RunAttemptTimePoint.query().insertGraph([
      {run_session_split_id: 1, run_session_split_collection_id: 1, event_type: 'attempt_started', time: 0},
      {run_session_split_id: 1, run_session_split_collection_id: 1, event_type: 'split_started', time: 0},
      {run_session_split_id: 1, run_session_split_collection_id: 1, event_type: 'split_stopped', time: 100},
      {run_session_split_id: 2, run_session_split_collection_id: 1, event_type: 'split_started', time: 100},
      {run_session_split_id: 2, run_session_split_collection_id: 1, event_type: 'split_stopped', time: 200},
      {run_session_split_id: 2, run_session_split_collection_id: 1, event_type: 'attempt_stopped', time: 20},
    ]);
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('sets next active split', async () => {
    const date1 = new Date().getTime() + 100;
    const date2 = new Date().getTime() + 200;
    const date3 = new Date().getTime() + 300;
    const date4 = new Date().getTime() + 400;

    // return value is the next split step
    let nextVal = await setNextActiveSplitHandler({}, '1', date1);
    expect(nextVal).to.eql(await SplitStep.query().findById(2));
    expect((await getActiveSplitHandler()).id).to.eql(2);

    // next (3)
    nextVal = await setNextActiveSplitHandler({}, '2', date2);
    expect(nextVal).to.eql(await SplitStep.query().findById(3));
    expect((await getActiveSplitHandler()).id).to.eql(3);

    // next (4)
    nextVal = await setNextActiveSplitHandler({}, '3', date3);
    expect(nextVal).to.eql(await SplitStep.query().findById(4));
    expect((await getActiveSplitHandler()).id).to.eql(4);

    // last split step stops timer when null returned
    nextVal = await setNextActiveSplitHandler({}, '4', date4);
    expect(nextVal).to.eql(null);
    expect((await getActiveSplitHandler()).id).to.eql(4);
  });
});
