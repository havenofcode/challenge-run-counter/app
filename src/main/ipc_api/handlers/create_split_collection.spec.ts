import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {createSplitCollectionHandler} from './create_split_collection';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {RunSession} from '../../model/run_session';
import {SplitStep} from '../../model/split_step';
import {RunSessionSplitCollection} from '../../model/run_session_split_collection';
import {RunSessionSplit} from '../../model/run_session_split';

describe('IpcApi::createRunScenarioHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 1'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });

    await RunSession.query().insert({
      id: 420,
      run_scenario_id: 69,
    });

    await RunSessionSplitCollection.query().insert({
      id: 420,
      run_session_id: 420,
      current_split_step_id: 1337,
      split_collection_id: 420,
    });

    await RunSessionSplit.query().insert({
      id: 1338,
      run_session_id: 420,
      split_step_id: 1337,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });

    await RunSessionSplit.query().insert({
      id: 1337,
      run_session_id: 420,
      split_step_id: 1337,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });

    await SplitStep.query().insert({
      id: 666,
      name: 'deleted',
      deleted: 1,
      split_collection_id: 420,
    });

    await RunSessionSplit.query().insert({
      id: 666,
      run_session_id: 420,
      split_step_id: 666,
      hits_way: 0,
      hits_boss: 0,
      time: 0,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(createSplitCollectionHandler).to.be.a('function');
  });

  it('creates a run split collection with an empty name', async () => {
    const splitCollection = await createSplitCollectionHandler({}, {
      name: '',
      run_scenario_id: 69,
    });
    expect(splitCollection.name).to.eql('');
  });
});
