import {IpcMainInvokeEvent} from 'electron';
import {SplitCollection} from '../../model/split_collection';

export const updateSplitCollectionHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<SplitCollection> & {id: number},
): Promise<SplitCollection> => {
  const {id} = data;
  await SplitCollection.query().findById(id).patch(data);
  return await SplitCollection.query().findById(id);
}
