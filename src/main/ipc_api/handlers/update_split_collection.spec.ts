import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {SplitCollection} from '../../model';
import {updateSplitCollectionHandler} from './';

describe('IpcApi::updateSplitCollectionHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(updateSplitCollectionHandler).to.be.a('function');
  });

  it('updates nothing', async () => {
    await fixtures.freshInstallFixture();
    const subjectBefore = await SplitCollection.query().findById(1);
    await updateSplitCollectionHandler({}, {id: 1});
    const subjectAfter = await SplitCollection.query().findById(1);
    expect(subjectBefore).to.eql({...subjectAfter, updated_at: null});
  });

  it('updates name', async () => {
    await fixtures.freshInstallFixture();
    await updateSplitCollectionHandler({}, {id: 1, name: 'banana'});
    const {name} = await SplitCollection.query().findById(1);
    expect(name).to.eql('banana');
  });
});
