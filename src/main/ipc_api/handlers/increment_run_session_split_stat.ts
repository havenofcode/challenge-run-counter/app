import {Transaction} from 'objection';
import {IpcMainInvokeEvent} from 'electron';
import {SplitStep} from '../../model/split_step';
import {RunSessionSplit} from '../../model/run_session_split';
import {getCurrentSessionWithTransaction} from './get_current_session';

export const incrementRunSessionSplitStatWithTransaction = async (trx: Transaction, statName: string) => {
  const runSession = await getCurrentSessionWithTransaction(trx);

  const runSessionSplit = await RunSessionSplit.query()
    .transacting(trx)
    .findById(runSession.current_session_split_id);

  const patch: Partial<RunSessionSplit> = {}

  switch (statName) {
    case 'hits_boss': {
      patch.hits_boss = ((runSessionSplit.hits_boss || 0) + 1);
      break;
    }
    case 'hits_way': {
      patch.hits_way = ((runSessionSplit.hits_way || 0) + 1);
      break;
    }
    default: {
      throw new Error('stat does not exist');
    }
  }

  await RunSessionSplit.query()
    .transacting(trx)
    .patch(patch)
    .findById(runSessionSplit.id);
}

export const incrementRunSessionSplitStatHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  statName: string,
): Promise<void> => {
  return SplitStep.transaction((trx) => {
    return incrementRunSessionSplitStatWithTransaction(trx, statName);
  });
}
