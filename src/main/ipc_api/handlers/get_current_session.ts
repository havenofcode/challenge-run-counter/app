import {Transaction} from 'objection';
import {AppSetting} from '../../model/app_setting';
import {RunSession} from '../../model/run_session';

export const getCurrentSessionWithTransaction = async (trx: Transaction): Promise<RunSession> => {
  const settings = await AppSetting.query()
    .transacting(trx)
    .findById(1);

  const runSession = await RunSession.query()
    .transacting(trx)
    .where('run_scenario_id', settings.current_run_scenario_id)
    .orderBy('created_at', 'desc')
    .first();

  return runSession;
}

export const getCurrentSessionHandler = () => {
  return AppSetting.transaction(async (trx) => {
    return getCurrentSessionWithTransaction(trx);
  });
}
