import {IpcMainInvokeEvent} from 'electron';

import {SplitStep} from '../../model/split_step';
import {setActiveSplitWithTransaction} from './set_active_split';

export const removeSplitStepHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string
): Promise<SplitStep> => {

  return await SplitStep.transaction(async (trx) => {
    await SplitStep.query()
      .transacting(trx)
      .findById(id)
      .patch({
        deleted: 1,
      });

    const splitStep = await SplitStep.query().transacting(trx).findById(id);

    const otherSplitSteps = await SplitStep.query()
      .transacting(trx)
      .where({
        split_collection_id: splitStep.split_collection_id,
        deleted: 0,
      })
      .orderBy('order', 'asc');

    if (otherSplitSteps.length === 0) {
      throw new Error('unable to delete last split step in collection');
    }

    for (let i = 0; i < otherSplitSteps.length; ++i) {
      await SplitStep.query()
        .transacting(trx)
        .findById(otherSplitSteps[i].id)
        .patch({order: i})
    }

    const [nextActiveSplit] = otherSplitSteps;
    if (nextActiveSplit) {
      await setActiveSplitWithTransaction(trx, `${nextActiveSplit.id}`, new Date().getTime());
    }

    return splitStep;
  });
}
