import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {getAllRunScenariosHandler} from './';

describe('IpcApi::getAllRunScenariosHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getAllRunScenariosHandler).to.be.a('function');
  });

  it('gets all run scenarios', async () => {
    await fixtures.multipleRunScenariosFixture();
    const res = await getAllRunScenariosHandler();
    expect(res).to.have.lengthOf(2);
  });
});
