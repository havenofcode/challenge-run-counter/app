import {IpcMainInvokeEvent} from 'electron';
import {DisplayServer} from '../../../ws';

export const notifySetActiveSplitHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string,
  time: number,
): Promise<void> => {
  DisplayServer.emitSetActiveSplit(id, time);
}
