import {IpcMainInvokeEvent} from 'electron';
import {DisplayServer} from '../../../ws';

export const notifySetCountByNameHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string,
  name: string,
  val: number,
): Promise<void> => {
  DisplayServer.emitSetCountByName(id, name, val);
  return Promise.resolve();
}
