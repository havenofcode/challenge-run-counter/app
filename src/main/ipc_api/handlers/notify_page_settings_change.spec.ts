import {expect} from 'chai';
import {createSandbox, SinonStub} from 'sinon';
import {DisplayServer} from '../../../ws';
import {notifyPageSettingsChangeHandler} from './';

describe('IpcApi::notifyPageSettingsChangeHandler', () => {
  const sandbox = createSandbox();

  beforeEach(async () => {
    sandbox.stub(DisplayServer, 'emitPageSettingsUpdate');
  });

  afterEach(async () => {
    sandbox.restore();
  });

  it('exists', () => {
    expect(notifyPageSettingsChangeHandler).to.be.a('function');
  });

  it('returns on fresh install', async () => {
    await notifyPageSettingsChangeHandler({}, {});
    expect((DisplayServer.emitPageSettingsUpdate as unknown as SinonStub).getCalls()).to.have.lengthOf(1);
  });
});
