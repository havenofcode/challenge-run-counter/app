import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';

import {
  RunSession,
  RunSessionSplit,
} from '../../model';

import {decrementRunSessionSplitStatHandler} from './';

describe('IpcApi::decrementRunSessionSplitStatHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(decrementRunSessionSplitStatHandler).to.be.a('function');
  });

  it('insures hits_boss stat is >= 0', async () => {
    await fixtures.freshInstallFixture();
    await decrementRunSessionSplitStatHandler({}, 'hits_boss');
    const runSession = await RunSession.query().findById(1);
    const runSessionSplit = await RunSessionSplit.query().findById(1);
    expect(runSession.current_session_split_id).to.eql(1);
    expect(runSessionSplit.hits_boss).to.eql(0);
    expect(runSessionSplit.hits_way).to.eql(0);
  });

  it('insures hits_way stat is >= 0', async () => {
    await fixtures.freshInstallFixture();
    await decrementRunSessionSplitStatHandler({}, 'hits_way');
    const runSession = await RunSession.query().findById(1);
    const runSessionSplit = await RunSessionSplit.query().findById(1);
    expect(runSession.current_session_split_id).to.eql(1);
    expect(runSessionSplit.hits_boss).to.eql(0);
    expect(runSessionSplit.hits_way).to.eql(0);
  });

  it('decreases hits_boss stat from 1 to 0', async () => {
    await fixtures.freshInstallFixture();
    await RunSessionSplit.query().patch({hits_boss: 1}).findById(1);
    await decrementRunSessionSplitStatHandler({}, 'hits_boss');
    const runSession = await RunSession.query().findById(1);
    const runSessionSplit = await RunSessionSplit.query().findById(1);
    expect(runSession.current_session_split_id).to.eql(1);
    expect(runSessionSplit.hits_boss).to.eql(0);
    expect(runSessionSplit.hits_way).to.eql(0);
  });

  it('decreases hits_boss stat from 2 to 1', async () => {
    await fixtures.freshInstallFixture();
    await RunSessionSplit.query().patch({hits_boss: 2}).findById(1);
    await decrementRunSessionSplitStatHandler({}, 'hits_boss');
    const runSession = await RunSession.query().findById(1);
    const runSessionSplit = await RunSessionSplit.query().findById(1);
    expect(runSession.current_session_split_id).to.eql(1);
    expect(runSessionSplit.hits_boss).to.eql(1);
    expect(runSessionSplit.hits_way).to.eql(0);
  });

  it('decreases hits_way stat from 2 to 1', async () => {
    await fixtures.freshInstallFixture();
    await RunSessionSplit.query().patch({hits_way: 2, hits_boss: 2}).findById(1);
    await decrementRunSessionSplitStatHandler({}, 'hits_way');
    const runSession = await RunSession.query().findById(1);
    const runSessionSplit = await RunSessionSplit.query().findById(1);
    expect(runSession.current_session_split_id).to.eql(1);
    expect(runSessionSplit.hits_boss).to.eql(2);
    expect(runSessionSplit.hits_way).to.eql(1);
  });

  it('decreases hits_way stat from 2 to 1', async () => {
    await fixtures.freshInstallFixture();
    let failed = false;

    try {
      await decrementRunSessionSplitStatHandler({}, 'banana');
    } catch (err) {
      failed = true;
      expect(err.message).to.eql('stat does not exist');
    }

    expect(failed).to.be.true;
  });
});
