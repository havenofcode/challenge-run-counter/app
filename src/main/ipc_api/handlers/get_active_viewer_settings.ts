import {
  ViewerSetting,
} from '../../model';

import {
  DisplayServer,
} from '../../../ws';

export const getActiveViewerSettingsHandler = async (): Promise<string[]> => {
  const displayServer = DisplayServer.get();

  if (displayServer) {
    const activeConfigs = Object.keys(displayServer.connectedConfigurations);
    if (activeConfigs.length > 0) {
      return activeConfigs;
    }
  }

  const lastActiveConfiguration = await ViewerSetting.query()
    .orderBy('updated_at', 'desc')
    .orderBy('created_at', 'desc')
    .findOne({});

  if (lastActiveConfiguration) {
    return [lastActiveConfiguration.name];
  }

  return [];
}
