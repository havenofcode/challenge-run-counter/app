import {IpcMainInvokeEvent} from 'electron';

import {
  RunScenario,
} from '../../model/run_scenario';

import {SplitCollection} from '../../model/split_collection';
import {SplitStep} from '../../model/split_step';
import {RunSession} from '../../model/run_session';
import {RunSessionSplitCollection} from '../../model/run_session_split_collection';
import {RunSessionSplit} from '../../model/run_session_split';

export const createRunScenarioHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<RunScenario>,
): Promise<RunScenario> => {
  const res = RunScenario.transaction(async (trx) => {
    const runScenario = await RunScenario.query().transacting(trx).insert(data);

    const splitCollection = await SplitCollection.query().transacting(trx).insert({
      run_scenario_id: runScenario.id,
      name: '',
    });

    const splitStep = await SplitStep.query().transacting(trx).insert({
      split_collection_id: splitCollection.id,
      name: '',
      order: 0,
    });

    const runSession = await RunSession.query().transacting(trx).insert({
      run_scenario_id: runScenario.id,
    });

    await RunSessionSplitCollection.query()
      .transacting(trx)
      .insert({
        run_session_id: runSession.id,
        current_split_step_id: splitStep.id,
        split_collection_id: splitCollection.id,
      });

    const runSessionSplit = await RunSessionSplit.query()
      .transacting(trx)
      .insert({
        run_session_id: runSession.id,
        split_step_id: splitStep.id,
      });

    await RunSession.query()
      .transacting(trx)
      .findById(runSession.id)
      .patch({
        current_session_split_id: runSessionSplit.id,
      });

    return runScenario;
  });

  return res;
}
