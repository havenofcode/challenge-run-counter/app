import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {startTimerHandler} from './';

import {
  RunSessionSplit,
  RunSessionSplitCollection,
  RunSession,
  RunScenario,
  SplitCollection,
  SplitStep,
  AppSetting,
  RunAttemptTimePoint,
} from '../../model';

describe('IpcApi::start_timer', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 1,
      name: 'run scenario 1'
    });

    await RunSession.query().insert({
      id: 1,
      run_scenario_id: 1,
    })

    await SplitCollection.query().insert({
      id: 1,
      name: 'split collection 1',
      run_scenario_id: 1,
    });

    await SplitStep.query().insert({
      id: 1,
      name: 'first ever split step',
      order: 0,
      split_collection_id: 1,
    });

    await RunSessionSplit.query().insert({
      id: 1,
      run_session_id: 1,
      split_step_id: 1,
    });

    await RunSessionSplitCollection.query().insert({
      id: 1,
      run_session_id: 1,
      current_split_step_id: 1,
      split_collection_id: 1,
    });

    await RunSession.query().findById(1).patch({
      id: 1,
      current_session_split_id: 1,
    });

    await AppSetting.query().insert({
      id: 1,
      current_run_scenario_id: 1,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(startTimerHandler).to.be.a('function');
  });

  it('starts the timer', async () => {
    const time = new Date().getTime() - 250;
    await startTimerHandler({}, time);
    const runTimes = await RunAttemptTimePoint.query()
      .where('event_type', 'attempt_stopped')
      .orWhere('event_type', 'attempt_started');
    expect(runTimes.length).to.eql(1);
    expect(runTimes[0].time).to.eql(time);
    expect(runTimes[0].event_type).to.eql('attempt_started');
  });

  it('throw error if already started', async () => {
    const time = new Date().getTime() - 250;
    const nextTime = new Date().getTime() - 100;
    await startTimerHandler({}, time);
    let failed = false;

    try {
      await startTimerHandler({}, nextTime);
    } catch (err) {
      failed = true;
    }

    expect(failed).to.be.true;
  });
});
