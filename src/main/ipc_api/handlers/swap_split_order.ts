import {IpcMainInvokeEvent} from 'electron';
import {Transaction} from 'objection';
import {SplitStep} from '../../model';
import {ISwapSplitOrderParams} from '../../../common/bridge';

export const swapSplitOrderWithTransaction = async (
  trx: Transaction,
  data: ISwapSplitOrderParams
) => {
  const from = await SplitStep.query().transacting(trx).findById(data.fromId);
  const to = await SplitStep.query().transacting(trx).findById(data.toId);

  await Promise.all([
    SplitStep.query().transacting(trx).findById(from.id).patch({order: to.order}),
    SplitStep.query().transacting(trx).findById(to.id).patch({order: from.order}),
  ]);
}

export const swapSplitOrderHandler = (
  event: Partial<IpcMainInvokeEvent>,
  data: ISwapSplitOrderParams,
): Promise<void> => {
  return SplitStep.transaction((trx) => {
    return swapSplitOrderWithTransaction(trx, data);
  });
}
