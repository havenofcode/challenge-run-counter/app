import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {getRunScenarioHandler} from './get_run_scenario';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';

describe('IpcApi::getRunScenarioHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split step 1',
      split_collection_id: 420,
    });

    await SplitCollection.query().insert({
      id: 1337,
      name: 'split collection 1337',
      run_scenario_id: 69,
    });

    await SplitCollection.query().insert({
      id: 1024,
      name: 'split collection 1024',
      run_scenario_id: 69,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getRunScenarioHandler).to.be.a('function');
  });

  it('returns split collection', async () => {
    const ipcMainInvokeEvent = {}
    const runScenario = await getRunScenarioHandler(ipcMainInvokeEvent, '69');
    expect(runScenario.id).to.eql(69);
    expect(runScenario.split_collections).to.have.lengthOf(3);
    const [firstSplitCollection] = runScenario.split_collections;
    expect(firstSplitCollection.id).to.eql(420);
    expect(firstSplitCollection.split_steps).to.have.lengthOf(1);
  });
});
