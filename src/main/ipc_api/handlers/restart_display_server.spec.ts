import {expect} from 'chai';
import {createSandbox, SinonStub} from 'sinon';
import {DisplayServer} from '../../../ws';
import {restartDisplayServerHandler} from './';

describe('IpcApi::restartDisplayServerHandler', () => {
  const sandbox = createSandbox();

  beforeEach(async () => {
    sandbox.stub(DisplayServer, 'restart');
  });

  afterEach(async () => {
    sandbox.restore();
  });

  it('exists', () => {
    expect(restartDisplayServerHandler).to.be.a('function');
  });

  it('returns on fresh install', async () => {
    await restartDisplayServerHandler();
    expect((DisplayServer.restart as unknown as SinonStub).getCalls()).to.have.lengthOf(1);
  });
});
