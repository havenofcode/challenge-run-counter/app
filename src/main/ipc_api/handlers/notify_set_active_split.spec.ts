import {expect} from 'chai';
import {createSandbox, SinonStub} from 'sinon';
import {DisplayServer} from '../../../ws';
import {notifySetActiveSplitHandler} from './';

describe('IpcApi::notifySetActiveSplitHandler', () => {
  const sandbox = createSandbox();

  beforeEach(async () => {
    sandbox.stub(DisplayServer, 'emitSetActiveSplit');
  });

  afterEach(async () => {
    sandbox.restore();
  });

  it('exists', () => {
    expect(notifySetActiveSplitHandler).to.be.a('function');
  });

  it('returns on fresh install', async () => {
    await notifySetActiveSplitHandler({}, '1', 69);
    expect((DisplayServer.emitSetActiveSplit as unknown as SinonStub).getCalls()).to.have.lengthOf(1);
  });
});
