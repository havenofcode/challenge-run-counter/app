import {DisplayServer} from '../../../ws';

export const notifySplitsChangeHandler = async (): Promise<void> => {
  await DisplayServer.sendSplits();
}
