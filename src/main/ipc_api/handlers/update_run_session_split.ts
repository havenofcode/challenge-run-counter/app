import {IpcMainInvokeEvent} from 'electron';

import {RunSessionSplit} from '../../model/run_session_split';

export const updateRunSessionSplitHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<RunSessionSplit>
): Promise<RunSessionSplit> => {
  const {id} = data;

  if (!id) {
    throw new Error(`params.id required`);
  }

  await RunSessionSplit.query().findById(id).patch(data);
  return await RunSessionSplit.query().findById(id);
}
