import {expect} from 'chai';
import {createSandbox} from 'sinon';
import {getRestoreDbTasks, RestoreTask} from '../../../test';
import {DisplayServer} from '../../../ws';
import {notifySetCountByNameHandler} from './';

describe('IpcApi::notifySetCountByNameHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;
  const sandbox = createSandbox();

  const fakeEmit = sandbox.spy();
  const fakeSocketIO = {
    of: () => ({emit: fakeEmit})
  }

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
    DisplayServer.instance = {
      io: fakeSocketIO,
    } as unknown as DisplayServer;
  });

  afterEach(async () => {
    await destroyDbTask();
    sandbox.restore();
    DisplayServer.instance = null;
  });

  it('exists', async () => {
    expect(notifySetCountByNameHandler).to.be.a('function');
  });

  it('sends info about count emitted', () => {
    notifySetCountByNameHandler({}, 'id', 'name', 69);
    const [eventName, id, name, val] = fakeEmit.getCall(0).args;
    expect(eventName).to.eql('set-count-by-name');
    expect(id).to.eql('id');
    expect(name).to.eql('name');
    expect(val).to.eql(69);
  })
});
