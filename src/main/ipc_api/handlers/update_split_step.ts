import {IpcMainInvokeEvent} from 'electron';

import {SplitStep} from '../../model/split_step';

export const updateSplitStepHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<SplitStep>
): Promise<SplitStep> => {
  const {id} = data;

  if (!id) {
    throw new Error('id required');
  }

  await SplitStep.query().findById(id).patch(data);
  return await SplitStep.query().findById(id);
}
