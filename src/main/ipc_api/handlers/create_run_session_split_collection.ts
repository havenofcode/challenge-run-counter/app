import {IpcMainInvokeEvent} from 'electron';

import {
  RunSessionSplitCollection,
} from '../../model/run_session_split_collection';

export const createRunSessionSplitCollectionHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<RunSessionSplitCollection>
): Promise<RunSessionSplitCollection> => {
  const {id} = await RunSessionSplitCollection.query().insert(data);
  return await RunSessionSplitCollection.query().findById(id);
}
