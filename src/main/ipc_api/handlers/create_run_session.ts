import {IpcMainInvokeEvent} from 'electron';
import {Transaction} from 'objection';

import {
  RunScenario,
  RunSession,
  RunSessionSplit,
  RunSessionSplitCollection,
  SplitCollection,
  SplitStep,
} from '../../model';

export const createRunSessionWithTransaction = async (
  trx: Transaction,
  data: Partial<RunSession> & {run_scenario_id: number}
) => {
  const {id} = await RunSession.query().transacting(trx).insert(data);

  const splitCollections = await SplitCollection.query()
    .transacting(trx)
    .where({
      run_scenario_id: data.run_scenario_id,
      deleted: 0,
    });

  // for each split collection in run scenario
  for (let i = 0; i < splitCollections.length; ++i) {
    const splitCollection = splitCollections[i];

    const splitStep = await SplitStep.query()
      .transacting(trx)
      .where({
        split_collection_id: splitCollection.id,
        deleted: 0,
      })
      .orderBy('order', 'asc')
      .first();

    await RunSessionSplitCollection.query()
      .transacting(trx)
      .insert({
        run_session_id: id,
        current_split_step_id: splitStep.id,
        split_collection_id: splitCollection.id,
      });

    const allSplitSteps = await SplitStep.query()
      .transacting(trx)
      .where({
        deleted: 0,
        split_collection_id: splitCollection.id,
      });

    const newSessionSplitSteps: RunSessionSplit[] = [];

    for (let y = 0; y < allSplitSteps.length; ++y) {
      const runSessionSplit = await RunSessionSplit.query()
        .transacting(trx)
        .insert({
          run_session_id: id,
          split_step_id: allSplitSteps[y].id,
        });

      newSessionSplitSteps.push(runSessionSplit);
    }

    if (i === 0) {
      await RunSession.query()
        .transacting(trx)
        .findById(id)
        .patch({
          current_session_split_id: newSessionSplitSteps[0].id,
        });
    }
  }

  return await RunSession.query().transacting(trx).findById(id)
}

export const createRunSessionHandler = (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<RunSession> & {run_scenario_id: number}
): Promise<RunSession> => {
  return RunScenario.transaction((trx) => {
    return createRunSessionWithTransaction(trx, data);
  });
}
