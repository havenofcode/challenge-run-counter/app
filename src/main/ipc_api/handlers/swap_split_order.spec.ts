import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {SplitStep} from '../../model';
import {swapSplitOrderHandler} from './';

describe('IpcApi::swapSplitOrderHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(swapSplitOrderHandler).to.be.a('function');
  });

  it('swaps split steps on fresh install', async () => {
    await fixtures.freshInstallFixture();
    await swapSplitOrderHandler({}, {
      fromId: 2,
      toId: 3
    });

    const splitSteps = await SplitStep.query().orderBy('order', 'asc');

    expect(splitSteps).to.have.lengthOf(4);
    expect(splitSteps[0].order).to.eql(0);
    expect(splitSteps[1].order).to.eql(1);
    expect(splitSteps[2].order).to.eql(2);
    expect(splitSteps[3].order).to.eql(3);
  });
});
