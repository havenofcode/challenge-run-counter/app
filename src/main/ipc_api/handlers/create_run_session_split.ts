import {IpcMainInvokeEvent} from 'electron';

import {RunSessionSplit} from '../../model/run_session_split';

export const createRunSessionSplitHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<RunSessionSplit>
): Promise<RunSessionSplit> => {
  const {id} = await RunSessionSplit.query().insert(data);
  return await RunSessionSplit.query().findById(id);
}
