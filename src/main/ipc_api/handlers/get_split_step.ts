import {IpcMainInvokeEvent} from 'electron';

import {SplitStep} from '../../model/split_step';

export const getSplitStepHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string
): Promise<SplitStep> => {
  return await SplitStep.query().findById(id);
}
