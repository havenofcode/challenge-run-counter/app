import {IpcMainInvokeEvent} from 'electron';

import {
  RunScenario,
} from '../../model/run_scenario';

export const updateRunScenarioHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<RunScenario>,
): Promise<RunScenario> => {
  const {id} = data;

  if (!id) {
    throw new Error('params.id required');
  }

  await RunScenario.query().findById(id).patch(data);
  return await RunScenario.query().findById(id);
}
