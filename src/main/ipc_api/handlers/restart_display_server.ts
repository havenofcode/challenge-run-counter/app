import {DisplayServer} from '../../../ws';

export const restartDisplayServerHandler = async (): Promise<void> => {
  await DisplayServer.restart();
}
