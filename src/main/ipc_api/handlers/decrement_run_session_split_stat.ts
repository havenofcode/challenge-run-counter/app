import {Transaction} from 'objection';
import {IpcMainInvokeEvent} from 'electron';
import {SplitStep} from '../../model/split_step';
import {RunSessionSplit} from '../../model/run_session_split';
import {getCurrentSessionWithTransaction} from './get_current_session';

export const decrementRunSessionSplitStatWithTransaction = async (trx: Transaction, statName: string) => {
  const runSession = await getCurrentSessionWithTransaction(trx);

  const runSessionSplit = await RunSessionSplit.query()
    .transacting(trx)
    .findById(runSession.current_session_split_id);

  const patch: Partial<RunSessionSplit> = {}

  switch (statName) {
    case 'hits_boss': {
      // number not allowed to be negative
      patch.hits_boss = ((runSessionSplit.hits_boss || 1) - 1);
      break;
    }
    case 'hits_way': {
      // number not allowed to be negative
      patch.hits_way = ((runSessionSplit.hits_way || 1) - 1);
      break;
    }
    default: {
      throw new Error('stat does not exist');
    }
  }

  await RunSessionSplit.query()
    .transacting(trx)
    .patch(patch)
    .findById(runSessionSplit.id);
}

export const decrementRunSessionSplitStatHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  statName: string,
): Promise<void> => {
  return SplitStep.transaction((trx) => {
    return decrementRunSessionSplitStatWithTransaction(trx, statName);
  });
}
