import {expect} from 'chai';
import {getRestoreDbTasks} from '../../../test/helpers';
import {createRunSessionHandler} from './create_run_session';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';
const {restoreDb, destroyDb} = getRestoreDbTasks();

describe('IpcApi::createRunSessionHandler', () => {

  beforeEach(async () => {
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 1'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split collection 420',
      split_collection_id: 420,
    });
  });

  afterEach(async () => {
    await destroyDb();
  });

  it('exists', () => {
    expect(createRunSessionHandler).to.be.a('function');
  });

  it('returns split collection', async () => {
    const ipcMainInvokeEvent = {}

    const runSession = await createRunSessionHandler(ipcMainInvokeEvent, {
      run_scenario_id: 69,
    });

    expect(runSession.run_scenario_id).to.eql(69);
  });
});
