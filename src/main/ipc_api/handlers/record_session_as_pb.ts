import {Transaction} from 'objection';
import {getCurrentSessionWithTransaction} from './get_current_session';
import {accumulateSplitDurations} from '../../../common'

import {
  RunSession,
  RunSessionSplit,
  RunSessionSplitCollection,
  SplitCollection,
} from '../../model';

export const recordSessionAsPBWithTransaction = async (
  trx: Transaction,
) => {
  const runSession = await getCurrentSessionWithTransaction(trx);

  if (runSession.current_session_split_id === null) {
    throw new Error(`runSession id ${runSession.id} does not have current_session_split_id`);
  }

  const currentRunSessionSplit = await RunSessionSplit.query()
    .transacting(trx)
    .withGraphJoined('split_step')
    .findById(runSession.current_session_split_id);

  const runSessionSplits = await RunSessionSplit.query()
    .transacting(trx)
    .where({
      run_session_id: runSession.id,
    });

  const runSessionSplitsMap: {[key: number]: RunSessionSplit} = {}
  runSessionSplits.forEach((item) => runSessionSplitsMap[item.split_step_id] = item);

  const runSessionSplitCollection = await RunSessionSplitCollection.query()
    .transacting(trx)
    .withGraphFetched(
      [
        '[run_attempt_time_points.[run_session_split.[split_step]]',
        'split_collection.[split_steps]]',
      ].join(','),
      {minimize: true}
    )
    .modifyGraph('split_collection', builder => {
      builder.where('deleted', false);
    })
    .modifyGraph('split_collection.split_steps ', builder => {
      builder.where('deleted', false);
    })
    .modifyGraph('run_attempt_time_points', builder => {
      // Order children by age and only select id.
      builder.orderBy('id', 'asc');
    })
    .findOne({
      split_collection_id: currentRunSessionSplit.split_step.split_collection_id,
      run_session_id: runSession.id,
    });

  const splitCollection = runSessionSplitCollection.split_collection;
  const timePoints = runSessionSplitCollection.run_attempt_time_points;

  const duration = accumulateSplitDurations(timePoints);
  let totalBossHits = 0;
  let totalWayHits = 0;
  let totalHits = 0;

  for (let i = 0; i < splitCollection.split_steps.length; ++i) {
    const splitStep = splitCollection.split_steps[i];
    const runSessionSplit = runSessionSplitsMap[splitStep.id];

    if (!runSessionSplit) {
      throw new Error(`internal error, no runSessionSplit for splitStep ${splitStep.id}`);
    }

    const pbTime: number | undefined = duration.splits[splitStep.id];
    totalBossHits += runSessionSplit.hits_boss || 0;
    totalWayHits += runSessionSplit.hits_way || 0;
    const pbHits = (runSessionSplit.hits_way || 0) + (runSessionSplit.hits_boss || 0);
    totalHits += pbHits;

    await splitStep.$query().transacting(trx).patch({
      pb_hits: pbHits,
      pb_time: pbTime,
    });

    await runSessionSplit.$query().transacting(trx).patch({
      time: pbTime,
    });
  }

  await SplitCollection.query()
    .transacting(trx)
    .findById(currentRunSessionSplit.split_step.split_collection_id)
    .patch({
      pb_time: duration.total,
      pb_hits: totalHits,
    });

  await runSessionSplitCollection.$query()
    .transacting(trx)
    .patch({
      hits_way: totalBossHits,
      hits_boss: totalWayHits,
      time: duration.total,
    });
}

export const recordSessionAsPBHandler = (): Promise<void> => {
  return RunSession.transaction((trx) => {
    return recordSessionAsPBWithTransaction(trx);
  });
}
