
import {
  SplitCollection,
} from '../../model';

export async function getAllSplitCollectionsHandler(): Promise<SplitCollection[]> {
  return await SplitCollection.query()
    .where('deleted', 0)
    .orderBy('created_at', 'desc');
}
