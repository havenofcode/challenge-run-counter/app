import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {updateRunSessionSplitHandler} from './update_run_session_split';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';
import {RunSessionSplit} from '../../model/run_session_split';
import {RunSession} from '../../model/run_session';

describe('IpcApi::updateRunSessionSplitHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1337,
      name: 'split step 1',
      split_collection_id: 420,
    });

    await RunSession.query().insert({
      id: 1,
      run_scenario_id: 69,
    });

    await RunSessionSplit.query().insert({
      id: 1,
      run_session_id: 1,
      split_step_id: 1337,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(updateRunSessionSplitHandler).to.be.a('function');
  });

  it('updates split step', async () => {
    const res = await updateRunSessionSplitHandler({}, {
      id: 1,
      run_session_id: 1,
      split_step_id: 1337,
      hits_way: 1
    });

    const [updatedRunSessionSplit] = await RunSessionSplit.query();
    expect(res.hits_way).to.eql(1);
    expect(res).to.eql(updatedRunSessionSplit);
  });
});
