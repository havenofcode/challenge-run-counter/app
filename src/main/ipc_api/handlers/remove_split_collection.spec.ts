import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';

import {
  RunSession,
  RunSessionSplitCollection,
  SplitCollection,
  SplitStep,
} from '../../model';

import {removeSplitCollectionHandler} from './';

describe('IpcApi::removeSplitCollectionHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(removeSplitCollectionHandler).to.be.a('function');
  });

  it('removes split collection on fresh install', async () => {
    await fixtures.freshInstallFixture();
    await removeSplitCollectionHandler({}, '1');
    const runSession = await RunSession.query().findById(1);

    const [s1, s2] = await SplitCollection.query()
      .select('name', 'id', 'deleted')
      .orderBy('id', 'asc');

    const [rssp1, rssp2] = await RunSessionSplitCollection.query()
      .select('id', 'split_collection_id', 'current_split_step_id', 'run_session_id')
      .orderBy('id', 'asc');

    const newSplitSteps = await SplitStep.query()
      .withGraphJoined('run_session_splits')
      .where({split_collection_id: 2});

    // run_session_split_collections
    expect(rssp1).to.eql({id: 1, split_collection_id: 1, current_split_step_id: 1, run_session_id: 1});
    expect(rssp2).to.eql({id: 2, split_collection_id: 2, current_split_step_id: 5, run_session_id: 1});

    // split_collections
    expect(s1).to.eql({id: 1, deleted: 1, name: 'split collection 1'});
    expect(s2).to.eql({id: 2, deleted: 0, name: ''});

    // split_steps
    expect(newSplitSteps).to.have.lengthOf(1);
    expect(newSplitSteps[0].run_session_splits).to.have.lengthOf(1);

    // run_sessions
    expect(runSession.current_session_split_id).to.eql(5);
  });

  it('removes split containing deleted splits', async () => {
    await fixtures.freshWithDeletedSplitsFixture();
    await removeSplitCollectionHandler({}, '2');
    const runSession = await RunSession.query().findById(1);

    const splitCollections = await SplitCollection.query()
      .select('name', 'id', 'deleted')
      .orderBy('id', 'asc');

    expect(splitCollections).to.have.lengthOf(2);

    expect(splitCollections).to.eql([
      {name: 'split collection 1', id: 1, deleted: 0},
      {name: 'split collection 2', id: 2, deleted: 1},
    ]);

    const runSessionSplitCollections = await RunSessionSplitCollection.query()
      .select('id', 'split_collection_id', 'current_split_step_id', 'run_session_id')
      .orderBy('id', 'asc');

    expect(runSessionSplitCollections).to.have.lengthOf(2);

    expect(runSessionSplitCollections).to.eql([
      {id: 1, split_collection_id: 1, current_split_step_id: 1, run_session_id: 1},
      {id: 2, split_collection_id: 2, current_split_step_id: 5, run_session_id: 1},
    ]);

    // // run_sessions
    expect(runSession.current_session_split_id).to.eql(1);
  });

  it('removes when run_sessions > 1', async () => {
    await fixtures.multipleCollectionsFixture();
    await removeSplitCollectionHandler({}, '2');
    const runSession = await RunSession.query().findById(2);

    const splitCollections = await SplitCollection.query()
      .select('name', 'id', 'deleted')
      .orderBy('id', 'asc');

    expect(splitCollections).to.have.lengthOf(2);

    expect(splitCollections).to.eql([
      {name: 'split collection 1', id: 1, deleted: 0},
      {name: 'split collection 2', id: 2, deleted: 1},
    ]);

    const runSessionSplitCollections = await RunSessionSplitCollection.query()
      .select('id', 'split_collection_id', 'current_split_step_id', 'run_session_id')
      .orderBy('id', 'asc');

    expect(runSessionSplitCollections).to.have.lengthOf(4);

    expect(runSessionSplitCollections).to.eql([
      {id: 1, split_collection_id: 1, current_split_step_id: 1, run_session_id: 1},
      {id: 2, split_collection_id: 2, current_split_step_id: 5, run_session_id: 1},
      {id: 3, split_collection_id: 1, current_split_step_id: 1, run_session_id: 2},
      {id: 4, split_collection_id: 2, current_split_step_id: 5, run_session_id: 2},
    ]);

    // run_sessions
    expect(runSession.current_session_split_id).to.eql(9);
  });
});
