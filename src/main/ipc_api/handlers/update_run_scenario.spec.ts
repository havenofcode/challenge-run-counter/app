import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {updateRunScenarioHandler} from './update_run_scenario';
import {RunScenario} from '../../model/run_scenario';

describe('IpcApi::updateRunScenarioHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 1'
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(updateRunScenarioHandler).to.be.a('function');
  });

  it('returns split collection', async () => {
    const res = await updateRunScenarioHandler({}, {id: 69, name: 'changed run scenario'});
    const [updatedRunScenario] = await RunScenario.query();
    expect(res.name).to.eql('changed run scenario');
    expect(res).to.eql(updatedRunScenario);
  });
});
