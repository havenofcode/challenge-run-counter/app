import {Transaction} from 'objection';
import {IpcMainInvokeEvent} from 'electron';

import {
  IViewerSettingsJson,
  ViewerSetting,
} from '../../model';

const getCurrentViewerSettings = async (trx: Transaction, name: string) => {
  const viewerSetting = await ViewerSetting.query()
    .transacting(trx)
    .findOne({
      name,
    });

  if (!viewerSetting) {
    const res = await ViewerSetting.query().transacting(trx).insert({name});
    return ViewerSetting.query().transacting(trx).findById(res.id);
  }

  return viewerSetting;
}

export const updateViewerSettingsHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  name: string,
  settings: IViewerSettingsJson,
): Promise<void> => {
  return ViewerSetting.transaction(async (trx) => {
    const viewerSetting = await getCurrentViewerSettings(trx, name);
    const parsedSettings: IViewerSettingsJson = JSON.parse(viewerSetting.settings);

    await viewerSetting.$query().transacting(trx).patch({
      settings: JSON.stringify({
        ...parsedSettings,
        ...settings,
      }),
    });
  });
}
