import {
  RunScenario,
} from '../../model/run_scenario';

export async function getAllRunScenariosHandler(): Promise<RunScenario[]> {
  return await RunScenario.query()
    .where('deleted', '0')
    .orderBy('created_at', 'desc');
}
