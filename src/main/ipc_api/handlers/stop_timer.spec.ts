import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {stopTimerHandler} from './';

import {
  RunSessionSplit,
  RunSessionSplitCollection,
  RunSession,
  RunScenario,
  SplitCollection,
  SplitStep,
  AppSetting,
  RunAttemptTimePoint,
} from '../../model';

describe('IpcApi::stop_timer', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 1,
      name: 'run scenario 1'
    });

    await RunSession.query().insert({
      id: 1,
      run_scenario_id: 1,
    })

    await SplitCollection.query().insert({
      id: 1,
      name: 'split collection 1',
      run_scenario_id: 1,
    });

    await SplitStep.query().insert({
      id: 1,
      name: 'first ever split step',
      order: 0,
      split_collection_id: 1,
    });

    await RunSessionSplit.query().insert({
      id: 1,
      run_session_id: 1,
      split_step_id: 1,
    });

    await RunSessionSplitCollection.query().insert({
      id: 1,
      run_session_id: 1,
      current_split_step_id: 1,
      split_collection_id: 1,
    });

    await RunSession.query().findById(1).patch({
      id: 1,
      current_session_split_id: 1,
    });

    await AppSetting.query().insert({
      id: 1,
      current_run_scenario_id: 1,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(stopTimerHandler).to.be.a('function');
  });

  it('stops the timer', async () => {
    const startTime = new Date().getTime() - 500;
    const stopTime = new Date().getTime() - 100;

    await RunAttemptTimePoint.query().insert({
      event_type: 'attempt_started',
      time: startTime,
      run_session_split_collection_id: 1,
      run_session_split_id: 1,
    });

    await RunAttemptTimePoint.query().insert({
      event_type: 'split_started',
      time: startTime,
      run_session_split_collection_id: 1,
      run_session_split_id: 1,
    });

    await stopTimerHandler({}, stopTime);
    const runTimes = await RunAttemptTimePoint.query()
      .orderBy('time', 'desc')
      .where('event_type', 'attempt_stopped')
      .orWhere('event_type', 'attempt_started');
    expect(runTimes.length).to.eql(2);
    expect(runTimes[0].time).to.eql(stopTime);
    expect(runTimes[0].event_type).to.eql('attempt_stopped');
  });

  it('throws if stopped twice', async () => {
    const startTime = new Date().getTime() - 500;
    const stopTime = new Date().getTime() - 100;
    const nextStopTime = new Date().getTime();

    await RunAttemptTimePoint.query().insert({
      event_type: 'attempt_started',
      time: startTime,
      run_session_split_collection_id: 1,
      run_session_split_id: 1,
    });

    await RunAttemptTimePoint.query().insert({
      event_type: 'split_started',
      time: startTime,
      run_session_split_collection_id: 1,
      run_session_split_id: 1,
    });

    await stopTimerHandler({}, stopTime);
    let failed = false;

    try {
      await stopTimerHandler({}, nextStopTime);
    } catch (err) {
      failed = true;
    }

    expect(failed).to.be.true;
  });

  it('throws if never started', async () => {
    let failed = false;

    try {
      await stopTimerHandler({}, new Date().getTime());
    } catch (err) {
      failed = true;
    }

    expect(failed).to.be.true;
  });
});
