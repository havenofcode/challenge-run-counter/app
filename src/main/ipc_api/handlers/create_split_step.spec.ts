import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {createSplitStepHandler} from './create_split_step';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';
import {RunSession} from '../../model/run_session';
import {RunSessionSplitCollection} from '../../model/run_session_split_collection';
import {RunSessionSplit} from '../../model/run_session_split';

describe('IpcApi::createSplitStepHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await RunSession.query().insert({
      id: 69,
      run_scenario_id: 69,
    })

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1,
      name: 'first ever split step',
      order: 0,
      split_collection_id: 420,
    });

    await RunSessionSplit.query().insert({
      id: 1,
      run_session_id: 69,
      split_step_id: 1,
    });

    await RunSessionSplitCollection.query().insert({
      id: 1,
      run_session_id: 69,
      current_split_step_id: 1,
      split_collection_id: 420,
    });

    await RunSession.query().findById(69).patch({
      id: 69,
      current_session_split_id: 1,
    })
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(createSplitStepHandler).to.be.a('function');
  });

  it('creates split step', async () => {
    const res = await createSplitStepHandler({}, {
      name: 'split step 1',
      split_collection_id: 420,
    });

    expect(res.name).to.eql('split step 1');
    expect(res.split_collection_id).to.eql(420);
    expect(await SplitStep.query().findById(res.id)).to.eql(res);
  });

  it('increments order', async () => {
    await createSplitStepHandler({}, {
      name: 'split step 1',
      split_collection_id: 420,
    });

    await createSplitStepHandler({}, {
      name: 'split step 2',
      split_collection_id: 420,
    });

    await createSplitStepHandler({}, {
      name: 'split step 3',
      split_collection_id: 420,
    });

    const res = await SplitStep.query().orderBy('order', 'asc');
    expect(res.map(({order}) => order)).to.eql([0, 1, 2, 3]);
  });
});
