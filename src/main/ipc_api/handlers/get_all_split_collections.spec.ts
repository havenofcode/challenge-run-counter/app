import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask, fixtures} from '../../../test';
import {getAllSplitCollectionsHandler} from './';

describe('IpcApi::getAllSplitCollectionsHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(getAllSplitCollectionsHandler).to.be.a('function');
  });

  it('gets all split collections', async () => {
    await fixtures.multipleCollectionsFixture();
    const res = await getAllSplitCollectionsHandler();
    expect(res).to.have.lengthOf(2);
  });
});
