import path from 'path';
import {BrowserWindow} from 'electron';
import {expect} from 'chai';
import {createSandbox} from 'sinon';
import {getRestoreDbTasks, RestoreTask} from '../../../test';
import {requestPreferencesWindowHandler} from './';
import {PreferencesWindow} from '../..';

describe('IpcApi::requestPreferencesWindowHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;
  const sandbox = createSandbox();

  class FakeBrowserWindow {
    static setMenuBarVisibility = sandbox.spy();
    static loadFile = sandbox.spy();
    static on = sandbox.spy();
    setMenuBarVisibility = FakeBrowserWindow.setMenuBarVisibility;
    loadFile = FakeBrowserWindow.loadFile;
    on = FakeBrowserWindow.on;
  }

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();
    PreferencesWindow.BrowserWindowClass = FakeBrowserWindow as unknown as typeof BrowserWindow;
  });

  afterEach(async () => {
    await destroyDbTask();
    PreferencesWindow.BrowserWindowClass = BrowserWindow;
    sandbox.restore();
  });

  it('exists', async () => {
    expect(requestPreferencesWindowHandler).to.be.a('function');
  });

  it('loads the preferences page', async () => {
    await requestPreferencesWindowHandler();
    const loadedPage = FakeBrowserWindow.loadFile.getCall(0).args[0];
    const filePath = path.resolve(__dirname, '..', '..', '..', '..', 'html', 'preferences.html');
    expect(loadedPage).to.eql(filePath);
  });
});
