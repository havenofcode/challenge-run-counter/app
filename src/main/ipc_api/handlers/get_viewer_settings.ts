import {IpcMainInvokeEvent} from 'electron';

import {
  ViewerSetting,
} from '../../model';

export const getViewerSettingsHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  name: string,
): Promise<ViewerSetting | null> => {
  return ViewerSetting.transaction(async (trx) => {
    const viewerSetting = await ViewerSetting.query()
      .transacting(trx)
      .findOne({
        name,
      });

    return viewerSetting || null;
  });
}
