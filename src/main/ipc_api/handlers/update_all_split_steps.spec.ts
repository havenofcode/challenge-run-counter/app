import {expect} from 'chai';
import {getRestoreDbTasks, RestoreTask} from '../../../test/helpers';
import {updateAllSplitStepsHandler} from './update_all_split_steps';
import {SplitCollection} from '../../model/split_collection';
import {RunScenario} from '../../model/run_scenario';
import {SplitStep} from '../../model/split_step';

describe('IpcApi::updateALlSplitStepsHandler', () => {
  let destroyDbTask: RestoreTask = async () => null;

  beforeEach(async () => {
    const {restoreDb, destroyDb} = getRestoreDbTasks();
    destroyDbTask = destroyDb;
    await restoreDb();

    await RunScenario.query().insert({
      id: 69,
      name: 'run scenario 69'
    });

    await SplitCollection.query().insert({
      id: 420,
      name: 'split collection 420',
      run_scenario_id: 69,
    });

    await SplitStep.query().insert({
      id: 1,
      name: 'split step 1',
      split_collection_id: 420,
    });

    await SplitStep.query().insert({
      id: 2,
      name: 'split step 2',
      split_collection_id: 420,
    });

    await SplitStep.query().insert({
      id: 3,
      name: 'split step 2',
      split_collection_id: 420,
    });

    await SplitStep.query().insert({
      id: 4,
      name: 'split step 2',
      split_collection_id: 420,
    });
  });

  afterEach(async () => {
    await destroyDbTask();
  });

  it('exists', () => {
    expect(updateAllSplitStepsHandler).to.be.a('function');
  });

  it('creates split step', async () => {
    await updateAllSplitStepsHandler({}, [
      {
        id: 2,
        name: 'changed split step 2',
      },
      {
        id: 3,
        name: 'changed split step 3',
      }
    ]);

    const newSplitStep2 = await SplitStep.query().findById(2);
    const newSplitStep3 = await SplitStep.query().findById(3);
    expect(newSplitStep2.name).to.eql('changed split step 2');
    expect(newSplitStep3.name).to.eql('changed split step 3');
  });
});
