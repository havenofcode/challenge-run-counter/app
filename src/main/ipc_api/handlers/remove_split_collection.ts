import {IpcMainInvokeEvent} from 'electron';
import {SplitCollection} from '../../model/split_collection';
import {RunSession} from '../../model/run_session';
import {RunSessionSplitCollection} from '../../model/run_session_split_collection';
import {RunSessionSplit} from '../../model/run_session_split';
import {SplitStep} from '../../model/split_step';

export const removeSplitCollectionHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  id: string,
): Promise<number> => {
  const res = await SplitCollection.transaction(async (trx) => {
    const splitCollection = await SplitCollection.query()
      .transacting(trx)
      .findById(id);

    const [runSession] = await RunSession.query()
      .transacting(trx)
      .where('run_scenario_id', splitCollection.run_scenario_id)
      .orderBy('created_at', 'desc');

    const deletedId = splitCollection.id;

    await SplitCollection.query()
      .transacting(trx)
      .findById(id)
      .patch({deleted: 1});

    const allSplitCollections = await SplitCollection.query()
      .transacting(trx)
      .where({
        run_scenario_id: splitCollection.run_scenario_id,
        deleted: 0,
      }).orderBy('created_at', 'desc');

    // if the last split collection was deleted,
    // we will create another empty split collection
    if (allSplitCollections.length === 0) {
      const newSplitCollection = await SplitCollection.query()
        .transacting(trx)
        .insert({
          run_scenario_id: splitCollection.run_scenario_id,
          name: '',
        });

      // create empty split for split collection
      const splitStep = await SplitStep.query()
        .transacting(trx)
        .insert({
          split_collection_id: newSplitCollection.id,
          name: '',
          order: 0,
        });

      // create session data for the new split collection
      await RunSessionSplitCollection.query()
        .transacting(trx)
        .insert({
          run_session_id: runSession.id,
          current_split_step_id: splitStep.id,
          split_collection_id: newSplitCollection.id,
        });

      // set the current session split as last active split
      const runSessionSplit = await RunSessionSplit.query()
        .transacting(trx)
        .insert({
          run_session_id: runSession.id,
          split_step_id: splitStep.id,
        });

      await RunSession.query()
        .transacting(trx)
        .findById(runSession.id)
        .patch({
          current_session_split_id: runSessionSplit.id,
        });
    } else {

      const [runSessionSplitCollection] = await RunSessionSplitCollection.query()
        .transacting(trx)
        .where({
          run_session_id: runSession.id,
          split_collection_id: allSplitCollections[0].id
        })
        .orderBy('created_at', 'desc')
        .limit(1);

      const [runSessionSplit] = await RunSessionSplit.query()
        .transacting(trx)
        .where({
          run_session_id: runSession.id,
          split_step_id: runSessionSplitCollection.current_split_step_id,
        })
        .orderBy('created_at', 'desc');

      await RunSession.query()
        .transacting(trx)
        .findById(runSession.id)
        .patch({
          current_session_split_id: runSessionSplit.id
        });
    }

    return deletedId;
  });

  return res;
}
