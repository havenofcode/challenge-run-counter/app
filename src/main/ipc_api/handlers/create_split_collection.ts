import {IpcMainInvokeEvent} from 'electron';

import {
  RunSession,
  RunSessionSplit,
  RunSessionSplitCollection,
  SplitCollection,
  SplitStep,
} from '../../model';

export const createSplitCollectionHandler = async (
  event: Partial<IpcMainInvokeEvent>,
  data: Partial<SplitCollection>
): Promise<SplitCollection> => {
  const res = await SplitCollection.transaction(async (trx) => {
    const splitCollection = await SplitCollection.query()
      .transacting(trx)
      .insert(data);

    // create empty split for split collection
    const splitStep = await SplitStep.query().transacting(trx).insert({
      split_collection_id: splitCollection.id,
      name: '',
      order: 0,
    });

    // get  the current run session
    const [runSession] = await RunSession.query()
      .transacting(trx)
      .where('run_scenario_id', splitCollection.run_scenario_id)
      .orderBy('created_at', 'desc')
      .limit(1);

    if (!runSession) {
      throw new Error('Run scenario must have an active session');
    }

    // create session data for the new split collection
    await RunSessionSplitCollection.query()
      .transacting(trx)
      .insert({
        run_session_id: runSession.id,
        current_split_step_id: splitStep.id,
        split_collection_id: splitCollection.id,
      });

    // set the current session split as last active split
    const runSessionSplit = await RunSessionSplit.query()
      .transacting(trx)
      .insert({
        run_session_id: runSession.id,
        split_step_id: splitStep.id,
      });

    await RunSession.query()
      .transacting(trx)
      .findById(runSession.id)
      .patch({
        current_session_split_id: runSessionSplit.id,
      });

    return splitCollection;
  });

  return res;
}
