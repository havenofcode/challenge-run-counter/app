import {expect} from 'chai';
import {initializeIpcApi} from './';

describe('IpcApi', () => {
  it('exists', () => {
    expect(initializeIpcApi).to.be.a('function');
  });
});
