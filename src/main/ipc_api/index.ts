import {ipcMain} from 'electron';

import {
  createRunScenarioHandler,
  createRunSessionHandler,
  createSplitCollectionHandler,
  createSplitStepHandler,
  getActiveViewerSettingsHandler,
  getAllRunScenariosHandler,
  getAllRunSessionsHandler,
  getAllSplitCollectionsHandler,
  getAppSettingHandler,
  getObsPageStylesHandler,
  getRunScenarioHandler,
  getRunSessionHandler,
  getRunSessionSplitCollectionHandler,
  getSplitCollectionHandler,
  getSplitPageDataHandler,
  getSplitStepHandler,
  getViewerSettingsHandler,
  notifyPageSettingsChangeHandler,
  notifySetActiveSplitHandler,
  notifySetCountByNameHandler,
  notifySplitsChangeHandler,
  openChooseDirectoryHandler,
  recordSessionAsPBHandler,
  removeRunScenarioHandler,
  removeSplitCollectionHandler,
  removeSplitStepHandler,
  requestPreferencesWindowHandler,
  restartDisplayServerHandler,
  restartRunHandler,
  setActiveSplitHandler,
  startTimerHandler,
  stopTimerHandler,
  swapSplitOrderHandler,
  trySetGlobalShortcutHandler,
  updateAllSplitStepsHandler,
  updateAppSettingHandler,
  updateObsPageStylesHandler,
  updateRunScenarioHandler,
  updateRunSessionHandler,
  updateRunSessionSplitHandler,
  updateSplitCollectionHandler,
  updateSplitStepHandler,
  updateViewerSettingsHandler,
} from './handlers';

export const initializeIpcApi = async () => {


  // non-transactional data related
  ipcMain.handle('create_run_scenario', createRunScenarioHandler);
  ipcMain.handle('create_run_session', createRunSessionHandler);
  ipcMain.handle('create_split_collection', createSplitCollectionHandler);
  ipcMain.handle('create_split_step', createSplitStepHandler);

  ipcMain.handle('get_all_run_scenarios', getAllRunScenariosHandler);
  ipcMain.handle('get_all_run_sessions', getAllRunSessionsHandler);
  ipcMain.handle('get_all_split_collections', getAllSplitCollectionsHandler);
  ipcMain.handle('get_app_setting', getAppSettingHandler);
  ipcMain.handle('get_obs_page_styles', getObsPageStylesHandler);
  ipcMain.handle('get_run_scenario', getRunScenarioHandler);
  ipcMain.handle('get_run_session_split_collection', getRunSessionSplitCollectionHandler);
  ipcMain.handle('get_run_session', getRunSessionHandler);
  ipcMain.handle('get_split_collection', getSplitCollectionHandler);
  ipcMain.handle('get_split_page_data', getSplitPageDataHandler);
  ipcMain.handle('get_split_step', getSplitStepHandler);
  ipcMain.handle('get_viewer_settings', getViewerSettingsHandler);

  ipcMain.handle('notify_page_settings_change', notifyPageSettingsChangeHandler);
  ipcMain.handle('notify_set_active_split', notifySetActiveSplitHandler);
  ipcMain.handle('notify_splits_change', notifySplitsChangeHandler);
  ipcMain.handle('notify_set_count_by_name', notifySetCountByNameHandler);
  ipcMain.handle('open_choose_directory', openChooseDirectoryHandler);
  ipcMain.handle('record_session_as_pb', recordSessionAsPBHandler);

  ipcMain.handle('remove_run_scenario', removeRunScenarioHandler);
  ipcMain.handle('remove_split_collection', removeSplitCollectionHandler);
  ipcMain.handle('remove_split_step', removeSplitStepHandler);
  ipcMain.handle('request_preferences_window', requestPreferencesWindowHandler);
  ipcMain.handle('restart_display_server', restartDisplayServerHandler);
  ipcMain.handle('restart_run', restartRunHandler);
  ipcMain.handle('set_active_split', setActiveSplitHandler);
  ipcMain.handle('start_timer', startTimerHandler);
  ipcMain.handle('stop_timer', stopTimerHandler);
  ipcMain.handle('swap_split_order', swapSplitOrderHandler);
  ipcMain.handle('try_set_global_shortcut', trySetGlobalShortcutHandler);

  ipcMain.handle('update_all_split_steps', updateAllSplitStepsHandler);
  ipcMain.handle('update_app_setting', updateAppSettingHandler);
  ipcMain.handle('update_obs_page_styles', updateObsPageStylesHandler);
  ipcMain.handle('update_run_scenario', updateRunScenarioHandler);
  ipcMain.handle('update_run_session_split', updateRunSessionSplitHandler);
  ipcMain.handle('update_run_session', updateRunSessionHandler);
  ipcMain.handle('update_split_collection', updateSplitCollectionHandler);
  ipcMain.handle('update_split_step', updateSplitStepHandler);
  ipcMain.handle('update_viewer_settings', updateViewerSettingsHandler);
  ipcMain.handle('get_active_viewer_settings', getActiveViewerSettingsHandler);

  return Promise.resolve();
}
