import {
  AppSetting,
  SplitStep,
} from '../model';

import {
  setNextActiveSplitWithTransaction,
  setPrevActiveSplitWithTransaction,
  getActiveSplitWithTransaction,
  incrementRunSessionSplitStatWithTransaction,
  decrementRunSessionSplitStatWithTransaction,
  restartRunWithTransaction,
  startTimerWithTransaction,
  stopTimerWithTransaction,
  recordSessionAsPBWithTransaction,
} from './handlers';

import {MainWindow} from '../windows/main';
import {DisplayServer} from '../../ws';

export class Hotkeys {
  private static instance: Hotkeys | null = null;

  static init() {
    if (!Hotkeys.instance) {
      Hotkeys.instance = new Hotkeys();
    }
  }

  static get() {
    if (!Hotkeys.instance) {
      this.init();
    }

    return Hotkeys.instance;
  }

  constructor() {
    this.init();
  }

  async init() {
    await this.initHotkeyNextSplit();
    await this.initHotkeyPrevSplit();
    await this.initHotkeyIncrementBossHit();
    await this.initHotkeyIncrementWayHit();
    await this.initHotkeyDecrementBossHit();
    await this.initHotkeyDecrementWayHit();
    await this.initHotkeyRestartRun();
    await this.initHotkeyStartTimer();
    await this.initHotkeyStopTimer();
    await this.initHotkeyRecordPb();
  }

  async initHotkeyNextSplit() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_next_split');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      try {
        const time = new Date().getTime();
        MainWindow.emitShortcutInvoked('hotkey_next_split', {time});
        await DisplayServer.emitNextSplit(time);

        const res = await AppSetting.transaction(async (trx) => {
          const splitStep = await getActiveSplitWithTransaction(trx);
          return await setNextActiveSplitWithTransaction(trx, `${splitStep.id}`, time);
        });

        // if returned value is null, then the timer stopped possibly
        if (res === null) {
          await DisplayServer.sendSplits().catch(console.error);
          MainWindow.emitShortcutInvoked('hotkey_stop_timer', {time});
        }
      } catch (err) {
        const message = err ? (err.message || 'unknown') : 'unknown';
        MainWindow.emitShortcutError({name: 'hotkey_next_split', message});
      }
    });
  }

  async initHotkeyPrevSplit() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_prev_split');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      try {
        const time = new Date().getTime();
        await DisplayServer.emitPrevSplit(time);
        MainWindow.emitShortcutInvoked('hotkey_prev_split', {time});

        await AppSetting.transaction(async (trx) => {
          const splitStep = await getActiveSplitWithTransaction(trx);
          await setPrevActiveSplitWithTransaction(trx, `${splitStep.id}`, time);
        });
      } catch (err) {
        const message = err ? (err.message || 'unknown') : 'unknown';
        MainWindow.emitShortcutError({name: 'hotkey_prev_split', message});
      }
    });
  }

  async initHotkeyIncrementBossHit() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_increment_boss_hit');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      try {
        const time = new Date().getTime();
        await DisplayServer.emitIncrementName('hits_boss');
        MainWindow.emitShortcutInvoked('hotkey_increment_boss_hit', {time});

        await SplitStep.transaction(async (trx) => {
          await incrementRunSessionSplitStatWithTransaction(trx, 'hits_boss');
        });
      } catch (err) {
        const message = err ? (err.message || 'unknown') : 'unknown';
        MainWindow.emitShortcutError({name: 'hotkey_increment_boss_hit', message});
      }
    });
  }

  async initHotkeyIncrementWayHit() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_increment_way_hit');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      try {
        const time = new Date().getTime();
        await DisplayServer.emitIncrementName('hits_way');
        MainWindow.emitShortcutInvoked('hotkey_increment_way_hit', {time});

        await SplitStep.transaction(async (trx) => {
          await incrementRunSessionSplitStatWithTransaction(trx, 'hits_way');
        });
      } catch (err) {
        const message = err ? (err.message || 'unknown') : 'unknown';
        MainWindow.emitShortcutError({name: 'hotkey_increment_way_hit', message});
      }
    });
  }

  async initHotkeyDecrementBossHit() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_decrement_boss_hit');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      try {
        const time = new Date().getTime();
        await DisplayServer.emitDecrementName('hits_boss');
        MainWindow.emitShortcutInvoked('hotkey_decrement_boss_hit', {time});

        await SplitStep.transaction(async (trx) => {
          await decrementRunSessionSplitStatWithTransaction(trx, 'hits_boss');
        });
      } catch (err) {
        const message = err ? (err.message || 'unknown') : 'unknown';
        MainWindow.emitShortcutError({name: 'hotkey_decrement_boss_hit', message});
      }
    });
  }

  async initHotkeyDecrementWayHit() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_decrement_way_hit');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      try {
        const time = new Date().getTime();
        await DisplayServer.emitDecrementName('hits_way');
        MainWindow.emitShortcutInvoked('hotkey_decrement_way_hit', {time});

        await SplitStep.transaction(async (trx) => {
          await decrementRunSessionSplitStatWithTransaction(trx, 'hits_way');
        });
      } catch (err) {
        const message = err ? (err.message || 'unknown') : 'unknown';
        MainWindow.emitShortcutError({name: 'hotkey_decrement_way_hit', message});
      }
    });
  }

  async initHotkeyRestartRun() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_restart_run');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      const time = new Date().getTime();
      await AppSetting.transaction(async (trx) => {
        await restartRunWithTransaction(trx);
      });

      await DisplayServer.sendSplits().catch(console.error);
      MainWindow.emitShortcutInvoked('hotkey_restart_run', {time});
    });
  }

  async initHotkeyStartTimer() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_start_timer');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      const time = new Date().getTime();

      await AppSetting.transaction(async (trx) => {
        await startTimerWithTransaction(trx, time);
      });

      await DisplayServer.sendSplits().catch(console.error);
      MainWindow.emitShortcutInvoked('hotkey_start_timer', {time});
    });
  }

  async initHotkeyStopTimer() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_stop_timer');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, async () => {
      const time = new Date().getTime();

      await AppSetting.transaction(async (trx) => {
        await stopTimerWithTransaction(trx, time);
      });

      await DisplayServer.sendSplits().catch(console.error);
      MainWindow.emitShortcutInvoked('hotkey_stop_timer', {time});
    });
  }

  async initHotkeyRecordPb() {
    const appSetting = await AppSetting.query().findById(1);
    const accelerator = appSetting.getHotkey('hotkey_record_pb');

    if (accelerator === '') {
      return;
    }

    if (AppSetting.globalShortcut.isRegistered(accelerator)) {
      return;
    }

    AppSetting.globalShortcut.register(accelerator, () => {
      const time = new Date().getTime();
      return AppSetting.transaction(async (trx) => {
        await recordSessionAsPBWithTransaction(trx);
        MainWindow.emitShortcutInvoked('hotkey_record_pb', {time});
        DisplayServer.sendSplits().catch(console.error);
      });
    });
  }
}
