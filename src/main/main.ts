import path from 'path';

import {
  MenuItemConstructorOptions,
  app,
  BrowserWindow,
  Menu,
  MenuItem,
  dialog,
  ipcMain,
} from 'electron';

import {initializeIpcApi} from './ipc_api';
import {Hotkeys} from './ipc_api/hotkeys';
import {initializeModels} from './model';
import {PreferencesWindow} from './windows/preferences';
import {MainWindow} from './windows/main';
import {startWebSocketServer, memoizeGetPackageJson} from '../ws';

const template: Array<(MenuItemConstructorOptions) | (MenuItem)> = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Quit',
        role: 'quit',
      }
    ]
  },
  {
    label: 'Edit',
    submenu: [
      {
        label: 'Undo',
        accelerator: 'CmdOrCtrl+Z',
        role: 'undo'
      },
      {
        label: 'Redo',
        accelerator: 'Shift+CmdOrCtrl+Z',
        role: 'redo'
      },
      {
        type: 'separator'
      },
      {
        label: 'Cut',
        accelerator: 'CmdOrCtrl+X',
        role: 'cut'
      },
      {
        label: 'Copy',
        accelerator: 'CmdOrCtrl+C',
        role: 'copy'
      },
      {
        label: 'Paste',
        accelerator: 'CmdOrCtrl+V',
        role: 'paste'
      },
      {
        label: 'Select All',
        accelerator: 'CmdOrCtrl+A',
        role: 'selectAll'
      },
      {
        label: 'Preferences',
        click: () => {
          PreferencesWindow.open();
        }
      }
    ]
  },
  {
    label: 'Window',
    role: 'window',
    submenu: [
      {
        label: 'Minimize',
        accelerator: 'CmdOrCtrl+M',
        role: 'minimize'
      },
      {
        label: 'Close',
        accelerator: 'CmdOrCtrl+W',
        role: 'close'
      },
      {
        type: 'separator'
      },
    ]
  },
  {
    label: 'Help',
    role: 'help',
    submenu: [
      {
        label: 'About',
        click: async () => {
          // todo: need to find a more elegent way to do this. right now i am
          // assuming the latest version is 1 minor version more than what is
          // actually in the package.json. reason is because ci/cd currently
          // runs semantic release after electron packages are built.
          const {version} = await memoizeGetPackageJson();
          const versionParts = version.split('.');
          const last = versionParts.pop();
          versionParts.push((parseInt(last, 10) + 1).toString());

          await dialog.showMessageBox({
            message: [
              `challenge-run-counter - ${versionParts.join('.')}`,
              '',
              'https://gitlab.com/havenofcode/challenge-run-counter/app',
              'https://teamhitless.com',
              'https://speedrun.com',
            ].join('\n')
          });
        }
      },
      {
        label: 'Developer Tools',
        accelerator: 'CmdOrCtrl+Shift+0',
        click: (menuItem: MenuItem, browserWindow: BrowserWindow | undefined) => {
          if (browserWindow) {
            browserWindow.webContents.openDevTools();
          }
        }
      }
    ]
  }
]

const logPaths = () => {
  console.info('dected paths');
  console.info(`  app path: ${app.getAppPath()}`);
  console.info(`  home dir: ${app.getPath('home')}`);
  console.info(`  appData dir: ${app.getPath('appData')}`);
  console.info(`  userData dir: ${app.getPath('userData')}`);
  console.info(`  cache dir: ${app.getPath('cache')}`);
  console.info(`  temp dir: ${app.getPath('temp')}`);
  console.info(`  exe dir: ${app.getPath('exe')}`);
  console.info(`  module dir: ${app.getPath('module')}`);
  console.info(`  desktop dir: ${app.getPath('desktop')}`);
  console.info(`  documents dir: ${app.getPath('documents')}`);
  console.info(`  downloads dir: ${app.getPath('downloads')}`);
  console.info(`  music dir: ${app.getPath('music')}`);
  console.info(`  pictures dir: ${app.getPath('pictures')}`);
  console.info(`  videos dir: ${app.getPath('videos')}`);
  console.info(`  logs dir: ${app.getPath('logs')}`);
  console.info(`  crashDumps dir: ${app.getPath('crashDumps')}`);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  try {
    // initialize orm
    await initializeModels({
      userDataDir: path.join(app.getPath('userData'), 'challenge_run_data'),
      defaultDataDir: path.join(app.getAppPath(), 'data'),
      migrate: true,
      migrationDirectory: path.join(app.getAppPath(), 'migrations'),
    });

    // debug info
    logPaths();

    // start websocket server
    await startWebSocketServer();

    // initialize ipc api
    await initializeIpcApi();
    Hotkeys.init();
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
    MainWindow.open();
  } catch (dialogError) {
    await dialog.showErrorBox(
      'Error in Main process',
      [
        `Error: ${dialogError.message}.`,
        'Application might already be open and cannot be opened twice.',
        'If this is not the case, please report this error message at',
        'https://gitlab.com/havenofcode/challenge-run-counter/app/-/issues'
      ].join(' '),
    );

    app.exit(1);
  }
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    MainWindow.open();
  }
});

// main
ipcMain.on('show-context-menu', (event) => {
  const menu = Menu.buildFromTemplate([
    {
      label: 'Cut',
      role: 'cut'
    },
    {
      label: 'Copy',
      role: 'copy'
    },
    {
      label: 'Paste',
      role: 'paste'
    },
    {
      label: 'Select All',
      role: 'selectAll'
    },
    {
      label: 'Inspect Element',
      role: 'toggleDevTools'
    }
  ]);
  menu.popup({window: BrowserWindow.fromWebContents(event.sender)})
});
