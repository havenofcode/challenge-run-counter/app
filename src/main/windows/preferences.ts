import path from 'path';

import {
  BrowserWindow,
} from 'electron';

export class PreferencesWindow {
  private static instance: PreferencesWindow | null = null;
  static BrowserWindowClass = BrowserWindow;

  private win: BrowserWindow | null = new PreferencesWindow.BrowserWindowClass({
    height: 600,
    width: 800,
    webPreferences: {
      preload: path.resolve(__dirname, '..', '..', 'preload', 'index.js'),
      devTools: true,
    }
  });

  emitViewerSettingsUpdated() {
    if (this.win) {
      this.win.webContents.send('active_viewer_settings_change');
    }
  }

  constructor() {
    if (PreferencesWindow.instance) {
      throw new Error('internal error: expected win to be null');
    }

    if (this.win) {
      this.win.setMenuBarVisibility(false);
      this.win.loadFile(path.resolve(__dirname, '../../../html/preferences.html'));
      this.win.on('closed', this.onClosed.bind(this));
    }
  }

  private onClosed() {
    this.win = null;
    PreferencesWindow.instance = null;
  }

  static open() {
    if (!PreferencesWindow.instance) {
      PreferencesWindow.instance = new PreferencesWindow();
    }
  }

  static get(): PreferencesWindow | null {
    return PreferencesWindow.instance;
  }
}
