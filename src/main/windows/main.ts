import path from 'path';

import {
  BrowserWindow,
} from 'electron';

import {IEventShortcut} from '../../common';

export class MainWindow {
  private static instance: MainWindow | null = null;

  private win: BrowserWindow | null = new BrowserWindow({
    height: 600,
    width: 800,
    webPreferences: {
      preload: path.resolve(__dirname, '..', '..', 'preload', 'index.js'),
    }
  });

  constructor() {
    if (MainWindow.instance) {
      throw new Error('internal error: expected win to be null');
    }

    if (this.win) {
      const filePath = path.resolve(__dirname, '..', '..', '..', 'html', 'split_counter.html');
      this.win.loadFile(filePath);
      this.win.on('closed', this.onClosed.bind(this));
    }
  }

  emitShortcutInvoked(name: string, e?: IEventShortcut) {
    if (this.win) {
      this.win.webContents.send(
        'event_shortcut_invoked',
        name,
        e,
      );
    }
  }

  static emitShortcutError(e: {name: string, message: string}) {
    if (MainWindow.instance) {
      MainWindow.instance.win.webContents.send('event_shortcut_error', e);
    }
  }

  static emitShortcutInvoked(name: string, e?: IEventShortcut) {
    if (MainWindow.instance) {
      MainWindow.instance.emitShortcutInvoked(name, e);
    }
  }

  private onClosed() {
    console.warn('main window closed');
    this.win = null;
    MainWindow.instance = null;
  }

  static open() {
    if (!MainWindow.instance) {
      MainWindow.instance = new MainWindow();
    }
  }

  static get(): MainWindow | null {
    return MainWindow.instance;
  }
}
