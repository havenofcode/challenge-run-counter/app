import {getSplitPageDataHandler} from '../main/ipc_api/handlers';
import {AppSetting} from '../main/model';
import {DisplayServer} from './display_server';
export * from './display_server';
export * from './model';

export const getCmsData = async () => {
  const appSetting = await AppSetting
    .query()
    .findById(1);

  const data = await getSplitPageDataHandler(
    {},
    `${appSetting.current_run_scenario_id}`,
  );

  return data;
}

export const startWebSocketServer = async (): Promise<DisplayServer> => {
  return await DisplayServer.start();
}
