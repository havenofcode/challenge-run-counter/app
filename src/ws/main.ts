import {startWebSocketServer} from './';
import {initializeModels} from '../main/model';

(async () => {
  await initializeModels();
  startWebSocketServer();
})()
  .then(() => console.log('running'))
  .catch(console.error);
