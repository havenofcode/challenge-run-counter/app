import {URL} from 'url';
import {promises as fs} from 'fs';
import path from 'path';
import {Server as SocketIOServer} from 'socket.io';
import express, {Request, Response, NextFunction} from 'express';
import {Server} from 'http';
import {app as electronApp} from 'electron';
import {logger} from '../common/util';
import {PreferencesWindow} from '../main/windows/preferences';

import {
  getActiveViewerSettingsHandler,
  getViewerSettingsHandler,
} from '../main/ipc_api/handlers';

import {
  AppSetting,
  ViewerSetting,
  IViewerSettingsConfiguration,
  IViewerSettingsJson,
} from '../main/model';

import {getSplits} from './model';

import {
  IPageSettings,
  INotifyConfigurationAvailable,
} from '../common';

export const getServerOptions = async () => {
  if (process.env.NODE_ENV === 'development') {
    return {
      cors: {
        origin: `*`,
        methods: ['GET', 'POST'],
      }
    }
  }
}

type CountableName = 'hits_way' | 'hits_boss';

export class DisplayServer {
  static instance: DisplayServer | null = null;
  connectedConfigurations: {[appName: string]: IViewerSettingsConfiguration} = {};
  server: Server;
  io: SocketIOServer;

  constructor(server: Server, io: SocketIOServer) {
    this.server = server;
    this.io = io;
  }

  async onNotifyConfigurationAvailable(
    req: INotifyConfigurationAvailable,
    cb: (err: Error) => void,
  ) {
    const {configuration} = req;
    const settings_schema = JSON.stringify(configuration);

    try {
      const viewerSetting = await ViewerSetting.query().findOne({name: req.name});

      const data = {
        name: req.name,
        settings_schema,
      }

      if (viewerSetting) {
        await viewerSetting.$query().patch(data);
      } else {
        await ViewerSetting.query().insert(data);
      }

      this.connectedConfigurations[req.name] = req.configuration;
      const preferencesWindow = PreferencesWindow.get();
      if (preferencesWindow) {
        preferencesWindow.emitViewerSettingsUpdated();
      }
    } catch (err) {
      if (cb) {
        cb(err);
      } else {
        throw err;
      }
    }
  }

  static emitPageSettingsUpdate(e: Partial<IPageSettings>) {
    if (this.instance) {
      logger.info('wss:page-settings-update');
      this.instance.io.of('/').emit('page-settings-update', e)
    }
  }

  static emitSetActiveSplit(id: string, time: number) {
    if (this.instance) {
      logger.info('wss:set-active-split');
      this.instance.io.of('/').emit('set-active-split', id, time);
    }
  }

  static emitSetCountByName(id: string, name: string, val: number) {
    if (this.instance) {
      logger.info('wss:set-count-by-name');
      this.instance.io.of('/').emit('set-count-by-name', id, name, val);
    }
  }

  static emitNextSplit(time: number) {
    if (this.instance) {
      logger.info('wss:next-split');
      this.instance.io.of('/').emit('next-split', time);
    }
  }

  static emitPrevSplit(time: number) {
    if (this.instance) {
      logger.info('wss:prev-split');
      this.instance.io.of('/').emit('prev-split', time);
    }
  }

  static emitIncrementName(name: CountableName) {
    if (this.instance) {
      logger.info('wss:increment-count');
      this.instance.io.of('/').emit('increment-count', name);
    }
  }

  static emitDecrementName(name: CountableName) {
    if (this.instance) {
      logger.info('wss:decrement-count');
      this.instance.io.of('/').emit('decrement-count', name);
    }
  }

  static emitTemplatesUpdated() {
    if (this.instance) {
      logger.info('wss:templates-updated');
      this.instance.io.of('/').emit('templates-updated');
    }
  }

  static emitServerRestartNow() {
    if (this.instance) {
      logger.info('wss:server-restart-now');
      this.instance.io.of('/').emit('server-restart-now');
    }
  }

  static async stop() {
    if (!this.instance) {
      return Promise.reject(new Error('display server not started'));
    }

    await new Promise<void>((resolve) => {
      this.instance.server.close(() => {
        resolve();
      });
    });

    delete this.instance;
  }

  static async restart() {
    if (this.instance) {
      this.emitServerRestartNow();
    }
  }

  private static async getExtraObsPageSettingsPath(
    appSetting: AppSetting
  ): Promise<string | null> {
    const dir = appSetting.extra_obs_pages_directory;
    if (dir) {
      try {
        const stat = await fs.stat(dir);

        if (stat.isDirectory()) {
          return dir;
        }

        console.warn('extra_obs_pages_directory is not a directory', dir);
      } catch (err) {
        console.warn('extra_obs_pages_directory does not exist', dir);
      }
    }
    return null;
  }

  static async sendSplits() {
    if (this.instance) {
      logger.info('wss:splits-updated');
      const data = await getSplits();
      this.instance.io.of('/').emit('splits-updated', data);
    }
  }

  static async start() {
    if (DisplayServer.instance) {
      console.warn('attempting to start server when already started');
      return Promise.resolve(DisplayServer.instance);
    }

    const app = express();
    const server = new Server(app);
    const io = new SocketIOServer(server, await getServerOptions());
    const appSetting = await AppSetting.query().findById('1');
    const appPath = electronApp.getAppPath();

    const viewerDir = path.join(
      appPath,
      'node_modules',
      '@havenofcode',
      'challenge-run-client'
    );

    const defaultTemplateDir = path.join(viewerDir, 'examples');
    const rootRouter = express.Router();

    app.use('/dist', express.static(path.join(viewerDir, 'dist')));
    app.use('/examples', express.static(path.join(viewerDir, 'examples')));
    app.use('/favicon.ico', express.static(path.join(appPath, 'asset', 'favicon.ico')));

    app.get('/default.html', (req: Request, res: Response) => {
      res.redirect('/examples/default.html');
      res.end();
    });

    rootRouter.use('/custom', async (req: Request, res: Response, next: NextFunction) => {
      const appSetting = await AppSetting.query().findById('1');
      const extraObsPagesDir = await this.getExtraObsPageSettingsPath(appSetting);

      if (extraObsPagesDir) {
        express.static(extraObsPagesDir)(req, res, next);
      } else {
        res.status(404);
        res.end();
      }
    });

    rootRouter.get('/api/configured-resource', async (req: Request, res: Response, next: NextFunction) => {
      if (!req.query.absolute_path) {
        res.status(404);
        res.end();
        return;
      }

      const absolute_path = req.query.absolute_path as string;
      const appNames = await getActiveViewerSettingsHandler();

      for (let x = 0; x < appNames.length; ++x) {
        const viewerSetting = await getViewerSettingsHandler({}, appNames[x]);
        const configuration = JSON.parse(viewerSetting.settings) as IViewerSettingsJson;

        if (!configuration) {
          continue;
        }

        const propertyNames = Object.keys(configuration);

        for (let y = 0; y < propertyNames.length; ++y) {
          const value = configuration[propertyNames[y]];

          if (value === absolute_path) {
            const origin = `http://${appSetting.obs_browser_host}:${appSetting.obs_browser_port}`;
            const newUrl = new URL(absolute_path, origin);
            req.url = newUrl.href;
            return express.static('/')(req, res, next);
          }
        }
      }

      res.status(404);
      res.end();
    });

    rootRouter.get('/api/templates', async (req: Request, res: Response) => {
      const result: {defaultTemplates: string[]; userTemplates: string[];} = {
        defaultTemplates: [],
        userTemplates: [],
      }

      const defaultFolderFiles = await fs.readdir(defaultTemplateDir);

      const defaultHtmlFiles = defaultFolderFiles.filter((name) => {
        return path.extname(name) === '.html';
      });

      result.defaultTemplates = defaultHtmlFiles

      // get custom obs pages dir
      const appSetting = await AppSetting.query().findById('1');
      const extraObsPagesDir = await this.getExtraObsPageSettingsPath(appSetting);

      if (extraObsPagesDir) {
        try {
          const customFolderFiles = await fs.readdir(extraObsPagesDir);
          const customHtmlFiles = customFolderFiles.filter((name) => {
            return path.extname(name) === '.html';
          });

          result.userTemplates = customHtmlFiles;
        } catch (err) {
          console.warn('unable to open custom obs pages dir, ignoring', extraObsPagesDir);
          console.error(err);
        }
      }

      res.status(200);
      res.json(result);
      res.end();
    });

    rootRouter.get('/', (req: Request, res: Response) => {
      res.status(200);
      res.end();
    });

    app.use('/', rootRouter);

    io.on('connect', (socket) => {
      console.log(`display connected ${socket.id}`);

      if (this.instance) {
        socket.on(
          'notify-configuration-available',
          this.instance.onNotifyConfigurationAvailable.bind(this.instance),
        );
      }

      DisplayServer.sendSplits();

      socket.on('disconnect', () => {
        console.log(`display disconnected ${socket.id}`);
      });
    });

    await (new Promise((resolve, reject) => {
      const opts = {
        port: appSetting.obs_browser_port,
        host: appSetting.obs_browser_host,
      }

      server.on('error', (err) => {
        const listenError = err as (Error & {code?: string})

        // the most common error to occur
        if (listenError.code === 'EADDRINUSE') {
          reject(err);
        }
      });

      server.listen(opts, () => {
        console.log('server started');
        resolve(io);
      });
    }));

    DisplayServer.instance = new DisplayServer(server, io);
    return DisplayServer.instance;
  }

  static get(): DisplayServer | null {
    return DisplayServer.instance;
  }
}
