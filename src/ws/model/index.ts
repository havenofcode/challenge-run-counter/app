import {promises as fs} from 'fs';
import path from 'path';
import memoize from 'fast-memoize';

import {
  AppSetting,
  IRoute,
  ISplitsData,
  IViewerSettingsJson,
  ViewerSetting,
} from '../../main/model';

import {
  getSplitPageDataHandler,
  getObsPageStylesHandler,
  getActiveViewerSettingsHandler,
} from '../../main/ipc_api/handlers';

import {getDuration} from '../../renderer/util/time';

export interface IPackageJson {
  version: string;
}

export const memoizeGetPackageJson = memoize(async (): Promise<IPackageJson> => {
  const filePath = path.resolve(__dirname, '..', '..', '..', 'package.json');
  const packageJsonString = await fs.readFile(filePath, 'utf8');
  const packageJsonObject = JSON.parse(packageJsonString) as IPackageJson;
  return packageJsonObject;
});

async function tryGetObsPageStyles() {
  try {
    return await getObsPageStylesHandler({});
  } catch (err) {
    console.warn('unable to get obs page styles');
  }

  return '';
}

const tryGetPageSettings = async (name: string): Promise<IViewerSettingsJson> => {
  const viewerSetting = await ViewerSetting.query().findOne({
    name,
  });

  if (viewerSetting) {
    return JSON.parse(viewerSetting.settings);
  }

  return {}
}

export const getSplits = async (): Promise<ISplitsData> => {
  const appSetting = await AppSetting.query().findById(1);
  const css = await tryGetObsPageStyles();
  const routesById: {[key: number]: IRoute} = {};

  const splitsData = await getSplitPageDataHandler(
    {},
    `${appSetting.current_run_scenario_id}`,
  );

  const {currentSplit} = splitsData;
  const timePoints = splitsData.currentSplitCollection.data.run_attempt_time_points;
  const attemptDuration = getDuration(timePoints);

  const routes: IRoute[] = splitsData.splitCollections.map(({splitCollection}) => {
    routesById[splitCollection.id] = {
      id: splitCollection.id,
      name: splitCollection.name,
      pb_time: splitsData.currentSplitCollection.splitCollection.pb_time,
      pb_hits: splitsData.currentSplitCollection.splitCollection.pb_hits,
    }
    return routesById[splitCollection.id];
  });

  // get all page settings for each connected client
  const activeViewerConfigurations = await getActiveViewerSettingsHandler();
  const viewerSettings: {[key: string]: IViewerSettingsJson} = {}

  for (let i = 0; i < activeViewerConfigurations.length; ++i) {
    const name = activeViewerConfigurations[i];
    const appViewerSettings = await tryGetPageSettings(name);
    viewerSettings[name] = appViewerSettings;
  }

  const packageJson = await memoizeGetPackageJson();

  return {
    api_version: packageJson.version,
    routes,
    attemptDuration,
    activeRoute: routesById[currentSplit.splitStep.split_collection_id],
    pageSettings: {
      width: appSetting.obs_page_width,
      height: appSetting.obs_page_height,
      css,
      viewerSettings,
    },

    activeSplits: splitsData.splits.map(({splitStep, data}) => {
      return {
        id: data.id,
        name: splitStep.name,
        pb_hits: splitStep.pb_hits,
        pb_time: splitStep.pb_time,
        hits_way: data.hits_way,
        hits_boss: data.hits_boss,
        active: currentSplit.splitStep.id === splitStep.id,
      }
    }),
    bestSegments: splitsData.bestSegments.map(({splitStep, data}) => {
      return {
        id: data.id,
        name: splitStep.name,
        pb_hits: splitStep.pb_hits,
        pb_time: splitStep.pb_time,
        hits_way: data.hits_way,
        hits_boss: data.hits_boss,
        active: currentSplit.splitStep.id === splitStep.id,
      }
    }),
  }
}
