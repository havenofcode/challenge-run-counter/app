const path = require('path');
const child_process = require('child_process');
const os = require('os');
const {setupKeychain} = require('./scripts/osx_codesign_utils');

const getOsxSignOptions = () => {
  if (process.env.NODE_ENV !== 'production') {
    return {
      osxSign: false,
      osxNotarize: false,
    }
  }

  const base64ToUtf8 = (b64Val) => Buffer.from(b64Val, 'base64').toString('utf8');
  const appleId = process.env.CHALLENGE_RUN_COUNTER_CI_APPLE_ID;
  const appleIdPassword = process.env.CHALLENGE_RUN_COUNTER_CI_APPLE_PASSWORD;
  const osxSignIdentity = base64ToUtf8(process.env.CHALLENGE_RUN_COUNTER_CI_APP_SIGN_IDENTITY);

  return {
    osxSign: {
      identity: osxSignIdentity,
      hardenedRuntime: true,
      entitlements: 'asset/entitlements.mac.plist',
      'entitlements-inherit': 'asset/entitlements.mac.plist',
      'signature-flags': 'library',
    },
    osxNotarize: {
      appBundleId: 'com.havenofcode.challenge-run-counter',
      appleId,
      appleIdPassword,
    },
  }
}

module.exports = {
  packagerConfig: {
    icon: path.join(__dirname, 'asset', 'icon-256'),
    appBundleId: 'com.havenofcode.challenge-run-counter',
    ...(getOsxSignOptions()),
    asar: (os.platform() !== 'darwin'),
  },
  hooks: {
    async packageAfterPrune(packaerConfig, buildPath, electronVersion, platform, arch) {
      // we must remove node_modules/sqlite3/build after sqlite3 builds from
      // source. otherwise codesign fails.
      await new Promise((resolve, reject) => {

        const rmCmd = os.platform() === 'win32' ?
          'powershell.exe Remove-Item -Recurse -Force' :
          'rm -rf';

        const dir = path.join(buildPath, 'node_modules', 'sqlite3', 'build');

        child_process.exec(`${rmCmd} ${dir}`, (err, stdout, stderr) => {
          console.log({stdout, stderr});

          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });

      // Apple requires that app bundles be notarized in
      // order to run on m1 macs. This prevents the "App bundle is damaged"
      // message when release is downloaded from a browser.
      if (os.platform() === 'darwin' && process.env.NODE_ENV === 'production') {
        await setupKeychain();
      }
    },
  },
  makers: [
    // {
    //   name: '@electron-forge/maker-zip',
    //   platforms: ['darwin', 'linux'],
    // },
    // {
    //  name: '@electron-forge/maker-squirrel',
    //  platforms: ['win32'],
    // },
    {
      name: '@electron-forge/maker-appx',
      config: {
        packageVersion: '1.0.0.0',
        platforms: ['win32'],
      },
    },
  ],
}
