exports.up = async (knex) => {
  await knex.schema.createTable('run_attempt_time_points', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');

    table.integer('run_session_split_id')
      .references('id')
      .inTable('run_session_splits')
      .notNullable();

    table.integer('run_session_split_collection_id')
      .references('id')
      .inTable('run_session_split_collections')
      .notNullable();

    table.enu('event_type', [
      'attempt_started',
      'attempt_stopped',
      'split_started',
      'split_stopped',
    ])
      .notNullable();

    // epoch time in milliseconds
    table.integer('time').notNullable();
  });
}

exports.down = (knex) => {
  return knex.schema
    .dropTableIfExists('run_attempt_time_points')
}
