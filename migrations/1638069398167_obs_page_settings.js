exports.up = async (knex) => {
  await knex.schema.alterTable('app_settings', (table) => {
    // relative paths are relative to the users configuration director
    // ex: on linux default value of `default.css` would be
    // `~/.config/challenge-run-counter/challenge_run_data/default.css`
    table.string('global_css_overrides').defaultTo('default.css');
  });
}

exports.down = async (knex) => {
  await knex.schema.alterTable('app_settings', (table) => {
    table.dropColumn('global_css_overrides');
  });
}
