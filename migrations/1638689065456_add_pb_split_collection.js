exports.up = async (knex) => {
  await knex.schema.alterTable('split_collections', (table) => {
    table.integer('pb_time').defaultTo(0);
    table.integer('pb_hits').defaultTo(0);
  });

  await knex.schema.alterTable('run_session_split_collections', (table) => {
    table.integer('hits_boss').defaultTo(0);
    table.integer('hits_way').defaultTo(0);
    table.integer('time').defaultTo(0);
  });
}

exports.down = async (knex) => {
  await knex.schema.alterTable('split_collections', (table) => {
    table.dropColumn('pb_time');
    table.dropColumn('pb_hits');
  });

  await knex.schema.alterTable('run_session_split_collections', (table) => {
    table.dropColumn('hits_boss');
    table.dropColumn('hits_way');
    table.dropColumn('time');
  });
}
