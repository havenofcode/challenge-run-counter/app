exports.up = async (knex) => {
  await knex.schema.alterTable('app_settings', (table) => {
    // relative paths are relative to the users configuration director
    // ex: on linux default value of `default.css` would be
    // `~/.config/challenge-run-counter/challenge_run_data/default.css`
    table.string('obs_page_width').defaultTo('auto');
    table.string('obs_page_height').defaultTo('auto');
  });
}

exports.down = async (knex) => {
  await knex.schema.alterTable('app_settings', (table) => {
    table.dropColumn('obs_page_width');
    table.dropColumn('obs_page_height');
  });
}
