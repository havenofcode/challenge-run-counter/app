exports.up = async (knex) => {

  await knex.schema.createTable('run_scenarios', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');
    table.string('name').notNullable();
    table.boolean('deleted').notNullable().defaultTo(0);
  });

  await knex.schema.createTable('split_collections', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');
    table.boolean('deleted').defaultTo(false).notNullable();
    table.string('name').notNullable();

    table.integer('run_scenario_id')
      .references('id')
      .inTable('run_scenarios')
      .notNullable();
  });

  await knex.schema.createTable('split_steps', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');

    table.integer('order')
      .defaultTo(0)
      .notNullable();

    table.integer('split_collection_id')
      .references('id')
      .inTable('split_collections')
      .notNullable();

    table.string('name').notNullable();
    table.integer('pb_hits');
    table.integer('pb_time');
    table.boolean('deleted').defaultTo(false).notNullable();
  });

  await knex.schema.createTable('run_sessions', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');

    table.integer('current_session_split_id')
      .references('id')
      .inTable('run_session_splits');

    table.integer('run_scenario_id')
      .references('id')
      .inTable('run_scenarios')
      .notNullable();

    // epoch in milliseconds
    table.integer('start_time');
    table.integer('end_time');
  });

  await knex.schema.createTable('run_session_splits', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');

    table.integer('run_session_id')
      .references('id')
      .inTable('run_sessions')
      .notNullable();

    table.integer('split_step_id')
      .references('id')
      .inTable('split_steps')
      .notNullable();

    table.integer('hits_way').defaultTo(0).notNullable();
    table.integer('hits_boss').defaultTo(0).notNullable();
    table.integer('time').defaultTo(0).notNullable();
  });

  await knex.schema.createTable('run_session_split_collections', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');
    table.integer('start_time');
    table.integer('end_time');

    table.integer('split_collection_id')
      .references('id')
      .inTable('split_collections')
      .notNullable();

    table.integer('run_session_id')
      .references('id')
      .inTable('run_sessions')
      .notNullable();

    table.integer('current_split_step_id')
      .references('id')
      .inTable('split_steps')
      .notNullable();
  });

  await knex.schema.createTable('app_settings', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');
    table.string('obs_browser_host')
      .notNullable()
      .defaultTo('localhost');

    table.string('obs_browser_port')
      .notNullable()
      .defaultTo('42069');

    table.string('hotkey_next_split')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_prev_split')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_increment_boss_hit')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_increment_way_hit')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_decrement_boss_hit')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_decrement_way_hit')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_restart_run')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_start_timer')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_stop_timer')
      .notNullable()
      .defaultTo('');

    table.string('hotkey_record_pb')
      .notNullable()
      .defaultTo('');

    table.integer('current_run_scenario_id')
      .references('id')
      .inTable('run_scenarios');
  });
}

exports.down = (knex) => {
  return knex.schema
    .dropTableIfExists('app_settings')
    .dropTableIfExists('run_session_split_collections')
    .dropTableIfExists('run_scenario_splits')
    .dropTableIfExists('run_sessions')
    .dropTableIfExists('split_steps')
    .dropTableIfExists('split_collections')
    .dropTableIfExists('run_session_splits')
    .dropTableIfExists('run_scenarios');
}
