exports.up = async (knex) => {
  await knex.schema.alterTable('app_settings', (table) => {
    table.string('extra_obs_pages_directory').defaultTo(null);
  });
}

exports.down = async (knex) => {
  await knex.schema.alterTable('app_settings', (table) => {
    table.dropColumn('extra_obs_pages_directory');
  });
}
