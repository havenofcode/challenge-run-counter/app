exports.up = async (knex) => {
  await knex.schema.createTable('viewer_settings', (table) => {
    table.increments('id').primary();
    table.integer('created_at');
    table.integer('updated_at');

    table.string('name')
      .notNullable()
      .unique();

    table.string('settings').notNullable().defaultTo('{}');
  });
}

exports.down = (knex) => {
  return knex.schema.dropTableIfExists('viewer_settings');
}
