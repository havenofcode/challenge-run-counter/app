exports.up = async (knex) => {
  await knex.schema.alterTable('viewer_settings', (table) => {
    table.string('settings_schema').notNullable().defaultTo('{}');
  });
}

exports.down = async (knex) => {
  await knex.schema.alterTable('viewer_settings', (table) => {
    table.dropColumn('settings_schema');
  });
}
