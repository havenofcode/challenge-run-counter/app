CHANGELOG
=========

## [1.5.4](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.5.3...v1.5.4) (2022-01-07)


### Bug Fixes

* persist splits on change instead of blur for [#18](https://gitlab.com/havenofcode/challenge-run-counter/app/issues/18) ([5fef7a7](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/5fef7a7f519a37c0de0e2d25734b8645454a6675))

## [1.5.3](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.5.2...v1.5.3) (2022-01-07)


### Bug Fixes

* add modular page specific settings api ([8776353](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/877635389c9c66a142997f704ec24a925798c28b))
* endpoint for fetching static files defined by client viewer configuration ([0d7ca4e](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/0d7ca4ebb781a645b9fc6125290980b1bbc3a404))
* refactor module settings api response to be indexed by app name ([761a65a](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/761a65afe5fa54711047889fa9c0e1ae53cf921a))

## [1.5.2](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.5.1...v1.5.2) (2022-01-04)


### Bug Fixes

* write logs for debugging and optimize some queries ([f9a5fe4](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/f9a5fe464f9dd2d6bc21925718028ee2e3a51d4c))

## [1.5.1](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.5.0...v1.5.1) (2022-01-02)


### Bug Fixes

* filter run session collections whre split collection is deleted ([0ad2e20](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/0ad2e201dcb1b32d393a13083b49bb6ac756a608))
* render viewer options for new install/upgrades ([a499f1c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/a499f1c3c08f6cd4dcb285cdf4ed4de575b949f2))

# [1.5.0](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.16...v1.5.0) (2022-01-02)


### Features

* improve scrolling, added progress bar, and added viewer settings ([ffee01b](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/ffee01bebfc056ff87963e98c77c2b255ecd530b))

## [1.4.16](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.15...v1.4.16) (2021-12-29)


### Bug Fixes

* add 100% code coverage for decrement_run_session_split_stat handler ([a418e1b](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/a418e1b072ab24dec9276bdc16482c99464957b1))
* add 100% code coverage for increment_run_session_split_stat, remove_run_scenario handler ([84c465f](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/84c465fc1f50bcd4560f466d5c15763b5ad101db))
* add 100% code coverage for record_session_as_pb handler ([e73833d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/e73833d29731b2946ae4715fee03add889a65481))
* add 100% code coverage for remove_split_collection handler ([cef929c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/cef929cdb038e4cd5fd540ee270e183b28439a20))
* add 100% code coverage for restart_run handler ([420ac52](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/420ac52a9735a347da2fa785d0cdb079f7f128af))
* add 100% code coverage for set_active_split handler ([2fe44f7](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/2fe44f762169ad19f6cae44b1fc30b6b88f9efe5))
* add 100% code coverage for set_prev_active_split handler ([9fcf79d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/9fcf79d7e2f98849bd68c2e4308d4d6cede46705))
* add 100% code coverage for try_set_global_shortcut handler ([c2b41eb](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/c2b41ebdc656327ecea7ac405f2be65e23a673e7))
* add 100% code coverage for update_app_setting handler ([0d23be8](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/0d23be85cd414c65aae247f4ee0da2e96c6da2f8))
* add 100% code coverage for update_obs_page_styles handler ([0853803](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/0853803b932734da5e0670d86e702f3124819a5d))
* add 100% code coverage for update_run_session handler ([2750fac](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/2750fac3c428abafe57cf07d9ab5037d753f3f1b))
* add 100% code coverage for update_split_collection handler ([10b6ff9](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/10b6ff9338ece987573be5e3c49f465a953c9d9a))
* add more unit tests for ipc handlers ([1222502](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/122250287f09dde521502941d2c91a1192745ab5))
* enable long polling in viewer clients for [#15](https://gitlab.com/havenofcode/challenge-run-counter/app/issues/15) ([88dddbf](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/88dddbfedc4cde5704dcb15f6ce5e3461c064e8a))
* remove unused api handlers for code coverage ([ed69fb3](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/ed69fb3fcad9aa35e656a4683cf6823eb03986d6))

## [1.4.15](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.14...v1.4.15) (2021-12-26)


### Bug Fixes

* add about menu and bump challenge-run-client for small fixes ([4760c3b](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/4760c3be9214319727a6fc2badf2f52cf501b098))

## [1.4.14](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.13...v1.4.14) (2021-12-24)


### Bug Fixes

* bump challenge-run-client for refresh new version ([f32e6a5](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/f32e6a56e8863c84c7fef13d22dc7ec4c96f56b9))
* send api_version (package.json) for on splits-updated event ([502cf19](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/502cf19f93a93a9776b978e40158b377ab0b7d08))
* simplify routing and fix record as pb when run contain deleted splits ([493f676](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/493f67643dc8aa56568d26675d193ea1557d6136))

## [1.4.13](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.12...v1.4.13) (2021-12-18)


### Bug Fixes

* bump challenge-run-client version for debugging ([6555f64](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/6555f64357dcd21ba31af6b101f38b9bfa6b6b64))
* remove ping/pong action in DisplayServer ([0854b1a](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/0854b1a567eaa3c3aab6e4e4ac22d44d8c0d08da))

## [1.4.12](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.11...v1.4.12) (2021-12-18)


### Bug Fixes

* add more coverage for get_active_split handler ([1cefbdf](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/1cefbdf1ebd67753475a161047827f8d17f932a8))
* stop timer if next button pressed on last split, and fix possible deadlock issues ([d146c1b](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/d146c1b3c41fbb789965c587f18e21dbec372cd8))

## [1.4.11](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.10...v1.4.11) (2021-12-08)


### Bug Fixes

* send current segment if no other segments exist for new runs ([736d2e4](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/736d2e4540180af0a9d72e3c1009fc144b3eebfc))

## [1.4.10](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.9...v1.4.10) (2021-12-07)


### Bug Fixes

* bump challenge-run-client version for 'speed-run.html' example ([8de8b9b](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/8de8b9b53e40b88dffc16039a29018e6f08ebc5a))
* send best segments to client ([e970e63](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/e970e633c4af8446442b36c814b6fb280f946492))

## [1.4.9](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.8...v1.4.9) (2021-12-06)


### Bug Fixes

* add columns for pb time, diff time, and tidy alignment ([c7fb29f](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/c7fb29fc056225856341fb5fda01078be9e3bb27))
* render pb duration, and current attempt duration ([9e9779a](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/9e9779a7f4e1e4c8ce3b2e19277a2f8204ed31d3))
* send websocket data for pb duration ([dc24302](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/dc24302b30a439850ecaa219c40e72929b636bed))

## [1.4.8](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.7...v1.4.8) (2021-12-06)


### Bug Fixes

* bump counter version for correct pb count on default page ([8bae96a](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/8bae96a3b592dbb2f30ab97e528f20fb94b5cdbf))
* page blank on new install and disable mac mas builds for faster build ([37044e9](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/37044e992b085a7dde57a80490478c5e51c83376))
* page does not update when hotkeys are pressed ([83e7312](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/83e73129618798dd4a9467c44bfa6290b521b125))

## [1.4.7](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.6...v1.4.7) (2021-12-05)


### Bug Fixes

* add common util for personal best duration count ([131ed87](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/131ed87da306879aad842adde0d6482821538da3))
* bump challenge-run-client version for recording pb ([36b1e5c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/36b1e5ce82a0b537dbb6d742925dfcbb7dbd3010))
* store the duration of splits for recording personal best ([af5737c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/af5737c31f7e20b11b1ed5a82cf6359ecd19007e))
* timer incorrectly off from timeout ([d0dc56d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/d0dc56da354e46916e1061ba36c68d825d3cc238))

## [1.4.6](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.5...v1.4.6) (2021-12-05)


### Bug Fixes

* use special key names in accelerator for global shortcuts ([b20db96](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/b20db9666392e712810f5eb19f4b33228363f7f3))

## [1.4.5](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.4...v1.4.5) (2021-12-05)


### Bug Fixes

* notify set active split and set count for display ([5ede398](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/5ede398b99d05d3e4fb4f6be32b104173fdfb2cb))
* notify splits data on change instead of periodically ([d019a2a](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/d019a2a46bd054a7fd625688d5a710f25d68c870))
* validate odd css values to fix blank page ([dce280c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/dce280caa910ce62a9cd605070c0bb08f3925a09))

## [1.4.4](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.3...v1.4.4) (2021-12-04)


### Bug Fixes

* send appearance page settings on chage for responsive updates ([30c9484](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/30c9484e25e38e605225ac515ec7a9d6b017d3de))

## [1.4.3](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.2...v1.4.3) (2021-12-04)


### Bug Fixes

* add server shutdown + restart events for graceful restart ([8370e69](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/8370e699bad335292a82289358b395dc51c95419))

## [1.4.2](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.1...v1.4.2) (2021-12-01)


### Bug Fixes

* use npm runner ([01e8454](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/01e8454891eac5f802345ccdb501f1f6613da5e5))

## [1.4.1](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.4.0...v1.4.1) (2021-12-01)


### Bug Fixes

* add gitlab pages docs, link to wiki and more readme stuff ([dc913f6](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/dc913f64d50c62bd2f4e2dbc0889827c10ecd487))

# [1.4.0](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.3.1...v1.4.0) (2021-12-01)


### Bug Fixes

* backward compatible routes ([53184ee](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/53184eecb349bb6659357805b01331015eefabf0))
* remove some unused dependencies ([a5bfb68](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/a5bfb68f49ef7b5aa86b8b1e42fe6f869e538eee))


### Features

* add extra obs page setting ([2b70870](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/2b708709fc0fc7f970133afa48776dc1a6d722d0))

## [1.3.1](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.3.0...v1.3.1) (2021-11-29)


### Bug Fixes

* better timer format and update client ([55c06f4](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/55c06f4941dd2edc199c7800c2674a6dc7faee90))

# [1.3.0](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.2.0...v1.3.0) (2021-11-28)


### Bug Fixes

* add global css overrides form ([52331f8](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/52331f86bee6851e7f0cdda04da7c3f72027ccf1))
* add obs page with + height ([fd79ee3](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/fd79ee3947e26875e56568ad04070e1e3d8edb9a))
* ship package app resources as an asar archive ([6e81179](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/6e81179cd812527b2daa1a8e4f2536b14d7f2b83))


### Features

* emit page settings data and time data to obs page ([721d274](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/721d27481848a8fe31a759321a4947e6358b9abe))

# [1.2.0](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.1.5...v1.2.0) (2021-11-27)


### Features

* add new table for tracking split start/stop time ([a6f6adb](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/a6f6adbb5c63e178f8f1a5116b2de592e835db32))
* render the attempt time for split and entire attempt ([8675145](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/8675145e0c675fb00feec7983af4dadc1470c7a5))

## [1.1.5](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.1.4...v1.1.5) (2021-11-27)


### Bug Fixes

* sign and notarize app for mac osx builds ([4254c4e](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/4254c4e1aeeb1e268c6a81d35a0f567eef229a2e))

## [1.1.4](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.1.3...v1.1.4) (2021-11-22)


### Bug Fixes

* add mas builds and archive artifacts in private storage ([afb6442](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/afb644244dcdf162d5b355582a61bb35968aa429))

## [1.1.3](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.1.2...v1.1.3) (2021-11-21)


### Bug Fixes

* build on mac intel runner for x64 packages ([c0c49cf](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/c0c49cfd6acc79b7adc949bb7fb84ecd90c86c15))

## [1.1.2](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.1.1...v1.1.2) (2021-11-19)


### Bug Fixes

* fix typo and rearrange hotkey form for preferences page ([29311f2](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/29311f2926f8edbae5c5df8449d1d30a63355ec8))

## [1.1.1](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.1.0...v1.1.1) (2021-11-03)


### Bug Fixes

* add confirm dialog for deleting splits and scenarios ([2d6f39f](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/2d6f39fab54018d044482f66106a9175f1602ed1))
* build releases via pipeline and fix up package.json deps ([ea443c7](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/ea443c7af3f3ddf810a6a606bbd44429b900d1a3))
* emit more obs browser page events for faster responses ([c689112](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/c68911206a9bde1f649905b17eff8de276f0ab61))
* handle error on startup and add obs page link ([a4e9998](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/a4e999865476eadd03713d04290f9d8171f3f4ff))
* immediate user feedback for hotkeys ([75dd7cf](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/75dd7cfb668709344bf741e0d9fe30c7142ab1e0))
* remove dev tools open in preferences ([bc24245](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/bc2424514c055c19205bc21e6d48f9b1fd367002))
* use the group runner for ci/cd ([ac5d935](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/ac5d935b7541c483eaae792fd72d006b71ed1b33))

# [1.1.0](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.0.0...v1.1.0) (2021-10-31)


### Bug Fixes

* fix order of splits in default data and a lot of other bugs ([5a35078](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/5a35078bc0e6799a0f5a654b4a5c551451e480da))


### Features

* include better default data for new install ([59950a4](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/59950a40a50db9dc2ec98dead646c4d8c6912c97))
* insert and duplicate routes ([fcaacdc](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/fcaacdc51e6555618e413c06d70e510dd0c0b6e6))

# 1.0.0 (2021-10-30)


### Bug Fixes

* .gitlab-ci.yml syntax ([bce7b8d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/bce7b8d8b8ca8495bfda32a6cb12b8cd20f27e5e))
* add docs to bump version ([98cd4cb](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/98cd4cbec82994de366b74093b222449fa58616c))
* Add get_split_scenario and get_split_collection ipc method and connect to app ([d2ee01c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/d2ee01c6c1801238fd8eefb759a293ad66a39de7))
* ci/cd only run on protected branches ([b22cf75](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/b22cf75ddc8d625d23166a19486b862d5bb83413))
* configure ci/cd for internal runner ([bf86dee](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/bf86deeaff7fdc676b25b79511427412fcaf0a3d))
* do not run npm publish for semantic-release ([443ed7b](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/443ed7ba02c680c6f3075a98e4d18ef8c46e5909))
* enable npm semantic release plugin ([b5749b8](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/b5749b86423d58e803acf1ffd797ef8f0e98a04d))
* fix wrong path for obs browser page ([3e59053](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/3e59053a58cc0b53444eec8ae358284c31bd4793))
* get semantic-release working ([2499bf1](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/2499bf19055545ef8f4d2524d08f70b17be95e6c))
* migrate to official gitlab repo ([6e81eda](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/6e81eda388882dda16946220f07024145e1ed08c))
* prepare to release to public gitlab ([6f6912d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/6f6912d1cec38830dc82bcc0b713e749d44e4758))
* reapply ci/cd jobs ([13cc33d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/13cc33da2f2c4c5f570e35dea3431955201c0b0b))
* release first version ([c95409f](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/c95409f613ede59718190b6d0e4b4c8d6e4bf774))
* remove broken makers ([7461e0c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/7461e0c68c14ef4c4c58c5c56db8cbd631781acc))
* remove old packages ([35e0c99](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/35e0c99d583872373d78c77124b745d0f4c16a4a))
* tests should fail the build ([fa4137d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/fa4137d644f93e6cd58a9134f4b9e8ed0a9dd3e3))

# [1.0.0-beta.3](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2021-10-30)


### Bug Fixes

* fix wrong path for obs browser page ([3e59053](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/3e59053a58cc0b53444eec8ae358284c31bd4793))

# [1.0.0-beta.2](https://gitlab.com/havenofcode/challenge-run-counter/app/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2021-10-30)


### Bug Fixes

* do not run npm publish for semantic-release ([443ed7b](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/443ed7ba02c680c6f3075a98e4d18ef8c46e5909))
* enable npm semantic release plugin ([b5749b8](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/b5749b86423d58e803acf1ffd797ef8f0e98a04d))

# 1.0.0-beta.1 (2021-10-30)


### Bug Fixes

* .gitlab-ci.yml syntax ([bce7b8d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/bce7b8d8b8ca8495bfda32a6cb12b8cd20f27e5e))
* add docs to bump version ([98cd4cb](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/98cd4cbec82994de366b74093b222449fa58616c))
* Add get_split_scenario and get_split_collection ipc method and connect to app ([d2ee01c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/d2ee01c6c1801238fd8eefb759a293ad66a39de7))
* ci/cd only run on protected branches ([b22cf75](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/b22cf75ddc8d625d23166a19486b862d5bb83413))
* configure ci/cd for internal runner ([bf86dee](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/bf86deeaff7fdc676b25b79511427412fcaf0a3d))
* get semantic-release working ([2499bf1](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/2499bf19055545ef8f4d2524d08f70b17be95e6c))
* migrate to official gitlab repo ([6e81eda](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/6e81eda388882dda16946220f07024145e1ed08c))
* prepare to release to public gitlab ([6f6912d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/6f6912d1cec38830dc82bcc0b713e749d44e4758))
* reapply ci/cd jobs ([13cc33d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/13cc33da2f2c4c5f570e35dea3431955201c0b0b))
* remove broken makers ([7461e0c](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/7461e0c68c14ef4c4c58c5c56db8cbd631781acc))
* remove old packages ([35e0c99](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/35e0c99d583872373d78c77124b745d0f4c16a4a))
* tests should fail the build ([fa4137d](https://gitlab.com/havenofcode/challenge-run-counter/app/commit/fa4137d644f93e6cd58a9134f4b9e8ed0a9dd3e3))

## [1.0.2-beta.6](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.2-beta.5...v1.0.2-beta.6) (2021-10-30)


### Bug Fixes

* prepare to release to public gitlab ([6f6912d](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/6f6912d1cec38830dc82bcc0b713e749d44e4758))

## [1.0.2-beta.5](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.2-beta.4...v1.0.2-beta.5) (2021-09-26)


### Bug Fixes

* Add get_split_scenario and get_split_collection ipc method and connect to app ([d2ee01c](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/d2ee01c6c1801238fd8eefb759a293ad66a39de7))

## [1.0.2-beta.4](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.2-beta.3...v1.0.2-beta.4) (2021-09-22)


### Bug Fixes

* add docs to bump version ([98cd4cb](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/98cd4cbec82994de366b74093b222449fa58616c))

## [1.0.2](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.1...v1.0.2) (2021-09-20)


### Bug Fixes

* .gitlab-ci.yml syntax ([bce7b8d](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/bce7b8d8b8ca8495bfda32a6cb12b8cd20f27e5e))
* add docs to bump version ([98cd4cb](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/98cd4cbec82994de366b74093b222449fa58616c))
* ci/cd only run on protected branches ([b22cf75](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/b22cf75ddc8d625d23166a19486b862d5bb83413))
* reapply ci/cd jobs ([13cc33d](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/13cc33da2f2c4c5f570e35dea3431955201c0b0b))
* remove broken makers ([7461e0c](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/7461e0c68c14ef4c4c58c5c56db8cbd631781acc))

## [1.0.2](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.1...v1.0.2) (2021-09-20)


### Bug Fixes

* .gitlab-ci.yml syntax ([bce7b8d](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/bce7b8d8b8ca8495bfda32a6cb12b8cd20f27e5e))
* add docs to bump version ([98cd4cb](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/98cd4cbec82994de366b74093b222449fa58616c))
* ci/cd only run on protected branches ([b22cf75](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/b22cf75ddc8d625d23166a19486b862d5bb83413))
* reapply ci/cd jobs ([13cc33d](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/13cc33da2f2c4c5f570e35dea3431955201c0b0b))
* remove broken makers ([7461e0c](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/7461e0c68c14ef4c4c58c5c56db8cbd631781acc))

## [1.0.2-beta.3](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.2-beta.2...v1.0.2-beta.3) (2021-09-20)


### Bug Fixes

* remove broken makers ([7461e0c](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/7461e0c68c14ef4c4c58c5c56db8cbd631781acc))

## [1.0.2-beta.2](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.2-beta.1...v1.0.2-beta.2) (2021-09-20)


### Bug Fixes

* .gitlab-ci.yml syntax ([bce7b8d](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/bce7b8d8b8ca8495bfda32a6cb12b8cd20f27e5e))
* ci/cd only run on protected branches ([b22cf75](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/b22cf75ddc8d625d23166a19486b862d5bb83413))

## [1.0.2-beta.1](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.1...v1.0.2-beta.1) (2021-09-20)


### Bug Fixes

* reapply ci/cd jobs ([13cc33d](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/13cc33da2f2c4c5f570e35dea3431955201c0b0b))

## [1.0.1](https://source.havenofcode.com/havenofcode/challenge-run-counter/compare/v1.0.0...v1.0.1) (2021-09-20)


### Bug Fixes

* remove old packages ([35e0c99](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/35e0c99d583872373d78c77124b745d0f4c16a4a))

# 1.0.0 (2021-09-20)


### Bug Fixes

* get semantic-release working ([2499bf1](https://source.havenofcode.com/havenofcode/challenge-run-counter/commit/2499bf19055545ef8f4d2524d08f70b17be95e6c))
