/* eslint @typescript-eslint/no-var-requires: "off" */
const path = require('path');
const {waitUntilDone} = require('./util');
const root = path.resolve(__dirname, '..');

const childProcessOptions = {
  cwd: root,
  stdio: 'inherit',
  env: {
    ...process.env,
    NODE_ENV: 'test',
  }
}

const runLint = () => waitUntilDone(['npm', 'run', 'test:lint'], childProcessOptions);
const runTest =() => waitUntilDone(['npm', 'run', 'test:unit'], childProcessOptions);

const main = async () => {
  await runLint();
  await runTest();
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
