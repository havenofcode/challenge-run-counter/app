/* eslint @typescript-eslint/no-var-requires: "off" */
const path = require('path');
const fs = require('fs').promises;
const uuid = require('uuid');
const {waitForCommand, mktempd} = require('./util');
const uuidName = uuid.v4();
const root = path.resolve(__dirname, '..');

const sensitiveChildProcessOpts = {
  cwd: root,
  // don't print any output
  stdio: 'ignore',
  env: {
    NODE_ENV: 'production',
    ...process.env,
  }
}

const getKeyChainName = () => {
  const name = `${process.env.CI_COMMIT_SHA || 'manual'}-${uuidName}`;
  return `build-${name}.keychain-db`;
}

/**
 * WARNING: the following code below uses sensitive information in order to
 * automate codesigning.
 */
const getAppleSecrets = async () => {
  const tmp = await mktempd();
  const defaultCerts = path.join(process.env.HOME, 'certs');

  const secrets = {
    appleCer: process.env.CHALLENGE_RUN_COUNTER_CI_CERT_APPLE_CA,
    dacCer: process.env.CHALLENGE_RUN_COUNTER_CI_CERT_APPLE_DAC,
    codeSignKey: process.env.CHALLENGE_RUN_COUNTER_CI_SIGNING_KEY,
    keyChainPass: process.env.CHALLENGE_RUN_COUNTER_CI_KEYCHAIN_PASS,
    codeSignPass: process.env.CHALLENGE_RUN_COUNTER_CI_SIGNING_PASS,
  }

  const keys = Object.keys(secrets);
  const result = {}

  for (let i = 0; i < keys.length; ++i) {
    const key = keys[i];
    const val = secrets[key];

    switch (key) {
      case 'dacCer':
      case 'appleCer':
      case 'codeSignKey': {
        if (val) {
          const ext = (key == 'codeSignKey' ? '.p12' : '.cer');
          const filePath = path.join(tmp, `${key}${ext}`);
          const buff = Buffer.from(val, 'base64');
          await fs.writeFile(filePath, buff);
          result[key] = filePath;
        }
        break;
      }
      default: {
        if (val) {
          result[key] = val;
        }
        break;
      }
    }
  }

  return {
    appleCer: path.join(defaultCerts, 'apple.cer'),
    dacCer: path.join(defaultCerts, 'dac.cer'),
    codeSignKey: path.join(defaultCerts, 'codesign.p12'),
    keyChainPass: '123123',
    codeSignPass: '123123',
    ...result,
  }
}

const setupKeychain = async () => {
  const keyChainName = getKeyChainName();

  const {
    appleCer,
    dacCer,
    codeSignKey,
    keyChainPass,
    codeSignPass,
  } = await getAppleSecrets();

  // create new keychain
  await waitForCommand([
    'security',
    '-v',
    'create-keychain',
    '-p',
    keyChainPass,
    keyChainName,
  ], sensitiveChildProcessOpts);

  // unlock keychain
  await waitForCommand([
    'security',
    '-v',
    'unlock-keychain',
    '-p',
    keyChainPass,
    keyChainName,
  ], sensitiveChildProcessOpts);

  // set it as default keychain
  await waitForCommand([
    'security',
    '-v',
    'default-keychain',
    '-s',
    keyChainName,
  ], sensitiveChildProcessOpts);

  // use both keychains for codesign
  await waitForCommand([
    'security',
    'list-keychain',
    '-s',
    'login.keychain-db',
    keyChainName,
  ], sensitiveChildProcessOpts);

  const keyChainPath = path.join(
    process.env.HOME,
    'Library',
    'Keychains',
    keyChainName,
  );

  // Apple Worldwide Developer Realtions
  await waitForCommand([
    'security',
    '-v',
    'import',
    appleCer,
    '-k',
    keyChainPath,
    '-T',
    '/usr/bin/codesign',
  ], sensitiveChildProcessOpts);

  // Apple Developer Authentication
  await waitForCommand([
    'security',
    '-v',
    'import',
    dacCer,
    '-k',
    keyChainPath,
    '-T',
    '/usr/bin/codesign',
  ], sensitiveChildProcessOpts);

  // Developer ID Application (codesign)
  await waitForCommand([
    'security',
    '-v',
    'import',
    codeSignKey,
    '-k',
    keyChainPath,
    '-P',
    codeSignPass,
    '-T',
    '/usr/bin/codesign',
  ], sensitiveChildProcessOpts);

  // Allow codesign key to be used by apple tools
  await waitForCommand([
    'security',
    '-v',
    'set-key-partition-list',
    '-S',
    'apple-tool:,apple:',
    '-k',
    keyChainPass,
    keyChainPath,
  ], sensitiveChildProcessOpts);
}

module.exports = {
  setupKeychain,
}
