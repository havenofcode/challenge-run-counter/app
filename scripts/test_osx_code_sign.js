/* eslint @typescript-eslint/no-var-requires: "off" */
const {setupKeychain} = require('./osx_codesign_utils');

const main = async () => {
  await setupKeychain();
}

main()
  .then(() => console.log('done'))
  .catch(console.error);
