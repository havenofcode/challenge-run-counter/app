/* eslint @typescript-eslint/no-var-requires: "off" */
const path = require('path');
const {waitUntilDone, removeIfExists} = require('./util');
const root = path.resolve(__dirname, '..');

const clean = async () => {
  const localDbDevPath = path.resolve(__dirname, '..', 'data', 'development.db');
  const localDbProdPath = path.resolve(__dirname, '..', 'data', 'production.db');
  const distDir = path.resolve(__dirname, '..', 'dist');
  const outDir = path.resolve(__dirname, '..', 'out');

  await removeIfExists(localDbDevPath);
  await removeIfExists(localDbProdPath);
  await removeIfExists(distDir, true);
  await removeIfExists(outDir, true);
}

const main = async () => {
  await clean();
  await waitUntilDone(['npm', 'run', 'build:tsc:main']);
  await waitUntilDone(['npm', 'run', 'build:tsc:web']);
  await waitUntilDone(['npm', 'run', 'build:webpack']);
  await waitUntilDone(['npm', 'run', 'migrate']);
  await waitUntilDone(['npm', 'run', 'seed'], {
    cwd: root,
    stdio: 'inherit',
    env: {
      ...process.env,
      NODE_ENV: 'development',
    }
  });
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
