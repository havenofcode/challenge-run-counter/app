/* eslint @typescript-eslint/no-var-requires: "off" */
const fs = require('fs').promises;
const path = require('path');
const {waitUntilDone} = require('./util');
const root = path.resolve(__dirname, '..');
const dataDir = path.join(root, 'data');
const dbFile = path.join(dataDir, 'development.db');
const seedScript = path.join(root, 'dist', 'test', 'seed_database.js');

const buildMainProcess = (watch = false) => waitUntilDone([
  'tsc',
  ...(watch ? ['-w', '--preserveWatchOutput'] : []),
  '-P',
  'tsconfig.main.json',
]);

const buildRenderProcess = (watch = false) => waitUntilDone([
  'tsc',
  ...(watch ? ['-w', '--preserveWatchOutput'] : []),
  '-P',
  'tsconfig.web.json',
]);

const seedDatabase = async () => {
  try {
    await fs.stat(dataDir);
  } catch (err) {
    console.warn('making data directory', dataDir);
    await fs.mkdir(dataDir);
  }

  try {
    await fs.stat(dbFile);
  } catch (err) {
    console.warn('building main process', dbFile);
    await buildMainProcess();
    console.warn('migrating database')
    await waitUntilDone(['knex', 'migrate:latest']);
    console.warn('running seed script');
    await waitUntilDone(['node', seedScript]);
  }
}

const buildWebpack = (watch = false) => waitUntilDone([
  'webpack',
  '-c',
  'webpack.config.js',
  ...(watch ? ['--watch'] : []),
]);

const runFullBuild = async () => {
  await buildMainProcess();
  await buildRenderProcess();
  await buildWebpack();
}

const startApp = () => waitUntilDone(['electron', root]);

const main = async () => {
  await seedDatabase();
  await runFullBuild();

  await Promise.all([
    startApp(),
    buildMainProcess(true),
    buildRenderProcess(true),
    buildWebpack(true),
  ]);
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
