/* eslint @typescript-eslint/no-var-requires: "off" */
const path = require('path');
const fs = require('fs').promises;
const child_process = require('child_process');
const os = require('os');
const root = path.resolve(__dirname, '..');

const defaultChildProcessOptions = {
  cwd: root,
  stdio: 'inherit',
  env: {
    NODE_ENV: 'production',
    ...process.env,
  }
}

async function mktempd() {
  const prefix = path.resolve(__dirname, '..', '..', 'out');

  try {
    await fs.stat(prefix);
  } catch (err) {
    await fs.mkdir(prefix);
  }

  return await fs.mkdtemp(`${prefix}/`);
}

async function waitForCommand(allArgs, opts = {}) {
  const [command] = allArgs;
  const args = allArgs.splice(1);

  const cmd = `> ${command} ${args.join(' ')}`;
  const cmdLog = path.resolve('..', 'cmd.log');

  try {
    await fs.stat(cmdLog);
  } catch {
    await fs.writeFile(cmdLog, '');
  }

  await fs.appendFile(cmdLog, `\n${command} ${args.join(' ')}\n`);

  return new Promise((resolve, reject) => {
    console.log(`running command: ${cmd}`)
    const child = child_process.spawn(
      (os.platform() === 'win32' ? `${command}.cmd` : command),
      args,
      {defaultChildProcessOptions, ...opts}
    );

    child.on('exit', (code) => {
      if (code) {
        console.error(`error: ${cmd} exit with ${code}`);
        reject(new Error(`${code}`));
      } else {
        console.info(`success: ${cmd}`);
        resolve(code);
      }
    });
  });
}

async function waitUntilDone(args, opts = {}) {
  return await waitForCommand(['npx', ...args], opts);
}

async function removeIfExists(strPath, recursive = false) {
  try {
    await fs.stat(strPath);
    await fs.rm(strPath, !recursive ? {} : {
      recursive: true,
      maxRetries: 5,
      retryDelay: 100,
    });
    console.warn(`${strPath} unlinked`);
  } catch (err) {
    console.warn(`${strPath} does not exist`);
  }
}

module.exports = {
  waitForCommand,
  waitUntilDone,
  removeIfExists,
  mktempd,
}
