challenge-run-counter
=====================

> A cross platform desktop application tool for displaying realtime challenge
> runs stats. Useful for displaying hit count on
> [No-Hit](https://www.teamhitless.com/about/rules/) runs or
> durations for [Speed-Run's](https://www.speedrun.com/knowledgebase/about).

# Disclaimer

This is a work in progress. If you have any issues, please post an issue
[here](https://gitlab.com/havenofcode/challenge-run-counter/app/-/issues) or
send a message in the [discord](https://discord.gg/duzrG5Es5Z).

# Features

- Cross Platform app available on Linux, Mac, Mac M1, and Windows.
- Possible to display stats over network using WebSocket.
- Data is fault tolerant, no data loss when power goes out.
- Customizeable display using high level
  [library](https://gitlab.com/havenofcode/challenge-run-counter/client)
- Persists all stats for every attempt on hard drive.
- Supports tracking stats for multi-game run's.

# Installation

## [Download Windows x64 Latest Release](https://storage.googleapis.com/challenge-run-counter/5fef7a7f-challenge-run-counter-win32-x64.zip)

## [Download Mac Intel Latest Release](https://storage.googleapis.com/challenge-run-counter/5fef7a7f-challenge-run-counter-darwin-x64.zip)

## [Download Mac M1 Latest Release](https://storage.googleapis.com/challenge-run-counter/5fef7a7f-challenge-run-counter-darwin-arm64.zip)

## [Download Linux x64 Latest Release](https://storage.googleapis.com/challenge-run-counter/5fef7a7f-challenge-run-counter-linux-x64.zip)

# Manual

Wiki is hosted
[here](https://gitlab.com/havenofcode/challenge-run-counter/app/-/wikis/home).

## Privacy Policy

Our privacy policy is available [here](./privacy-policy.md).

### Report Issues

To report issues and get help please create an issue [here]
(https://gitlab.com/havenofcode/challenge-run-counter/app/-/issues). Feature
requests or suggestions are allowed.

### Development

```bash
npm install
npm start
```

### Test

```bash
npm test
```

### Do you use tmux with tmuxp?

Create a session for development.

```
npm install
tmuxp load tmuxp.yml
```

### Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

### Docs

These [docs](https://havenofcode.gitlab.io/challenge-run-counter/app/) are
intended to aid development in this repository. For documentation related to
creating custom client, read the [challenge-run-client]
(https://havenofcode.gitlab.io/challenge-run-counter/client/) docs.

### Build for distribution.

```bash
npm run package
```

If building for production, set `NODE_ENV=production`.

```bash
export NODE_ENV=production
npx run package
```

#### CI/CD and Infrastructure Notes

#### Mac OSX

Gitlab runner installation. Install as a system daemon and change the plist.


`/Library/LaunchDaemon/gitlab-runner.plist`

Add these fields.

```
<key>UserName</key>
<string>gitlab-runner</string>
<key>SessionCreate</key>
<true/>
```

Should look like this.

```
<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN"
"http://www.apple.com/DTDs/PropertyList-1.0.dtd" >
<plist version='1.0'>
  <dict>
    <key>Label</key>
    <string>gitlab-runner</string>
    <key>UserName</key>
    <string>gitlab-runner</string>
    <key>ProgramArguments</key>
    <array>
      <string>/usr/local/bin/gitlab-runner</string>
      <string>run</string>
      <string>--working-directory</string>
      <string>/Users/gitlab-runner/the-gitlab-runner</string>
      <string>--config</string>
      <string>/etc/gitlab-runner/config.toml</string>
      <string>--service</string>
      <string>gitlab-runner</string>
      <string>--syslog</string>
    </array>
    <key>SessionCreate</key>
    <true/>
    <key>KeepAlive</key>
    <true/>
    <key>RunAtLoad</key>
    <true/>
    <key>Disabled</key>
    <false/>
    <key>StandardOutPath</key>
    <string>/usr/local/var/log/gitlab-runner.out.log</string>
    <key>StandardErrorPath</key>
    <string>/usr/local/var/log/gitlab-runner.err.log</string>
  </dict>
</plist>
```

## Build destributables from `forge.config.js`.

```bash
npm run make
```

## Database

Current schema created by `generate-database` npm command.

![database diagram](./database.jpg)

## Async Api Documentation

Use `@asyncapi/generator` to generate markdown documentation for client api.

```
npm install -g @asyncapi/generator
ag ./asset/challenge-run-client-async-api.yml @asyncapi/markdown-template -o /tmp/markdown
```
